----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
-- This design implements a pwm tone generator using custom logic along
-- with a sine function lookup table (LUT) from the Xilinx IP Catalog 
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.all;

entity lab7_top is
    Port (  CLK100MHz : in STD_LOGIC;-- Clock
            
            --input
            SW : in std_logic_vector(15 downto 0); -- freq and volume select (SW 15 is on/off switch)
            BTNC : in std_logic; -- reset
            
            --audio out stuff
            AUD_PWM : out std_logic; --pwm out
            AUD_SD : out std_logic; --on/off switch
            
            --display stuff
            SEG7_CATH : out std_logic_vector(7 downto 0);
            AN : out std_logic_vector(7 downto 0)
         );
end lab7_top;

architecture Behavioral of lab7_top is

    component dds_compiler_0 -- Sine LUT IP from catalog
        Port
        (   aclk : in std_logic;
            s_axis_phase_tvalid : in std_logic;
            s_axis_phase_tdata : in std_logic_vector(7 downto 0); -- 8 bit phase resolution
            m_axis_data_tvalid : out std_logic;
            m_axis_data_tdata : out std_logic_vector(15 downto 0) -- 16 bit sine LUT (dropped to 10 for PWM)       
        );
    end component;
    
    signal freq_to_display, vol_to_display : std_logic_vector(15 downto 0);
    signal to_display : std_logic_vector(31 downto 0);
    signal count_to : unsigned(11 downto 0); -- only need 10 bits for sample_rate_gen, but just in case
    signal freq_sel, vol_sel : std_logic_vector(2 downto 0); -- input switches
    signal no_out, no_pulse : std_logic; -- to set sample rate generator to reset for 0Hz
    signal incr_phase : std_logic;
    signal phase : unsigned(7 downto 0);
    signal dont_care : std_logic;
    signal lut_out : std_logic_vector(15 downto 0);
    signal vol_change_out : unsigned(9 downto 0); 
    signal sign_gone : unsigned (15 downto 0);
    
begin
    
    --input and display logic
    
    AUD_SD <= SW(15); -- on/off switch for audio
    vol_sel <= SW(5 downto 3);
    freq_sel <= SW(2 downto 0);
    
    with freq_sel select
        freq_to_display <=  x"0500" when "001", -- 500Hz
                            x"1000" when "010", -- 1kHz
                            x"1500"when "011", -- 1.5kHz
                            x"2000" when "100", -- 2kHz
                            x"2500" when "101", -- 2.5kHz
                            x"3000" when "110", -- 3kHz
                            x"3500" when "111", -- 3.5kHz
                            x"0000" when others;
                 
     with vol_sel select 
         vol_to_display <=  x"0001" when "001",
                            x"0002" when "010",
                            x"0003"when "011", 
                            x"0004" when "100",
                            x"0005" when "101",
                            x"0006" when "110",
                            x"0007" when "111",
                            x"0000" when others;
    
    to_display <= vol_to_display & freq_to_display;
    
    display : entity seg7_controller port map
    (   clk => CLK100MHz,                              
        reset => BTNC,                            
        disp_chars => to_display,  
        anode_ctl => AN,   
        seg7_out => SEG7_CATH
    );
    
    -- logic for frequency generation
    
    with freq_sel select -- all values hand-calculated: count_to = (round_to_nearest_10ns(1/freq/256))/10
        count_to <= to_unsigned(781, 12) when "001", -- 500Hz
                    to_unsigned(391, 12) when "010", -- 1kHz
                    to_unsigned(260, 12) when "011", -- 1.5kHz
                    to_unsigned(195, 12) when "100", -- 2kHz
                    to_unsigned(156, 12) when "101", -- 2.5kHz
                    to_unsigned(130, 12) when "110", -- 3kHz
                    to_unsigned(112, 12) when "111", -- 3.5kHz
                    to_unsigned(0, 12) when others;
        
    no_pulse <= '1' when freq_sel = "000" else '0'; -- no pulses for 0Hz            
    no_out <= no_pulse or BTNC;
    
    sample_rate_gen : entity pulse_gen 
        generic map (n => count_to'length)
        port map -- sample rate generator
        (   clk => CLK100MHz,
            reset => no_out, -- hacking reset signal to allow '0Hz' Sine Wave
            count_to => count_to, 
            pulse_10ns => incr_phase
        );
    
    phase_accumulate : process (CLK100MHz) -- phase accumulator
    begin
        if(no_out = '1') then
            phase <= (others => '0');
        elsif(rising_edge(CLK100MHz)) then
            if(incr_phase = '1') then
                phase <= phase + 1;
            end if;
        end if;
    end process;
    
    sine_lut : dds_compiler_0 port map --LUT IP
    (   aclk => CLK100MHz,
        s_axis_phase_tvalid => '1',
        s_axis_phase_tdata => std_logic_vector(phase), 
        m_axis_data_tvalid => dont_care,
        m_axis_data_tdata => lut_out  
    );
    
    sign_gone <= unsigned(lut_out) + to_unsigned(32767,16);
    
    damp_sig : entity volume_changer port map --volume selector
    (  vol_sel => vol_sel,
       sine_in => std_logic_vector(sign_gone),
       sine_out => vol_change_out
    );
    
    tone_gen : entity pwm_gen port map --PWM to mono audio out port
    (   clk => CLK100MHz,
        reset => no_out,
        duty_cycle => vol_change_out,
        pwm_out => AUD_PWM
    );
    
end Behavioral;
