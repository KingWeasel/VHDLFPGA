----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--module to generate pulse of width 10ns
--everytime counter with max value count_to
--rolls over
---------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pulse_gen is
    generic (n: positive :=31);
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           count_to : in unsigned(n-1 downto 0);
           pulse_10ns : out STD_LOGIC
          );
end pulse_gen;

architecture Behavioral of pulse_gen is
    signal cntr : unsigned (n-1 downto 0);
    signal clear : std_logic;
begin
    process(clk, reset)
    begin
        if(reset = '1') then
            cntr <= (others => '0');
        elsif(rising_edge(clk)) then
            if(clear = '1') then
                cntr <= (others => '0');
            else
                cntr <= cntr + 1;
            end if;
        end if;
    end process;
    clear <= '1' when (cntr = count_to) else '0';
    pulse_10ns <= clear;
end Behavioral;
