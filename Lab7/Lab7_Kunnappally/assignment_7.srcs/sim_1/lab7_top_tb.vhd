----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--testbench for pulse_gen
---------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab7_top_tb is
--  Port ( );
end lab7_top_tb;

architecture testbench of lab7_top_tb is
    signal clk : std_logic;
    signal reset : std_logic;
    signal freq_sel : std_logic_vector (2 downto 0);
    signal vol_sel : unsigned (2 downto 0) := (others => '0');
    signal switches : std_logic_vector(15 downto 0);
    signal pwm_out : std_logic;
    signal audio_switch : std_logic;
    signal seg7 : std_logic_vector(7 downto 0);
    signal anodes : std_logic_vector(7 downto 0);
begin
    
    audio_switch <= '1';
    
    change_vol : process
    begin
        vol_sel <= vol_sel - 1;
        wait for 20 ms;
    end process change_vol;
    
    freq_sel <= "111";
    switches <= audio_switch & "000000000" & std_logic_vector(vol_sel) & freq_sel;

    lab7_top_inst : entity lab7_top port map
    (
        CLK100MHZ => clk,
        SW => switches,
        BTNC => reset,
        AUD_PWM => pwm_out,
        AUD_SD => audio_switch,
        SEG7_CATH => seg7,
        AN => anodes
    );
    
    reset <= '0', '1' after 20 ns, '0' after 1 ms;
    
    ticktock : process
    begin
        clk <= '1';
        wait for 5 ns;
        clk <= '0';
        wait for 5 ns;
    end process ticktock;

end testbench;