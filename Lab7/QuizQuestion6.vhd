use IEEE.NUMERIC_STD.ALL;

...

signal count : unsigned(1 downto 0);

...

process (reset, clk_50)
begin
	if(reset = '1') then
		en12_5 <= '0';
		count => (others => '0');
	elsif(rising_edge(clk_50)) then
		count <= count + 1;
		if(count = 0) then
			en12_5 <= not en12_5;
		end if;
	end if;
end process;

clk12_5_inst: entity BUFG port map 
(O => clk_12_5, I => en12_5);
