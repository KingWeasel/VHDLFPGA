----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
-- This component takes in a 10-bit unsigned number representing a pwm duty
-- cycle and uses it as the governor for a pwm output pulse train
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pwm_gen is
    Port (  clk : in STD_LOGIC;
            reset : in STD_LOGIC;
            duty_cycle : in unsigned (9 downto 0); --didn't make generic like example
            pwm_out : out STD_LOGIC
         );
end pwm_gen;

architecture Behavioral of pwm_gen is
    signal cntr : unsigned(9 downto 0);
begin
    
    process(clk, reset)
    begin
        if(reset = '1') then
            cntr <= (others => '0');
        elsif(rising_edge(clk)) then
            cntr <= cntr + 1;
        end if;
    end process;
    
    pwm_out <= '1' when (cntr < duty_cycle) else '0';

end Behavioral;
