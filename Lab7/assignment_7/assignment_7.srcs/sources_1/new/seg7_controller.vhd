----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--module to flash a different character on each
--of the 8 displays on the Nexsys board
--at 1kHz
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;
use IEEE.NUMERIC_STD.ALL;

entity seg7_controller is
   port (   clk : in std_logic;
            reset : in std_logic;
            disp_chars : in std_logic_vector (31 downto 0);
            anode_ctl : out std_logic_vector (7 downto 0);
            seg7_out : out std_logic_vector (7 downto 0)
        );
end seg7_controller;

architecture Behavioral of seg7_controller is
    signal mux_char_out : std_logic_vector (3 downto 0);
    signal an_and_cath_pick : unsigned (2 downto 0);
    signal pulse : std_logic;
    
    constant count_width : positive := 17;
begin
    
    --seven-segment display decoder really I think
    seg7_encoder : entity seg7_hex port map
    (
        digit => mux_char_out,
        seg7 => seg7_out
    );
    
    -- generates a 1kHz pulse train for activating the
    -- display_chooser entity below
    
    pulse_1kHz : entity pulse_gen 
    generic map (n => count_width)
    port map
    (
        clk => clk,
        reset => reset,
        count_to => to_unsigned(100000, count_width), -- 1ms (1kHz)
        pulse_10ns => pulse
    );
    
    -- entity for changing enabled display incrementally from
    -- right to left
    display_chooser : entity anode_scroller port map
    (
        clk => clk,
        reset => reset,
        scroll_enable => pulse,
        anode_enc_val => an_and_cath_pick
    );
    
    -- with this process, based on the output of the display_chooser,
    -- pick which character gets displayed on the enabled display
    -- Remember active low for anodes!
    char_mux : process(an_and_cath_pick,disp_chars)
    begin
        case an_and_cath_pick is
            when "001" =>
                mux_char_out <= disp_chars(3 downto 0);
                anode_ctl <= not x"01";
            when "010" =>
                mux_char_out <= disp_chars(7 downto 4);
                anode_ctl <= not x"02";
            when "011" =>
                mux_char_out <= disp_chars(11 downto 8);
                anode_ctl <= not x"04";
            when "100" =>
                mux_char_out <= disp_chars(15 downto 12);
                anode_ctl <= not x"08";
            when "101" =>
                mux_char_out <= disp_chars(19 downto 16);
                anode_ctl <= not x"10";
            when "110" =>
                mux_char_out <= disp_chars(23 downto 20);
                anode_ctl <= not x"20";
            when "111" =>
                mux_char_out <= disp_chars(27 downto 24);
                anode_ctl <= not x"40";
            when others =>
                mux_char_out <= disp_chars(31 downto 28);
                anode_ctl <= not x"80";
        end case;
    end process char_mux; 
      
end Behavioral;
