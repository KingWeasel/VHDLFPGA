----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
-- This component takes in a 16-bit number representing a pwm duty cycle
-- and outputs a 10-bit number representing a scaled version of the input
-- My initial design called for this volume changer to be more complex, hence
-- why it is described as an entity/architecture pair
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity volume_changer is
    Port ( sine_in : in STD_LOGIC_VECTOR (15 downto 0);
           vol_sel : in STD_LOGIC_VECTOR (2 downto 0);
           sine_out : out unsigned (9 downto 0));
end volume_changer;

architecture Behavioral of volume_changer is
    signal sine_scaled : std_logic_vector (9 downto 0);
begin

    with vol_sel select -- makes amplitude smaller, but is not centered around signed 0
        sine_scaled <= "0000000" & sine_in(15 downto 13) when "000",                 
                       "000000" & sine_in(15 downto 12) when "001",
                       "00000" & sine_in(15 downto 11) when "010",
                       "0000" & sine_in(15 downto 10) when "011", 
                       "000" & sine_in(15 downto 9) when "100",
                       "00" & sine_in(15 downto 8) when "101",
                       "0" & sine_in(15 downto 7) when "110",
                       sine_in(15 downto 6) when "111",
                       "0000000000" when others;             
        
    sine_out <= unsigned(sine_scaled);

end Behavioral;
