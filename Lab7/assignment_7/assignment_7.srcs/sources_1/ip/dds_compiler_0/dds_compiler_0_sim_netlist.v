// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Thu Apr 26 20:37:26 2018
// Host        : laptop running 64-bit Manjaro Linux
// Command     : write_verilog -force -mode funcsim
//               /home/jk/Documents/Homework/VHDLFPGA/Lab7/assignment_7/assignment_7.srcs/sources_1/ip/dds_compiler_0/dds_compiler_0_sim_netlist.v
// Design      : dds_compiler_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dds_compiler_0,dds_compiler_v6_0_15,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "dds_compiler_v6_0_15,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module dds_compiler_0
   (aclk,
    s_axis_phase_tvalid,
    s_axis_phase_tdata,
    m_axis_data_tvalid,
    m_axis_data_tdata);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 aclk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME aclk_intf, ASSOCIATED_BUSIF M_AXIS_PHASE:S_AXIS_CONFIG:M_AXIS_DATA:S_AXIS_PHASE, ASSOCIATED_RESET aresetn, ASSOCIATED_CLKEN aclken, FREQ_HZ 100000000, PHASE 0.000" *) input aclk;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 S_AXIS_PHASE TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXIS_PHASE, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 0, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef" *) input s_axis_phase_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 S_AXIS_PHASE TDATA" *) input [7:0]s_axis_phase_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 M_AXIS_DATA TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME M_AXIS_DATA, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 0, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef" *) output m_axis_data_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 M_AXIS_DATA TDATA" *) output [15:0]m_axis_data_tdata;

  wire aclk;
  wire [15:0]m_axis_data_tdata;
  wire m_axis_data_tvalid;
  wire [7:0]s_axis_phase_tdata;
  wire s_axis_phase_tvalid;
  wire NLW_U0_debug_axi_resync_in_UNCONNECTED;
  wire NLW_U0_debug_core_nd_UNCONNECTED;
  wire NLW_U0_debug_phase_nd_UNCONNECTED;
  wire NLW_U0_event_phase_in_invalid_UNCONNECTED;
  wire NLW_U0_event_pinc_invalid_UNCONNECTED;
  wire NLW_U0_event_poff_invalid_UNCONNECTED;
  wire NLW_U0_event_s_config_tlast_missing_UNCONNECTED;
  wire NLW_U0_event_s_config_tlast_unexpected_UNCONNECTED;
  wire NLW_U0_event_s_phase_chanid_incorrect_UNCONNECTED;
  wire NLW_U0_event_s_phase_tlast_missing_UNCONNECTED;
  wire NLW_U0_event_s_phase_tlast_unexpected_UNCONNECTED;
  wire NLW_U0_m_axis_data_tlast_UNCONNECTED;
  wire NLW_U0_m_axis_phase_tlast_UNCONNECTED;
  wire NLW_U0_m_axis_phase_tvalid_UNCONNECTED;
  wire NLW_U0_s_axis_config_tready_UNCONNECTED;
  wire NLW_U0_s_axis_phase_tready_UNCONNECTED;
  wire [0:0]NLW_U0_debug_axi_chan_in_UNCONNECTED;
  wire [7:0]NLW_U0_debug_axi_pinc_in_UNCONNECTED;
  wire [7:0]NLW_U0_debug_axi_poff_in_UNCONNECTED;
  wire [7:0]NLW_U0_debug_phase_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_data_tuser_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_phase_tdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_phase_tuser_UNCONNECTED;

  (* C_ACCUMULATOR_WIDTH = "8" *) 
  (* C_AMPLITUDE = "0" *) 
  (* C_CHANNELS = "1" *) 
  (* C_CHAN_WIDTH = "1" *) 
  (* C_DEBUG_INTERFACE = "0" *) 
  (* C_HAS_ACLKEN = "0" *) 
  (* C_HAS_ARESETN = "0" *) 
  (* C_HAS_M_DATA = "1" *) 
  (* C_HAS_M_PHASE = "0" *) 
  (* C_HAS_PHASEGEN = "0" *) 
  (* C_HAS_PHASE_OUT = "0" *) 
  (* C_HAS_SINCOS = "1" *) 
  (* C_HAS_S_CONFIG = "0" *) 
  (* C_HAS_S_PHASE = "1" *) 
  (* C_HAS_TLAST = "0" *) 
  (* C_HAS_TREADY = "0" *) 
  (* C_LATENCY = "2" *) 
  (* C_MEM_TYPE = "1" *) 
  (* C_MODE_OF_OPERATION = "0" *) 
  (* C_MODULUS = "9" *) 
  (* C_M_DATA_HAS_TUSER = "0" *) 
  (* C_M_DATA_TDATA_WIDTH = "16" *) 
  (* C_M_DATA_TUSER_WIDTH = "1" *) 
  (* C_M_PHASE_HAS_TUSER = "0" *) 
  (* C_M_PHASE_TDATA_WIDTH = "1" *) 
  (* C_M_PHASE_TUSER_WIDTH = "1" *) 
  (* C_NEGATIVE_COSINE = "0" *) 
  (* C_NEGATIVE_SINE = "0" *) 
  (* C_NOISE_SHAPING = "0" *) 
  (* C_OPTIMISE_GOAL = "0" *) 
  (* C_OUTPUTS_REQUIRED = "0" *) 
  (* C_OUTPUT_FORM = "0" *) 
  (* C_OUTPUT_WIDTH = "16" *) 
  (* C_PHASE_ANGLE_WIDTH = "8" *) 
  (* C_PHASE_INCREMENT = "2" *) 
  (* C_PHASE_INCREMENT_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_PHASE_OFFSET = "0" *) 
  (* C_PHASE_OFFSET_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_POR_MODE = "0" *) 
  (* C_RESYNC = "0" *) 
  (* C_S_CONFIG_SYNC_MODE = "0" *) 
  (* C_S_CONFIG_TDATA_WIDTH = "1" *) 
  (* C_S_PHASE_HAS_TUSER = "0" *) 
  (* C_S_PHASE_TDATA_WIDTH = "8" *) 
  (* C_S_PHASE_TUSER_WIDTH = "1" *) 
  (* C_USE_DSP48 = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  dds_compiler_0_dds_compiler_v6_0_15 U0
       (.aclk(aclk),
        .aclken(1'b1),
        .aresetn(1'b1),
        .debug_axi_chan_in(NLW_U0_debug_axi_chan_in_UNCONNECTED[0]),
        .debug_axi_pinc_in(NLW_U0_debug_axi_pinc_in_UNCONNECTED[7:0]),
        .debug_axi_poff_in(NLW_U0_debug_axi_poff_in_UNCONNECTED[7:0]),
        .debug_axi_resync_in(NLW_U0_debug_axi_resync_in_UNCONNECTED),
        .debug_core_nd(NLW_U0_debug_core_nd_UNCONNECTED),
        .debug_phase(NLW_U0_debug_phase_UNCONNECTED[7:0]),
        .debug_phase_nd(NLW_U0_debug_phase_nd_UNCONNECTED),
        .event_phase_in_invalid(NLW_U0_event_phase_in_invalid_UNCONNECTED),
        .event_pinc_invalid(NLW_U0_event_pinc_invalid_UNCONNECTED),
        .event_poff_invalid(NLW_U0_event_poff_invalid_UNCONNECTED),
        .event_s_config_tlast_missing(NLW_U0_event_s_config_tlast_missing_UNCONNECTED),
        .event_s_config_tlast_unexpected(NLW_U0_event_s_config_tlast_unexpected_UNCONNECTED),
        .event_s_phase_chanid_incorrect(NLW_U0_event_s_phase_chanid_incorrect_UNCONNECTED),
        .event_s_phase_tlast_missing(NLW_U0_event_s_phase_tlast_missing_UNCONNECTED),
        .event_s_phase_tlast_unexpected(NLW_U0_event_s_phase_tlast_unexpected_UNCONNECTED),
        .m_axis_data_tdata(m_axis_data_tdata),
        .m_axis_data_tlast(NLW_U0_m_axis_data_tlast_UNCONNECTED),
        .m_axis_data_tready(1'b0),
        .m_axis_data_tuser(NLW_U0_m_axis_data_tuser_UNCONNECTED[0]),
        .m_axis_data_tvalid(m_axis_data_tvalid),
        .m_axis_phase_tdata(NLW_U0_m_axis_phase_tdata_UNCONNECTED[0]),
        .m_axis_phase_tlast(NLW_U0_m_axis_phase_tlast_UNCONNECTED),
        .m_axis_phase_tready(1'b0),
        .m_axis_phase_tuser(NLW_U0_m_axis_phase_tuser_UNCONNECTED[0]),
        .m_axis_phase_tvalid(NLW_U0_m_axis_phase_tvalid_UNCONNECTED),
        .s_axis_config_tdata(1'b0),
        .s_axis_config_tlast(1'b0),
        .s_axis_config_tready(NLW_U0_s_axis_config_tready_UNCONNECTED),
        .s_axis_config_tvalid(1'b0),
        .s_axis_phase_tdata(s_axis_phase_tdata),
        .s_axis_phase_tlast(1'b0),
        .s_axis_phase_tready(NLW_U0_s_axis_phase_tready_UNCONNECTED),
        .s_axis_phase_tuser(1'b0),
        .s_axis_phase_tvalid(s_axis_phase_tvalid));
endmodule

(* C_ACCUMULATOR_WIDTH = "8" *) (* C_AMPLITUDE = "0" *) (* C_CHANNELS = "1" *) 
(* C_CHAN_WIDTH = "1" *) (* C_DEBUG_INTERFACE = "0" *) (* C_HAS_ACLKEN = "0" *) 
(* C_HAS_ARESETN = "0" *) (* C_HAS_M_DATA = "1" *) (* C_HAS_M_PHASE = "0" *) 
(* C_HAS_PHASEGEN = "0" *) (* C_HAS_PHASE_OUT = "0" *) (* C_HAS_SINCOS = "1" *) 
(* C_HAS_S_CONFIG = "0" *) (* C_HAS_S_PHASE = "1" *) (* C_HAS_TLAST = "0" *) 
(* C_HAS_TREADY = "0" *) (* C_LATENCY = "2" *) (* C_MEM_TYPE = "1" *) 
(* C_MODE_OF_OPERATION = "0" *) (* C_MODULUS = "9" *) (* C_M_DATA_HAS_TUSER = "0" *) 
(* C_M_DATA_TDATA_WIDTH = "16" *) (* C_M_DATA_TUSER_WIDTH = "1" *) (* C_M_PHASE_HAS_TUSER = "0" *) 
(* C_M_PHASE_TDATA_WIDTH = "1" *) (* C_M_PHASE_TUSER_WIDTH = "1" *) (* C_NEGATIVE_COSINE = "0" *) 
(* C_NEGATIVE_SINE = "0" *) (* C_NOISE_SHAPING = "0" *) (* C_OPTIMISE_GOAL = "0" *) 
(* C_OUTPUTS_REQUIRED = "0" *) (* C_OUTPUT_FORM = "0" *) (* C_OUTPUT_WIDTH = "16" *) 
(* C_PHASE_ANGLE_WIDTH = "8" *) (* C_PHASE_INCREMENT = "2" *) (* C_PHASE_INCREMENT_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
(* C_PHASE_OFFSET = "0" *) (* C_PHASE_OFFSET_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) (* C_POR_MODE = "0" *) 
(* C_RESYNC = "0" *) (* C_S_CONFIG_SYNC_MODE = "0" *) (* C_S_CONFIG_TDATA_WIDTH = "1" *) 
(* C_S_PHASE_HAS_TUSER = "0" *) (* C_S_PHASE_TDATA_WIDTH = "8" *) (* C_S_PHASE_TUSER_WIDTH = "1" *) 
(* C_USE_DSP48 = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "dds_compiler_v6_0_15" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module dds_compiler_0_dds_compiler_v6_0_15
   (aclk,
    aclken,
    aresetn,
    s_axis_phase_tvalid,
    s_axis_phase_tready,
    s_axis_phase_tdata,
    s_axis_phase_tlast,
    s_axis_phase_tuser,
    s_axis_config_tvalid,
    s_axis_config_tready,
    s_axis_config_tdata,
    s_axis_config_tlast,
    m_axis_data_tvalid,
    m_axis_data_tready,
    m_axis_data_tdata,
    m_axis_data_tlast,
    m_axis_data_tuser,
    m_axis_phase_tvalid,
    m_axis_phase_tready,
    m_axis_phase_tdata,
    m_axis_phase_tlast,
    m_axis_phase_tuser,
    event_pinc_invalid,
    event_poff_invalid,
    event_phase_in_invalid,
    event_s_phase_tlast_missing,
    event_s_phase_tlast_unexpected,
    event_s_phase_chanid_incorrect,
    event_s_config_tlast_missing,
    event_s_config_tlast_unexpected,
    debug_axi_pinc_in,
    debug_axi_poff_in,
    debug_axi_resync_in,
    debug_axi_chan_in,
    debug_core_nd,
    debug_phase,
    debug_phase_nd);
  input aclk;
  input aclken;
  input aresetn;
  input s_axis_phase_tvalid;
  output s_axis_phase_tready;
  input [7:0]s_axis_phase_tdata;
  input s_axis_phase_tlast;
  input [0:0]s_axis_phase_tuser;
  input s_axis_config_tvalid;
  output s_axis_config_tready;
  input [0:0]s_axis_config_tdata;
  input s_axis_config_tlast;
  output m_axis_data_tvalid;
  input m_axis_data_tready;
  output [15:0]m_axis_data_tdata;
  output m_axis_data_tlast;
  output [0:0]m_axis_data_tuser;
  output m_axis_phase_tvalid;
  input m_axis_phase_tready;
  output [0:0]m_axis_phase_tdata;
  output m_axis_phase_tlast;
  output [0:0]m_axis_phase_tuser;
  output event_pinc_invalid;
  output event_poff_invalid;
  output event_phase_in_invalid;
  output event_s_phase_tlast_missing;
  output event_s_phase_tlast_unexpected;
  output event_s_phase_chanid_incorrect;
  output event_s_config_tlast_missing;
  output event_s_config_tlast_unexpected;
  output [7:0]debug_axi_pinc_in;
  output [7:0]debug_axi_poff_in;
  output debug_axi_resync_in;
  output [0:0]debug_axi_chan_in;
  output debug_core_nd;
  output [7:0]debug_phase;
  output debug_phase_nd;

  wire \<const0> ;
  wire aclk;
  wire event_s_phase_tlast_missing;
  wire [15:0]m_axis_data_tdata;
  wire m_axis_data_tvalid;
  wire [7:0]s_axis_phase_tdata;
  wire s_axis_phase_tvalid;
  wire NLW_i_synth_debug_axi_resync_in_UNCONNECTED;
  wire NLW_i_synth_debug_core_nd_UNCONNECTED;
  wire NLW_i_synth_debug_phase_nd_UNCONNECTED;
  wire NLW_i_synth_event_phase_in_invalid_UNCONNECTED;
  wire NLW_i_synth_event_pinc_invalid_UNCONNECTED;
  wire NLW_i_synth_event_poff_invalid_UNCONNECTED;
  wire NLW_i_synth_event_s_config_tlast_missing_UNCONNECTED;
  wire NLW_i_synth_event_s_config_tlast_unexpected_UNCONNECTED;
  wire NLW_i_synth_event_s_phase_chanid_incorrect_UNCONNECTED;
  wire NLW_i_synth_event_s_phase_tlast_unexpected_UNCONNECTED;
  wire NLW_i_synth_m_axis_data_tlast_UNCONNECTED;
  wire NLW_i_synth_m_axis_phase_tlast_UNCONNECTED;
  wire NLW_i_synth_m_axis_phase_tvalid_UNCONNECTED;
  wire NLW_i_synth_s_axis_config_tready_UNCONNECTED;
  wire NLW_i_synth_s_axis_phase_tready_UNCONNECTED;
  wire [0:0]NLW_i_synth_debug_axi_chan_in_UNCONNECTED;
  wire [7:0]NLW_i_synth_debug_axi_pinc_in_UNCONNECTED;
  wire [7:0]NLW_i_synth_debug_axi_poff_in_UNCONNECTED;
  wire [7:0]NLW_i_synth_debug_phase_UNCONNECTED;
  wire [0:0]NLW_i_synth_m_axis_data_tuser_UNCONNECTED;
  wire [0:0]NLW_i_synth_m_axis_phase_tdata_UNCONNECTED;
  wire [0:0]NLW_i_synth_m_axis_phase_tuser_UNCONNECTED;

  assign debug_axi_chan_in[0] = \<const0> ;
  assign debug_axi_pinc_in[7] = \<const0> ;
  assign debug_axi_pinc_in[6] = \<const0> ;
  assign debug_axi_pinc_in[5] = \<const0> ;
  assign debug_axi_pinc_in[4] = \<const0> ;
  assign debug_axi_pinc_in[3] = \<const0> ;
  assign debug_axi_pinc_in[2] = \<const0> ;
  assign debug_axi_pinc_in[1] = \<const0> ;
  assign debug_axi_pinc_in[0] = \<const0> ;
  assign debug_axi_poff_in[7] = \<const0> ;
  assign debug_axi_poff_in[6] = \<const0> ;
  assign debug_axi_poff_in[5] = \<const0> ;
  assign debug_axi_poff_in[4] = \<const0> ;
  assign debug_axi_poff_in[3] = \<const0> ;
  assign debug_axi_poff_in[2] = \<const0> ;
  assign debug_axi_poff_in[1] = \<const0> ;
  assign debug_axi_poff_in[0] = \<const0> ;
  assign debug_axi_resync_in = \<const0> ;
  assign debug_core_nd = \<const0> ;
  assign debug_phase[7] = \<const0> ;
  assign debug_phase[6] = \<const0> ;
  assign debug_phase[5] = \<const0> ;
  assign debug_phase[4] = \<const0> ;
  assign debug_phase[3] = \<const0> ;
  assign debug_phase[2] = \<const0> ;
  assign debug_phase[1] = \<const0> ;
  assign debug_phase[0] = \<const0> ;
  assign debug_phase_nd = \<const0> ;
  assign event_phase_in_invalid = \<const0> ;
  assign event_pinc_invalid = \<const0> ;
  assign event_poff_invalid = \<const0> ;
  assign event_s_config_tlast_missing = \<const0> ;
  assign event_s_config_tlast_unexpected = \<const0> ;
  assign event_s_phase_chanid_incorrect = \<const0> ;
  assign event_s_phase_tlast_unexpected = \<const0> ;
  assign m_axis_data_tlast = \<const0> ;
  assign m_axis_data_tuser[0] = \<const0> ;
  assign m_axis_phase_tdata[0] = \<const0> ;
  assign m_axis_phase_tlast = \<const0> ;
  assign m_axis_phase_tuser[0] = \<const0> ;
  assign m_axis_phase_tvalid = \<const0> ;
  assign s_axis_config_tready = \<const0> ;
  assign s_axis_phase_tready = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ACCUMULATOR_WIDTH = "8" *) 
  (* C_AMPLITUDE = "0" *) 
  (* C_CHANNELS = "1" *) 
  (* C_CHAN_WIDTH = "1" *) 
  (* C_DEBUG_INTERFACE = "0" *) 
  (* C_HAS_ACLKEN = "0" *) 
  (* C_HAS_ARESETN = "0" *) 
  (* C_HAS_M_DATA = "1" *) 
  (* C_HAS_M_PHASE = "0" *) 
  (* C_HAS_PHASEGEN = "0" *) 
  (* C_HAS_PHASE_OUT = "0" *) 
  (* C_HAS_SINCOS = "1" *) 
  (* C_HAS_S_CONFIG = "0" *) 
  (* C_HAS_S_PHASE = "1" *) 
  (* C_HAS_TLAST = "0" *) 
  (* C_HAS_TREADY = "0" *) 
  (* C_LATENCY = "2" *) 
  (* C_MEM_TYPE = "1" *) 
  (* C_MODE_OF_OPERATION = "0" *) 
  (* C_MODULUS = "9" *) 
  (* C_M_DATA_HAS_TUSER = "0" *) 
  (* C_M_DATA_TDATA_WIDTH = "16" *) 
  (* C_M_DATA_TUSER_WIDTH = "1" *) 
  (* C_M_PHASE_HAS_TUSER = "0" *) 
  (* C_M_PHASE_TDATA_WIDTH = "1" *) 
  (* C_M_PHASE_TUSER_WIDTH = "1" *) 
  (* C_NEGATIVE_COSINE = "0" *) 
  (* C_NEGATIVE_SINE = "0" *) 
  (* C_NOISE_SHAPING = "0" *) 
  (* C_OPTIMISE_GOAL = "0" *) 
  (* C_OUTPUTS_REQUIRED = "0" *) 
  (* C_OUTPUT_FORM = "0" *) 
  (* C_OUTPUT_WIDTH = "16" *) 
  (* C_PHASE_ANGLE_WIDTH = "8" *) 
  (* C_PHASE_INCREMENT = "2" *) 
  (* C_PHASE_INCREMENT_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_PHASE_OFFSET = "0" *) 
  (* C_PHASE_OFFSET_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_POR_MODE = "0" *) 
  (* C_RESYNC = "0" *) 
  (* C_S_CONFIG_SYNC_MODE = "0" *) 
  (* C_S_CONFIG_TDATA_WIDTH = "1" *) 
  (* C_S_PHASE_HAS_TUSER = "0" *) 
  (* C_S_PHASE_TDATA_WIDTH = "8" *) 
  (* C_S_PHASE_TUSER_WIDTH = "1" *) 
  (* C_USE_DSP48 = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  dds_compiler_0_dds_compiler_v6_0_15_viv i_synth
       (.aclk(aclk),
        .aclken(1'b0),
        .aresetn(1'b0),
        .debug_axi_chan_in(NLW_i_synth_debug_axi_chan_in_UNCONNECTED[0]),
        .debug_axi_pinc_in(NLW_i_synth_debug_axi_pinc_in_UNCONNECTED[7:0]),
        .debug_axi_poff_in(NLW_i_synth_debug_axi_poff_in_UNCONNECTED[7:0]),
        .debug_axi_resync_in(NLW_i_synth_debug_axi_resync_in_UNCONNECTED),
        .debug_core_nd(NLW_i_synth_debug_core_nd_UNCONNECTED),
        .debug_phase(NLW_i_synth_debug_phase_UNCONNECTED[7:0]),
        .debug_phase_nd(NLW_i_synth_debug_phase_nd_UNCONNECTED),
        .event_phase_in_invalid(NLW_i_synth_event_phase_in_invalid_UNCONNECTED),
        .event_pinc_invalid(NLW_i_synth_event_pinc_invalid_UNCONNECTED),
        .event_poff_invalid(NLW_i_synth_event_poff_invalid_UNCONNECTED),
        .event_s_config_tlast_missing(NLW_i_synth_event_s_config_tlast_missing_UNCONNECTED),
        .event_s_config_tlast_unexpected(NLW_i_synth_event_s_config_tlast_unexpected_UNCONNECTED),
        .event_s_phase_chanid_incorrect(NLW_i_synth_event_s_phase_chanid_incorrect_UNCONNECTED),
        .event_s_phase_tlast_missing(event_s_phase_tlast_missing),
        .event_s_phase_tlast_unexpected(NLW_i_synth_event_s_phase_tlast_unexpected_UNCONNECTED),
        .m_axis_data_tdata(m_axis_data_tdata),
        .m_axis_data_tlast(NLW_i_synth_m_axis_data_tlast_UNCONNECTED),
        .m_axis_data_tready(1'b0),
        .m_axis_data_tuser(NLW_i_synth_m_axis_data_tuser_UNCONNECTED[0]),
        .m_axis_data_tvalid(m_axis_data_tvalid),
        .m_axis_phase_tdata(NLW_i_synth_m_axis_phase_tdata_UNCONNECTED[0]),
        .m_axis_phase_tlast(NLW_i_synth_m_axis_phase_tlast_UNCONNECTED),
        .m_axis_phase_tready(1'b0),
        .m_axis_phase_tuser(NLW_i_synth_m_axis_phase_tuser_UNCONNECTED[0]),
        .m_axis_phase_tvalid(NLW_i_synth_m_axis_phase_tvalid_UNCONNECTED),
        .s_axis_config_tdata(1'b0),
        .s_axis_config_tlast(1'b0),
        .s_axis_config_tready(NLW_i_synth_s_axis_config_tready_UNCONNECTED),
        .s_axis_config_tvalid(1'b0),
        .s_axis_phase_tdata(s_axis_phase_tdata),
        .s_axis_phase_tlast(1'b0),
        .s_axis_phase_tready(NLW_i_synth_s_axis_phase_tready_UNCONNECTED),
        .s_axis_phase_tuser(1'b0),
        .s_axis_phase_tvalid(s_axis_phase_tvalid));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R7Su66EFP3j7HdSRwT0ufavHZ21RJuR7GdMa5N1qrx05vZRLzNZT/TrlIe3c6DsFCenpiZCD2noZ
QAoR4Rt+mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
CMEJWch+GbdZ7DIDA14J94rfET0XyGxfytAfvkgCwK+buy8C6yPuTyczckBiUAmLYwq3N0YLZZjn
gsyXn6e48OgTdLuKlj0b1I+R+nOfWP/cHyUHpk91Upohu0q4i+T1Z7YlZ2KevK2O/yOn6S3pNXlM
CA1hIxQSQLLJQcJjXBI=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IDWChuOHJQwebqfYcE88tSCCIBnxLv1aLHU6OnUVlxJuAYH1Wr0uPmJkkVb7CXm2iZXQx/jo6XaT
TumCKxTZIL3ET0tLNKmedouL0GaXfUzXVCSzEoTXiWf2gNPQB6+v0sryyUdggn9CbJglWE9UkluW
rCPI7feYIVKqODl/+/XlmC+0ONTNrMlZjktMivGmmfgFiOaVxlj7ZiVhYDRk2pmK7N0SbS8Yhqtp
tu4XIZyivSAfozOEYzRk3aC5YLPqYEODky8fadXC0TifmV1/9ihpE9MdNVbsAfiU6jAuYaPtixy1
eWfPyz8p770Y8aO4Ymmlv6Cov/zwD1Zr7rP3ng==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
D4tWnXwgYbtbYBATOz3rKT5u236p/210UA0/0NawJUvRuLLRIOY863EXCqmoNKd3cdAJGfRGO/fA
mX3MQnn8fORd5NV0Drcjtq7LVURk4LrUaNUiho8FoaaKgENLoHWz5zN6jL9cfE19cPf5q6X+HSoS
vhMpVULwvEeloyESsidHnjc6Leo2s08QmBHWIJ4gX6Y353OK7qNS3bZaZnw5UMLbMBvsopLT0HMU
QgsF83OsAoA/LETx2kFpFT62GHW7Xr0WQupO68ddkWdncI1pQ1ry5DiS4IAcjHmDYTyo542wmUO5
kUoT65xdo6CgR0mBfndpvcIfOPFrzBLsA3X/8A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TYxxwxeYhuHcZvmvIoDp6PM6jwmqvK/EOpQJuzYEJwksrBgERfR0MxeEKttmbgtW3IAljWYtUY74
488K1yihiHHoprJ33R35ZxUze+TipXVo/GLAiCGp6aVvDPTACRhogMPXLJypmeRU1yO394pPbgS6
wC0P27Oimz3cJkJrwIhG7UV3FbbvFXVTh6Lp9wme459SE3zFnKsJYjUpffIirIVsuN+DETk1csWY
DA9UX9JySwER9tWjcgC7RtzEV1hjIG9WuwYm3zkOqr4FZ/dkK9PLm51AgWpaMXgB/7ws+/P8fkKm
QNdT6izgEuqxwJScjWNpExqD7cRIM9y2FibGuA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Y058Y7qqyKaMCwzJEnFzRJA1mSsdLWRJPV8jeagM24nQRyHL6Of41SQjwa7S6UfHPjaxh3kStD/R
iqFSj7BMeRnjDwKkql9QbQCQ1AEtG8kKMw6X1Sw8vQdkSSWaY8A0qHxlAj9yFFRWps0IUCT20y4r
a1FWV0KSxSpJrwls87U=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BkCcTwW7IOFCvnzvt27BUy3KHmy1QJwSQsGYOAQoWdJnp7bpQCB3MV/YrDTHZ6GeuEjTv+Y4jK1+
AUi7wPge8Y2zeEpQSTFjwsHrg0a6KicpWuoUxj9ZsRjp7lihT95V1Q0eAIg8YhlL39mGtTcQ5Vdp
7z8wKvjx++phq/T2pWg3qojhz3yoqaCG4uvKWuNn2R3f0YfPc7K1qQ8cRTBYuIfje99ZizVelHfv
/gPaALzJb7mtbJVe83NohlYy8IyL0cxXXClT+sW1XPYiN9k5NbywIoRmRDobstBVd3O4Ukd5mT3V
p/qjzuZHyCC/I/jJRQFyZvHI5rcbT8On+yp5MA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fFyx5+ochzMz0jq+iuzdztAE5BUGHUlTlI2VoXvEoggq+52xn3Mr7+VtukNAsvt0xpNQzKjC6bjd
OcuKPAYWAcoKcvPNf4IPDtsx3PM7D0Cq+5qjT5jXW1HwC7Igfq0xykDxKz/bh+cO0RYcdjpm6Zx6
/FXgULioSB78cdEuZzr708ut14m33A0+0PvASPnMK4LKTLbyo82THzB0Oy7tHY4wr3StUHpwMAvP
PZPBI02wkgcEVUz1SwuZcngxdQvC/M/5RFtLROC6YYlRltmHeJ0jaJxHzSnpzsiF57XDpYSMVn4T
2gZsJcFUvGt5j3QmXj5BPHFvdJBqO6RdAgILiA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GLXq02NNa960zLMvUvhi3PvbvIoPNyTSzyPyFJa2x6W8ZZFScj7iN8oJgHQcv9rgoIEVIacvpbwG
DR5tHB9tu0lOBiSuMvynKDPjf2EAds0jtAJWzpA0i7pi3y+hLagwCWDQCAZYYpci8DHiNzw4jRC9
Ns/zRBhqxwSdtk+5Ks7rcnGx7FahxDWdOV8gsiCpxDVKhAZIyq2sGBTBecURFeSImbJ5Z1uXD02N
pvj1aNGElmXdpPfUMQTq8uH1O688f3BvBsMYoQQaivhkrvwLab+mPf83o9iU7HvN6jss7Ha4LdeL
RVBc1g2Low1xBGJRTl213EQRTxmlEL/wFUY0Ww==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 23696)
`pragma protect data_block
2DQlSYlG0rLk+NAKwVk630NI8nE/SnHBDBp0O6fcFVKamzNhp2nq827z4kZnj3FglGFMLvBaDfPR
SS7L7qJrrgts7XyF/QaYwEgXeBvDUzHQSpcrC/GBa+fleRJuR2bD1NGgBg3nVjODNs4LbZB9lIGc
FK0/y0TTWSVhKi0cL5pTLNOjFfzOH5PvIoKcpyiJTb/4wTOxxfNSICKyiSiryXBqM+x3KtycPTjB
iMpj5/9PupwNnTyzVQihe/mjuz68BV0RZbJ0yRjQzrtgIvb3NjBJCGm/up6MRhUw5PGzjgcNseOL
0HRfJfkwGYdqrGiFF6kV8Sctncov3XLJ7LAye1sDeddKqoPC+5KMXIJVQq3S3WP7vpcVaPdk41WJ
VHa9eKuH1QUilJl9PSUWpgckV5GASP5OrMtnx5v9ZEjidWOUYr/jH8amayTr8XFOlr41EYP7w9xB
S4WrutPxrQOJ8iv8JrLknRxy7Ll6MCwxtK/qlVicRWTAj/fPkyFvNzk8fRvJP2gEO18+9/aTDbVZ
OhzNaYDX/VzBWD3RFqpMeTR0ogxFnzCyf9Wphj5pJXJcoZ08fUxHyVmyI/ATNBg+iHV+AB1O/pNh
CQvpWhJ3Y05WirbA3Tu4Psw6PFhJIMTHmJOAmyXEzSOGbWrAOxOvLmH2y7Jm+t64pH+uGZgSZSf0
/tpO/KvBTiuecN8f9zu+mI7WdFeOIZt1fqbYL1RhYsj1CygI69Th/w5m1/QJPwqQLD8To2WWR6EL
EBpM23U20o7PjQrEHJ0XAfwvbDbBKboDec9UbNqnuUde8QYJeV6aqkQ/BRA5JHUEYOOhjxajAIhV
cx/kMCdX7CWeNy42epSxjNy5ZLeqspxZqoChqWm27hS3hoZZbHngHveRfA7MUMBI3mJKQc75cXMZ
hZ/4fSZABG0VSPlu+MhrN3riszPYuJ5UATdxCFmZTJqGedjwYg2f2OPOYUkfe/EpooneWc+VWcLL
VO5a1JiQpzcTTSeIBfrvkR5JD8ZTlnFbpT/kWAURAjlQbQd0SGrTWIJHmvTTq1bg2W3hGFJbHtm5
Db32zCiTKjCY6QSGDi9vsr5phw+qAWfkgFh1cw5EUgv0DHeKcMJKyDgAq6mhlV+dst1g3AKiT6s+
dryHUbjpZUQXoM9mncaSDE1LPWom8GMZJOyXCuInYuPCwabVHX8NDTrX0Pyovg21ylFzc/qhq5TV
Lu+Z3DByRO/Znylb3GKjdLnXbNXL7s4CLXvtH4mz+UjqpePld6LfS0tPB5gw15L04N49R/Sfl9aW
StCXqrCAEcPLTNiVuBD4hWofd7fj/LPYBlDsswsB31pJKCawET0ytRmXY5DFjMPEnk86dd0wns/U
Q0OMFf1ybsE55+Mvfsan6xUdpyZE45WqQwue1LSVpHRLLLZGeKGv3hUnSRLGt8C0NRcSgi+mTdcn
FT6UI7HaX80OCV2xhVvrC4u88Ra2iBpQckG2xlAiX8K+CUoLJ0SiemxWCpGTckjm4xFlmgldFZ22
hWnphR73hHf0Z4kZyOdgfmrcmP0MEknDNwgGqieLQMaZbKuUgLyz169dHzJoOMIEIO62dTJDZld/
47c/3VwkhFOi/4dI26RKggmWLDZRJWbZ7J29QRm3MZcZPlPMlRpT2xF5Z9XW8OqwiFfgAQcxr+Dv
FvUoP4GNGzNjQEFa/mP1/9d4U41/ZHXdCeQN96zxd4zG97djny6AiNZoIlCGjV7AKfSfxNbl/c22
KI6AAnt6rfMyH4AM6vzfcGAy18i6ogLr7pBWUENxUy96rosCDoVWeQK+512K/C9b+3qWhPmLpc38
XE/zWaoHaDVXOSdgdpHlvv/NwzF7nbdsIk02vjfdXqBZTdkVpem0rVWWyN+d0mF69VeMW11/g+aJ
eNm8tkhb8kzWfG/WZgxpKVq80Li/vfBEEHnKeJaD9B4Li+kjD+DOhQ2QpSYRbaeDiB18yBJgGQss
X2q3Ac84aTJIGC2fKRSpBRr2jT2BS3lfBeBEBHRd1EqFUVLygrZmy7I/Xnt+0lwpVcAsTsaK3e46
j3QlhVw0YryHVX3h2dPXZRHBfBxjg+AiMWJfFHH5pYBBQwWvfsAZNEdfX2O2rUtJLza5cUFzcGNx
SfnBTwMtqiDCqzzrLOa+hqXwqFAreQ05nQ/gV8aHVlfj/JQYk9i5Sw/BBpsLgj0Xwsm4QCaq/haL
+BmyFnSsgj2e2WaTnfoULg0BZtwetxQMgrMIvWtRhZH8TsEnPHyZfI0oBUc8gm+pAgn3ok2UnyDk
hX1L8hwknsH15v+8TgttoMQyN92dyoLhGQrftkTg7ZgtcCYbiwd93IehNFCDahSSevXKT1RojFAl
ieeLKbwge9jDntnRmR4ujAlAgMU7QOp7Qe/LLtYOS3rVtddbx+NK0dOKMLK1HtPaWB4+YUW97vgW
tcNgJ+m6YyEhMdad8gLUfRtElUJf1AihCDEcFq5JHwFQfV+EhwVeltb4iRjB0ZdVsZnAWfhgUshP
dfJiT65zfS6JQEWgEyqNVOZV/OVFsdL3TVr/PabkZWDA4WBlt868qrB2DXGUEzw65f9YUkS1WmPU
weZnZXzXWdP25sLT8lC3ymKNDPGgxISP78KMZXCwoqkZOPQox4GR1i9oO5lT5cOoghb2yuoSRcp0
aMVlGCVpy9dR75yxzbph09Au97J+OKa5ZoFgGuucPZb5KvRS54LZWgl0QnF1kbGMve8OTgz19sC2
AiNkitKkaOzF4XOy0lqcv1IGizm3KJU8iZZyfS8QC3fKinv0keWA+JqHJw5uT2MMHDvX5x9xowz5
coK6N3iV/d0LIlvoz7SfDwWF6ulpf/r62nmC/5SHoE8yTQPYggCW9rRKr7BTwtVSOruZbhuv099t
E2pnvilVgLq0EAj8lgLB59czwnoO4PlAKuwJ3CEC4/cewODlMuX+XwsQvvT0vQ6Zh4Dt7FvutDxy
09TZqIuQ/MAEVsIryErHg82x2M522t3u6sYbMSbAvnsXN362eRD6tD/sMENWJdbTnmmGzEjrmGE4
7sPCRRE/GEauftbsrkttDSjReZQescyGQhlQASjQPeXGg9NEnoV3FR9hbKu6LvuyYkTaDtGjC9XM
JYcfbHhkUa/iDAau6D4Ig1L6WYP3rcOdMjnbgPEG9k6Mct0hhIkFiEni8aVQ0+m+97GlprftasgL
S3WtFmQT2h5cGR9AMI0ftfjiT6bhr1E+QUCREE6J6soQFooXkPFplqt3f8p+aWKePAgNjcqZIkzQ
zdB11RaIhyO+quRtmU4vLAY+AkxAz1DUf0i34/XvofNfAV+IzJfWk/KT31LOPFglA8i1SueYxnDX
IC2c1T6fvk5THr5ZCuOCEXHzrW4+IKWwmBRbpECv5xMTAcTIMD3xgNAqTfCjKa1XXsrRiG0VrUSy
vqzy9eDFpXAhJkKSk0v8nMk7twe7MPNeqjlF/QBsz1iWAK5zYqFTjOVr99BfBq5PcVU6Ay/Gt4jV
YUV6N7tp5hjkL6h14f6Y03F5wnBNcAFCPmRUCsWNlrfSkTrZ5ljWAdxqMtg/wdmpoELx5VG2Udne
xd9MQW5s1M7/C8M5AVuDtN33BgUzrMvTZ1p7btPoVg1x1iN81DVr89gCdiqzgAzRhR5KWxDDF8j/
CRnKJhpy4POwAblq+muIbj8KQ6ne7oyQGCYMt9Mk/EMGHdbvkVi9kdXNYk9uROSwUvP4NOIZjwln
cUVo1DAZn1P3qkrU0TQ79/N4Q3fz/smymE3tRljfh2oycEyK4X/Gx77WgTBZlSxc1tu169hml1/R
QreWFQpRl/xt06AhtKyZBSug8OWLnA5fUQgmkMvZawAddsjq6Dw/8j+IIlM6EshJqrRMlyRi2J6m
q2MXf38+vilEQHoNE5Dc4+enXGkEg07rpEO/ObqAPokb0GHEtIDrrbyNN4VGdc487X/2QZ5tC2JW
x0b0G2y+haSUU9TuJ2kB0RthpDgX7LGZS9LvV4zw35BC2pjyelSn1ozaIE2aV1WdP6l2b1WkKOES
Li20J69MdTJRBBjVfYtDchRUcZF7MiAOjqDuVHGoTQwD6VAAqTupzCkXHK4e6PoRV46UHOe9pvNZ
zdaoQUCABTVbnRYJKEY9rEYDegWNeBJXww2zHzDtgWUjXBYHXuClYenl23D2PvipKJbRImpE00yn
9ELlX/LfAiP7LIiqhPM08kkpW2Yo17YVYEAziENfqKqmzAiEZLwRZBqpsr59iQP9n+LtbNr/6mjf
xZiiYSwoEMDg844eMKEjo5LPIk6dxynjqWNYWhyWJqjdsQ4tfTxtR5c5kuxxFdzCa7SlFDVQvBXQ
ON1XXrS441AyQkfxM5S1XcFQG+N59CeKAaZTAaFgeeElTt+5Av9Q05hV05CxU8J4J6nnEPGOB97v
lri0yC0EH0GBB8fmImh+LGdL8kdVnQ0Fn5AA3xMB5o6SAinCfQlboELkRj/QpuLI+pya95FU+EJP
hYGzB05Dq3uxKglVjjMBE3D4UZhCAxYwQw5IpOqYXH0Njpp7Cye0QTllNaCbzimhlD8LcaMiAIrn
D3lUqWZLy3w4XyjcRUjzCT7eCkmcuKOStvB3ElTmYTiRNC2B58uEJVL8bLxSp4MqNbR2CGMEdPbz
zugY2xvk/KE3keMS03ZHXypqdpGZ/+2r6KqxUCGdxY6i2BVd9ITq56zCPBGGV65hQuI2clPfaku/
p9e3nQ5RrPHwLakmPH07ztmzeQEaM1bV43TU8K0PVYTg3xKs6ItPOmkXMK4YMoF0Co4zZWodkhUk
foqaU21j1VDCCr6iyzfZU9Rg4mWeZIATDePX8I+Ihv+wqwU3mbKzsbzgzbya3IVR2Ra2RMhEpdHy
K9eSFd1/k6q4W1gg7VfDSJzq1D6ad7V7bnGl5oDK20sW98m53hqV4zJwXuavkD6jJN/8rCsAOxvw
yHLwDAf5tkOeofUMv7YKmfUEbg/1VfFOjMLATy0Tf+j3a0tLEWAsqz0sTyv6FS5naxbPKrwErzfa
yIisuXT6hIal4Er+JZhYY00JG8rYeXihQA3EagCAexbTyyKxhj120LRficoLNfeT93PyjxOKikep
IGLeNSBnIh+WiVvH8rtzCTELiyuOCfGwSEbcs15glu/6UojYQxA59TqtB3/1gedXoj6COpFenCcZ
mihRZqjk/CKyB7h7nmtvBF1lJbFD0wQf892Gnlr6fvIYYYR6ePdwuabBfWeTCPALt8SS2/kuwckJ
5D5qNLyU02q/p4HViiERpGwuv27+321kL5GGDFqjYfs+bl7Q78tXe0uR0OaYm3434xTDjGGyjy/U
9MZwnoilAspTaCJHIKzaWBrCLWT7XRCgGN8Np7aVWPldI7FP8bF1cZ8qfp4UXJmpq2Zx8t2vpqxr
ht5aIttXCMRFDXbrDrxSVZP+EaQ3fNPGZTI0v7RSTyoEHA+7CuzgEjj9XuxrZmSc/NkgziWLImGI
gP1DIv7QNFip+52wNdIHQxcZYJvYmGpsRZkyU2mch+8atj/uDasf/u4VnO7VEIx/44ysgblG3ePL
jhJH+fA2VO0TQ4gtg+dmkpzSnRQx65zhDTi5XbfGt/qeISO7bxD6/eeNTocz1H/z3e5DAHuGXcUE
YpJaB8LKWYLJ03Stod2brkQkoKhuEWsrQutSFPJ3VSh7hZHSOi3pPA8bWb0bIgBT4VcuAjzffzAt
2OKEpiW3LbNKKuzY0uoJZG0zDLq393pB1p00BmMk4xHnleAKgzzK2TkBnqRo3Ope921UHtd50hxK
ifip17rPUZfYHQmn5xRDkrVSSAJpWCRC+efMIhcagmh4pU441YBQqfu4EaVBswae/BUX9ZuUHZP/
q/kma8rLj4eDjhGZCrxdD7RvXu0XPyuAY3Q1pXyqo1JAcwznYjTmRCOZLzLdZQuS+RhX35VYpTAk
dU3zAaNW0gXLPHwpGUfTUNHX5n63ztRT5BPcpRmv/gVcn1NFn2l97C9B19331jvrrrqs1WvbjIxD
mwz00VDJBorOa9st24lr2tx/D7CcXRKz9kUjC2HQu6CXpGkGTXFHkhfP2IJ2F1ZgsXMDbSmrJYRG
J9bos/8uc+MwwlGJmuDWsTN1rf5g/cLsGHpgfqZAddGxPdHEtIh57BJ9fV69zykEY0NhsUy0LrJe
ansO9DHODTnV2bQhxyKFCRTE52rflBf2fVUgRc+3rpv4hHG04HzZyFNiA7mX430F1G0Iaj9ga5rP
Lab7G8AL3BTrrFUIDLQoHeWWLTqMLASQ0ttIrInV1lyVpB+CuG1EXa8Yhx4aeoQu9ayPRVNcdg2X
40OXxhUo0xuQPj6rg+SHip5nJuPjxQiKKp+Wrt+hR4aWwO7puoEzYd/zZI89HQzqjB1OWfoW9P2o
0TOMmMRTz0Gt6+MgHOOXiL8aM8z8DLlHX3j+xt/+Wv3XDIke8GzX7wbqvCyMypi8B2SwGhrdsTQ8
YJYC1J58DZl+P9gyU+frcnUTG3Tsr+MQ1SiV3mGXi1hjYT/yR1Z329TsJN0am84OKW1QYZDZQpKv
K1hwBFdkXWGKxsgIHacCj28vmaY0ylyDcFaoNc8peY6910kJFBfZnuukyfEAgpc1QxRhHG+bGxnF
0o9Z52OrBJbn1sFM6A+H2Wb89z908TQOw7LbZQtLAeXaqDojJvTNePHdC1EXc0LmD508aVACjffL
9Rut7dYL0V3s0FstHGyWkKG0/aIZkXpz39q7BVJ/ynqZ2ZB4lMF+4iJWWZ8tT6kBqxmgZAp7bJGY
e+kOnxJ9Pxwhe67hRy2bWhHO1O6N4V/MKB9kCfHpRjlyBH8c8nZtXEaRqWqUx8gZAsAbNbLvsjY6
A+TYkhi/jg5XCAXgP5ghmBPNOh139xQLJVS369vwDuwO9y0joYvdXeE8X6AuGizcxfG8H/bVgc5U
SneWDkiGlcwI/gpMFoTrWdZPz5FSn44pCrk6unUD8E3jzLdV958S7sBcdukQJ6xXBEkGL5XAucTR
Cj2yVrnXehzkCR201J8uCMfFjkVFYV3OxgLGjVDiJYWJ0dxw2bXcdkzqLDnbH2MKB4ZP2SsG5yT/
gBZ6djb/ZXNa85tK3uUiY3hN104UveCHhwBOEmbNG4JfPr7QYrhKssjow83tNdcZa1vlXWFbrukM
8pzNU8ArZNKgAn2M7Mj9NZ+iHu/5mgEkBcT73vj++7pwN3Xz18X3+NojF1VUinJQSVAZAWw4O1go
aWVKFyQyYhFgd91W3AoDnrjcWMKuyHhxZAiRVTLxwUe9tPatluKKsHowi7jeQaWuVxUIYZHwuHkM
4QEsVHjc4Ah/9SW4xT3b3+H6RpDT9R8bLnZrtEAzypPsarDD0Fgjw/WnNRteGRiXn9eIkWYNRxxx
Ho67xWk4pKzQEtRFLqBmNlK9NT/xWMCzqZIYWOBuX45ZVlDvwmH83f0OGm+oJ4zaq8C4/+1z8Uc+
74wHizUxztKeAMadetZM8W8+fvc20zsLlWTyvMGqWmB992scew4t3yQDlei2jTCJvef4xNkynbtL
qEQRZpLfoFxYjzxB9O4J+Qa1qHotrJ47gnCvvErrXPqbdMUFhHgRahDXjOV7Xy9jwJ508OzugrdO
4/fqVs8f8jNiMFBoWFiGn72YsNNUEOgo+hfC1X7fI80ZfGbr9b+G38PmLok3vxQ+MpYaSqyKJrj5
J3nxCiSdyPowRo8VHvZrrmatRJMOAT2JDIiK0kHg8E7j6iR41n7aB3vayHmDZUO9M5Q5xunG9qcG
cIcjXUuF/YtUN+WekM5a7sOUp0rmqppw3tHPUrybdpsA9ar3Nbs/3eyabJRDsq5DT5rSn4LTQtLz
NVNJOdNYdGOybrSfwmBBY9EyQfOdsNMAEJTNCBKrpx01M2VdEyyRC2cl/8jlEFqfGBpek/NwpV1o
C1Q2DfBgJ4HzBW4UreKUXogD9MEQ89H69KZX4gW45lWEriyRbzmu4PZQDhxqZACP97Y20aI1OvDE
FboPcPWjQsLCSDduET4zRkgktiF33lNyJCfJ4P0MZ82kdeTf+CAiY8RyaMflHiLCqd8xueA/ON6b
4sBBZFztHkiFcd5ngEHQNwYDFUCb7zDazM5u2h+KrrEWpjPqeddSoIhMM0+L+maZunaJvZItpTXo
E/VCmZULcotIKiMb/zpK4Zm4IKO7zxZXej5RYtDvY7WfNKzAFynNwsor52mik+AgIrJkwj9R4e9J
d4EqWn8yqO110JPQxqqFqLg7Q/u+17Mp6uhBPJjTJF6pC9NYoUfD3VO+T2s85eykG68sdSfLv4gg
Ox+giPA+R7q8cgvfFxb7xW67ColAu2laDRJntmUDPckN/dIGzBzqZJ9CvhUcEXnbxGlfBxuF9XUo
Nsxbpbov2AAncsitQWi3O/+NnyvrSnz7CHakoNyCnL/gwroxtZPe7LgGCQJQJD2yVQtSpBd/Z1Dj
58cNc3bcs8J9JLbY5PUaG2r5FHsfVimUjg7wXMkFQ9j3t2KNrOZ7polR/BKA2JtYe226UYethsH+
WN42tjrmkFA4+6yGPyvjF/NXD5fuqRu98Yf5PG8WZ9XNQy2dzplKC1en60I2x11Yjq6D5k0f1LXe
1ySiYmTcAtLQff8OqZCcuBDnpw6HlOOzB1uw7yc+/3ap4u1QD4QKLY0TralXgy0Vf7nprNc4BeaY
h/PlPmKLhd85qVZVsjXaluHIFTYZCH/qhwRN5zpmoSfLxF3b4JeMX9TnfQvLKDj9Q+OqUe2RG4lW
9QxsEJA9MZ0IuEX4Q/4eJzF/v5KlS3Uqy7sc/DO+c1s0D5nDVxsr4QwiKpSZUL0ruzibU9POw+8n
VYnQV4Q6TxfcLMOsO2OvscX2gECv25gav7PD+v0rwQSj8g4Twb6Zzfl9vY60E14ip8SuED4/P5pH
qmoeFJiFfRiHsrz8zYPC7uWhd5WR9eMZgPlPWJLk9zfZBSisYZuQf1PcjL2SOX/hRlnr1TaLPCOs
JLSeisQERFVRAzHmlKHPXh8zwCyr3nJRNRoJhbxsfwA1Syj81qwBwoNWoriv83e2XrB9jQS2pNwk
vU41dcIwUAePizCVG8jn2dnvPAMAQ3GghvTIcxhEw2Oc+nWE4pIfM+e9NhpKUvf36JLEc/CF0xSA
mXRwZwas61dDqsf7CjT5A/c+uMh3jHObFoqEwO115JN6CX8NJ3fwGQk8h8nHzk5VmzS7oz5mZxi1
logGWhpZKudfF4dUc1tlKRZ51E3WkvrVqC7SQG9j9bSR1gmaKILxU9CMciGeMn0WMvW0MIzWtT4b
60/yHKW3WmBX5GH8yK71Hd8aPMHnzUeamH/XkozKPjdsAimmgL5jNB1aVp6MLJvGvK6pbuxxmt5H
DdatB4KTUbowUz4nhnS/g2WUOuXBYD1xjxwgOdiEonqP6TyTwvwu9DQS3FXTA5rr/kag7fw//onR
uuexetepGopHlfiddrqDhfomjCl7glBkh/yi15cJrBtRC9sfAvjDKovO+32E8zi3O424JfXtjty3
5LeqLklj4BNBTkvcFIWgJ7OKjccWqwwkL+KobhOZDuobMGHrnmVA5SGJVUZkFCcKhJ3Jlwn4bVOK
oV8x/EHmZVU7rySCUKCGpKZlujwtzk8wjQv9tBQyvelAhoXBnHhCCX9+8JjrrEaL9ZYeFEr+mT1x
t3ot7kadsl83c05t4emg2xxGq9rpnV/y0wL0g+x2gZPCndWqAf8rJmPf8zTNbQbiO0uzaXIdU04g
tiFuIXxjAWjA4L2BP1VzjCMIeBcH5VQHS9E6e96jFQDwLSod1/Pst/iysmocEl9Obin0h8F2A5Fb
Di2NhZu5WcRj9vPeO/gjKjiWR3xx01zTehGBOe8eLcevCCblyVpvSmxbqAzQI5ta170AMyn1svrQ
9RWzygzz+reD62+x0CKB3tFiNQ93Nu+KcSMBRbN9mxhQcA3oECUfqA/HBwalHxslWiW39vOc434J
PiQVZ3cxuZUd8miikMSqnMDp1nXUqLt3iFM5Uk9NnQBHCMqLDLxANzFSfXf3TiNEbmsKXd9GlCtx
J+1Dv1zir8tHmwOhBsbQKtp3DRiC7Gpum5XBbK4yX+aW+okyUhaPMv7eUDtXdqKJn9sgMM6+W1tw
dZRq9+2Td30kmwmpijdWbQ+foD9myJcuRA9achl0Q4+1o3uSpeg7o9g/UrjFOAMwgHK58wTPuYGv
3zIU93bC9/tATJfsHi9M11uwuYMG/lLnEMiUqWNWxVZfh3Gu3qdAN8mOnBnc0tFrKES25j42aHw+
KxSl5w7DVzP/upbY1e3fTWcKxdagFaKk7LEisTpTj04iZJnlKPCwnhRk65NaQKpFYXD7ZxxYe/3G
EMFU7zDoBCri0tWeBMUfoWQTGsCQBrb5RWgGHPRcMcLZmRLz2+JQrZ5nVwVPyD4TE5ekMwyn7URU
HC6midsclcD2fGf7JSoI7miv9cr8rj20lNQXVFuIY6Xxj6P0syZ6zcAeb8XPZHKKZB17vjFeZIGe
rqE15IRzFtoW4n8yNphzI4PeqQ4gdu+qBlarFAifttfKs+Zb09kBk/In/QkabY/3L9vMK8mDvRRO
5XYSL1HYKt+3hL2zwVNHZRsG10FXyF1zE1rdbxEASL+YUP4owNnYeQlCuF3JVwZVk0xZpFoviP43
dRyk5Jcl9gBtoPT15kL3fJtobkQHpekgDRCCNh2olsOQe72mJCKniRF1i5RCsmRvAKJL4aF+FM5v
zldKNGkGYLvZdA0WFbzt2slMZ+TQsXskBD7BIMyy52cQWTaWnBf/P8PiGaPH23ewkwto4plaFY39
O4oRlgxIjdqZ3N7qt7Cp/rtUHgfmY7LJ8udE/gzF2pfuZxUrwiWETLa/2vU2FghXFOc++0gKuvqZ
YgZAtf2nh1A6w2EoRS0v9fqPByIgyz53RK1Lne5b040t0/e/FGUg873R3DI/B2pTq6nMiJbObJGQ
9K+jRL+aTqoSK572w/aPBPRQa89qZqWaR2mLSzqL8uqx4ab0tTibljlzvBIrfA+7tYSKKX/yWPeM
8CcUHHNktirv26ZPC1E/0iubnsuvSbeOZ5Rjhc8tKGOcPn7Lu+bzXd0fugpB7dDbRdo5W1OukDDt
X0o6NhOtsDmoqS6Bpz+Ko4Xkv/OFDWoalRTJZzYP+04YWN0jdDgIfJpPNV6UglzNGWo8V/rZSTUj
Xgbs4rnYQNE2qM6MboT0rhWUB5/NY+fS28cqG7etKSVHMNLnIkgc7qexitEtMUedZLJkkdWjdW6p
+fca6PGI+PUAyIi+c5gDmvJeQwIJ/bYN6gNqlUsXGxSokQBeZptTyAwKRggQl1i4YS/8MKo3l8G5
/ISiDP9KWnALb6mm/V4lRlw3/aTZLwFgG6gi76QIQ7wJVVnl/9Kp00yORShe+76y00b0TRjHiZl3
C0oGpdqs6YJBNI51V7q4/ZG1wxMD4fjXk8TOWHbfVFCMNnI2A9iqgzrA4r66lolEJE2TgDxGlWTS
czvdZhzce2TBIPuKAHG4J4RLWV5jBTy6SGTPANXxnzWDeYPrdnATNOH4+1oEMmIz+2emFKSLO7KO
PofUuJ+CFpoRsXqiuf4yvlOtqfP5QpctLuZuPxNmiI2Vt7LW9Ub6zhNvZI7Qh6Nj+viEHQSR4otI
4i3/RXHBcef+0g6CZESFCrX8Mo+KAZqkAfrDSrGPJsDlnsyO8f48/cBcQMHSIM6MPLGfHksCut35
3yy3hTmsgFe/OISm7ScUmcrJYmCDb9aAgsHrgJMcSd7UXPMV/rEEB21btgYyk81Qm/sTW7VMVYPp
Q68DclIfJI2W1OTEzZ8Gic9QI1RcrbOW8WpMCkkWnQp9zuyxaJRlZnLERzMQRBOrdn2yiz8U3nVU
CHeY907WpahxiLjFLWrIRicyvu3Kq3836C5suDHZSsgJ14IeIVH8YiO01RAgeUw0F1TAmReHNL5Q
iaPyCa+VINKpqLbHQzeWvyHuWpNx+3GWRK/Ceuo57AXYccJVLAb8v1G1j5WQv5jJTsg8x6cMGmMM
Wm2Kea5tW1f5SktnYOZbJJaTulvJkDDd+RHCXmi3KGvpXa0b3DU5Vkti2w8RDRtXuicGBb6vatPa
OYf3d3DuOACX3gi7rC2SqX9Rv23fYGAyhkPTfZnWNts7R2KaYZB+IAYpDattwzc2uT5lNAO4BA5x
h6pMRipe4nvgh6PYxpbpxRYqOKuX5Cdy9swxl9d57LPI+AA368jnoF0I2Z3Os/5hiRIMjrEfXkeI
4+EUdP1X5EB2/xVqcJ64DmBsz+jm+4XHOkdM3Pdjsv4ORb/smiqtSISlliWblVoNu4gojTTEn8eh
KsW6B2H/Cx9UZCXxo69lSWMdcbiM5hEipsD44HdK3xrZTRV0AOWEYC48hpHwMaxeJEEaFzI7LHqc
7X2Mavkt9x/KiH0yVH/ma3+Pbbe+FMcC4NvHBNrJBi5NIJm5UHwTaXpH6AQEPrcWhk0zmswaw8Y+
/+coiJqSn+X3glOkXm5soVlfngGJ8lctu8sfz+9U5nnVgrSLTrFXJeG8Q1pgyJxdLzeYbFXjjD3f
b470iIzJABfpoaOVm/YkilVGNA2aWiwpqx/V9CPqlHn7cmuq9LpTlyC4Pz9uDU7ywao48OW+E0Vt
MnQIN335Z0aTzpNyBV+dfWlK23f1UpnHptQ0wMqo6CMO2j7AEOu+qvAol6HPB2+xIJQ28XTOtfoV
D1LKeX+ABjRmiOdhM9ZGeG/TckPK0FLI22HCrra6BHMQPnCXmQ0NajIeLxzAPK0kPP4StZjuisGX
yAc6mYTrjJ1GsV58Dag6bDtXOPt4y55VFoXt+5nAp5zLWMogCW7Qd4uBSQLK+UpmS5qmeiNmgvaX
sjq3DfOdw1ALSb0cJ7YqgyrWRsy0WlzalaGBaA8yE2FoqxAYc22rKTSqlmtSGYGbWrzDh1Di78pi
cRhj9UaWiDOrX3V9+deEF+SOBbUf0ojn1hHlA5w1T8vyRDoj1pX8+5YJLu7Xb1g8YshnjNQ82AVb
1y6EU0KJSAPJLwj76IyWcvwHtfdLnuxU5eYSzoDObNBqjClopIZAUwZ0lHl7HSku++89xc4Dzt44
c6a26ZH1EZVMcSFiZfceW21libajOMCmuBCy913OwqZkaKc8a1UFzuwcQu1Plpj95E3xd99io+UO
6Hdg8aP1zQh6ioIfBfRwj39/Z20KUFLKuNf2N6DmixMUWfFn7q0OHh9yv0MS1x27uxucXGR/QuQH
O3b/e0SBSo3dPAAi5UEf4bQ9KQrMO6zyUWxVGLfm7woe2CU3v2CBJ9LwgIdCznCCCBQGCQHZE2pp
ZNsXkzjbG93IQATlehwftVA3jhBD8+ieeK4KkroStfZknsd2aQjEquf1jJ+kzn30qFjUzMLAqqrh
J4XuVN1ClpUemyumDw76hYB5YKq+4Q/HUEQRKIQPEmWcePXH8aRuGx/Uu/B+nA7B3DNQTG9sC4k/
FAwmM0IG7qdxCUnTeF0GlBsHa3389RaA+q+XMPVs+vDbk+Vy3mEppfTp9kAruh93P5H0zdyysiM0
ka5Xg39xoDyHWUfF6ohZsvKb6viZBERinrWCzmLHLximLFAkY4kld87r/NsstIxczPCBs/AhX0Pb
0YushcQiVodzwKGtva2TEsGYTe/HlgEowRCIkgxBQUspJ3l2heB7zHMR7iaU5xC4Zno1e8aaktBE
kBs1BC/TmK46VbHBWdAeqFv2dHdq/1WSACZBKc34l+7vq97vJ6hbm7V8srlKUDccLLTIpaXlTv5l
1jAeRLQ3deS5cKjT74jd7vRzDGS2aEEsdll5DGyrF8jS7SiaNJCCWRussYWHHLWDGVtG5YTwmswL
L0FtI9jvpTtU/TR2KHQHXM8z5evSBZ6fpUVrS8GUkZ6LqQOjAxnWgFGyYYWI+q2sfPm95f9yax0Z
YcHzeDIWnxoY2Rn+Zx4Q7coVX2DwVP9yZEZQgflrSW4sZ6SP6Atu2U9j6YrKyl5sxrIApyZ852r0
molBhaEWoWnAkOMTdK9pFtEL0whB2pmDYllIhq445kbBBd2FVaTbvMZEOsX0icC+tIv08jRtyBXf
DuhMqWqCOKbugHxKYQui9lHeybol6u0Vnzp2mkLexFFpCSAlblgNDoIY3QTRExbYbRte4lJ8cpY8
gPJ3TvS9ZkTaB36S3K5FGGfpjHSbYinObS6MAKHZw+kt426Xad6pRLcQCtcI/ORDOF0JLyVIGniH
wAiaS8Dk0Ptk9jD7uPq4jh4sexNoVZZPm8hpwy8ICl8h8p31hTMSzwIrAg0Mo74XBs8JDX7L/T7L
xMzNCblD0pPZMfyX5J5BUeRQ6Drcj64ML6mxTjn8Y8hcirmGx+ZuOC2qjwC4GBHlgwBp3cz4Qi5y
eTPu2FNWa+vtM9kNfXZMU3JFNBcAmSJYxnODb2MYGcGovVhchNnVpkzJks5PRvD6qLRdlZoSSrtd
vkdyMhd7gKOqk4uhT4E4gijdUVPjxGlHu4IGOQvbL/2mGutd/D52L7BkzsccZQP4L6oJ055mj9x0
MKTQNMwydOgxt2hREpsna5myZRXY+AecNxScqMe2ujp3QXwi7ocIlOvN1XE4jLtgbINyWvo73Z/F
gECc7ZPnqsKLU4/GnyNtgc3T6+U7RaeMVOd5FHFFhKq1GG5twLfebBWNmQwwIC7KT18lyPo/ICd/
uqTCWVjbj9CnoQc9EX5z5jLTsgGIYr3+C2WkUagemuOe/3UUK+hN0C1vK1Z6hswi0pe0P+pnQnqO
L7uD/a94e12diUdqB5KNe+7645U9AOYXcKyP3r1bzfx7dddjRCDnOLTI0He+JzE3ymLFTHIFPM5D
dkYF7j1wy8DxxFC9qev7S2iSpisVxIoX6XOFEMcxnaZBCTv06IvPagGvyB6RtNdPkUBJD3NwP7Wm
2pO2HsBwObKiVNU8MHTeY3lMHbUSz6wLwKBXpNwjllLpREngO7x5oC0mjqJ9mwhZ4hwlTptNHMZW
nrpQr9Ho/PQPixd1zzti738Rmq8qjVIJWrlndKLMJdIeWzgP3qey///0+a7MdYt5x+c673y6W9lv
qSbgqDgfLhsyR8JIw8Si+RZes8PCoUx+5lAKRGQ/HlKKriEECt0tl+CORjC4Ehh0Ok3eCvgXe0ZA
80DjZepo7bxGt69nwNpsBnU/QCHSQJ8IVccMxMnpkBdgcHynYbNgWehRQ8i3VJwzyC5SWzdq0Dsi
/OTmJ5gtXMAFo2BYtN8yWnOwzCLlReWQiU5rCfgsj0Pi2/mT3z4EPdkGrGsbH1yiSoouClVCzKuG
m/Gl3KW6aclsVtUIWAFXNYOk/nqIVZa7XZwLbk89Ui0rEUw8Ktx9or2YWtfGQy2LBWBhHuTu76rr
N67wNFB5D1VU8/V9g/e9Ijb/nkhwBdzsK6FBsEIjgkUVjT0ZuZBDsJClBhVU8e0JQh5z77eERXne
k/tCdsKbMsf5RI1n0i8pL39csNO/UXyPBW5ACvnKbA0T5ih8DLMEtmhNKXF5PXrNNHCg3gVJT9x2
Bwls5RQ1AvJuadwkTpq8F0nTSg6cy8927CdEkhNQSRPoBBHGZKttjORhx2x1bq0wKfwds6LobZWe
haCPa0Qq6RB+cgzYAChFpkdrJ6f9bAgp1Z6OOq0D6n0nlxn8np+AnZywPr2ItxJMv468usF2ZFGE
+I2o1LPhdjgUppjnx/o613LayuXIe98B9tvkapXHUuos98ogh6KkABPyb7uptnf1Ba1vFUwqgNBI
OKCg+JshbZ2qu6HDk8URAN0iIaIJbLr/wCtaZQS7XbeO1D9WKs1d4+fT57Vvktwl8i6UDnKhdW2f
B+H4ett+Nsj2Y6TbPw0mDBe1Jaa+Js8XaFkfGFRh+DTWxHte/wZRE0m3tNLN6fJYG8rXLpX3Sv7M
3eS2cM9qld6SmXS5MF933lXj4G9cvQeOX1hHjGpEvO/lS5SUxKoTNIgIuZboRIBfSp2ZS1WgBJd0
ISHN/M5/QxfJvtlGu2UPjnEasudn1jya856J299Fw7nwqPqQkDum/lrEBb97D4wX4dCwFhseK03V
Z8cfIH9h5DcK8o8UeyI+Z02ua9NXg3RKOVX8Xj7NZP53hoEdZqrBWYK1Edo9U0I7O4aUEJ38s59r
rjFeXRwPOOV3st0Aotie9cvFMLyMCTtWIoLgidrD+QCxwJ8ZRSSjWRdLh8YdVPJh4qCuDLidCp7K
6hut7aC9XPjaLp2o+5q49eYkoojjB+q4yvIbWgsPSLVb8z+lfVx0vu0ES42oZRBZomOZDB/rie2C
EVTPHUTGNCmrdvRsay3vZZjag1vBuPgQHnTwCLuaGMKJJpc4HZyCPNFK6BEUP1o2Yw0aa1Tf/2wc
1PKjDyOOhw9u3DrMAqDRtN6+qL+ltIuOu12YrUnnIrj1lUQm0rlLQVpASIwv7I4dIn05M3rp010+
gBAa7M+GJgjJikCKNsX84PdTuifqURbbTWMfXq70Tt64eJyVWYqEDBXnRgq2I4Rwkr+aav8aUi8v
qm9S5rTrMw0d3oowCILKgZr9VHEv4z7CTHkLqFxSc7KywnZQWeBRfUH1CsHid5JCVqAYib/MLtKr
q4yS20qR4X2eHrqQgi2BMuqM3Nwvtr/Aac55fX6KtB7V2AkSf0w55kWxMnFX0Ck+bByc9Z+WMFtR
SlCMRLWywBrBz9ZNSnTaBC3BveIvz2upQga96tgZzLMUMcaf+GmZ3EFZGj0jG0lUTQMrLaJ60x3E
rLgJarpnle57VkA4JyBZnEracy4XuW+61iZ2j/UNJ4QH3wbPnpXFmbwuq7enK5V58umoE1hfl968
A4b3uopMP/2ZXb4e29X2TPK4gL3jw3jYHvsfgwKblp3w0btK2Zcp9cZKcfFPlWqzQZbooD5BWmuw
nMdfGNimwH0oazbkQA3+VOfj3c3HFauWh/IIWwK42D2AhYzmwLUh2sz3SMM5dM8qpw6rXWkYWYEY
zGPK93tegPqhDnvDnZ6kD/22iIoZZOjtXRAOXIdQ0Myqbov9HajfmMBjSH08ZCvNu1YdtzDAJNpR
CcTAoH+nC+oRfzwqd3bRgAixrVL1/p/OsmWcZxnuAfqUbSjDtNLj/E4kPZYcSyhfe1rjkBYwt9AM
Y57xBJmsd26YC2/KP9NU8sDHu6/9hfXQHUdVjYY0h/biJmCMyuhdybJ8eKz7S80yOUWOp9RGAk/d
pvtrpUr+eo4YfZOaUZrKuOm/i0nHEKwyEVOJvgH7aEqsIwQEW1K4nVtftqZNLlf5/n9jtfhlWYih
BAc0YXSCplljZpgFGo4jszEyaWdmLtsegXeJBiELEVzIceDAj0R9EdOSJk2Yf8pmTeKJZoNimqU0
8Few/V28Tc0WsmI8wAYjAp/LXkvwiALIRQnyvDWx+XMGRffEK64ciiRG4GbovGe6dV2qRx/CvF16
HU86NxqaAYM8w/9QaY2EKU/GpK3spvslM0h56HGnL15Nzhe/WYPbIqLvTIer987/ohdsK8P7r9O5
IsSaGEzuxK5YaYlAXDPrl9d2P8XbtVxe7b8opeVDlR+e0t0LQ+6sv1GTTSNzme/GvioQVFFdsL3s
62zGqlypSbkC2lmFiKTg4W/7dv2s6FGXfUY6B1NIqbE6ybySMnYQhD5x6DBKZv38rb1P99nRYHzu
AfKwckMDjZTs35hDm2QhdTHmX0x+gzkJbFxgZ4dodXtk9C7TcNnbA1cDjETRfK0oq6T3AJ9/1y1N
ahWZve9+pBqQOQneHSyIJ6+VpboEOW4/a3kVeRhkqZOEGlmv19ovZntcKeiqCwFDpz4AlzlKAztS
XJaoh6MGiR/yqSXISLU4Y7pP4hXTb6wGBDAk56hOKvx2DNExfJfYMzSY9QgnjpMvLw40rqmwUjIk
jBlsq8wSPICnRJxHtamJeiTRsrPEr7kWD56LgVEp+es92i4a3aW3F5EHXykcTIqC9G7/7o67sSKm
TFjaX8o+edd9kjXMvmXIqLMfHBoCmSWkioJQlYbXWcITIk0YHcG3TMQG1iAWonUFl5y5lkqORUZH
AuX/2z/dRhFtlQsRQhOcvwZfaV99zOnL2kjcuCI+mx1f+PgHl395rDUfbofLOYNwfuO4b49ewEXJ
32Qla92rdQCwlLvZzzghU2OZjcQhvYCrLxuzu3XHoTYqh6lfLLY/sxGX+EEvSG1BpZXw2UojRaaq
9If7wajPvCKktqILi6kZbFpirW4AxlhSKCkgmjj5lwMoXhC2LXhz9KOd7kgBpHXYHhq/hu7TM4VY
Ek0JTIdfJ7SSc9h5WCW7ftQKZMFwAcqfwnoi12YoEtR0plQFnkK1zvfC1ouZ32M+O7LfMjVD628/
bxetVKes3xOluhnkfm5w6sboPKmbeM6JIaH3xhrnGGCrBuGo6bidAz5PkSZS2fK0bg5ZZx3miIQn
MvX7tqjOM3FmKHx5CxQHMM3k3yt4e7q4Av+WmAW1D9R83Y1nQTgRkJoNBuCtlmKTpMHbCb/YzP8j
HM9m+ZC5RKvbq3RoR2JVQ68NaBhdoaAF9OXasx9p2cwJTaCsPAfzjPOaad6bghwLFwimX2UX0Wy8
nAiHMRHiFLTPgC3C7HxAe9skHn23QbFFow/J+uRH3RMNMkT8PRByMPr7qPCDw1XOCTmknsWphIPX
T4BCUK5Iqvyno+IVMrABgxkywJhRLvi0gcvvBACobYn0GeKv5cNIMnmZmAi41dvMQlFcYpGgCtfx
+NUhffDlk7wpjwfBAPFDyOxqqtR+EMdOeSVh41XvybYrwpvA33Ti/91BJjToig4KBKmR8DRxG9SC
rMT8Cpbar/0QYP6GYLNAPTH/q2HPtgZmqKAAVIYJ71EpzE7Zk3ZSLXiZG81Z0YTTHboT08v2Wj2J
rC9teTVLcbDmQPXRrA8OUsG1tXhx+h5nkHgZitYktOyZz7jTTEiKDd0P+JmxQpEtvEETLO8wX3xf
j8CptGXx69VPQcebVljae7uzP5OzcoDgFvm6+QwD1eI47yw5M4vIIzRrYls8ZA89KVzYNI5kSUWU
xEqX/MRE1hXqc5I7tBfYChHXbSA1QDXHujot/tZQtIklqAwmBs90XVkfK3BKI5nPq9ZhG7lDxJ6R
nsYz3TeuHavSZIwFIu77jb76PL/9sSdwF7EW8qpfI7Ve3gkdlYiM8yZR2AdZJUQKuE16UNYWrVLD
frc7CuorJOjSQlQudC2/Ubqd2ljzBEyi2cAJ8yxxGyM3k6l/8deceVn6exnBK5Ma57kEgm0or+i8
G6v1088WpsJln8kdK9BD9DEEk9V1bxSpUaspzyIpGSY6QNvQrhX3+tN6EIcDy56Qslx0wBj/eSAR
0YDUr9EuMeu5vD52sJGPVCGIJzTkxA7N902/6Mw9yuAD6fUu2fsBlBYPHYUUJigHQSpMPx2qOlbX
zjgc21/4aoUHTYBZS6CW6CHvBiOr85hMbHMfmoXzGDPHa764VE3yaV3/BoUF+sc/pxKAXj1midrB
cWpzdAexCLyyq83m+a0CRKFSIh37WzJGMcoyeADxnFoON0fT4Dr//t6V2mway6iUVfqrQExFAT6f
XBNZSMlffDiZzZegWE5QKUu02phCEi3wQvmW5Q9rL1DlflY1dRYK5dDWf2cMS0uPwAmPTm4xjS7g
00tIMRw0RD+SlBNumZctvCC00lZsEuwVkyGtjvJRtVVxIceHw9esQ5E8EHBXIKY/WbjGES17KwND
mBniKg8/QaSModeO9zsZslcrsf/sLNkzL2XGye3AXYiXZzzTjOnkOUpx7c1912akhxsz0Wjc/WmE
x9c2JKmHysWX6j/mfHy0XNDyG93ezHkP5bLpjp/+WrFJlHK2CNC7M3cdgKGOwbk2z1pibXZly3LH
IqpLnHtoXLUZO6hBn2HaymNbadlBPOcE5/Wpytnk4oE/0Xld4D5wrgi8LeOW5hOKPAtV7MstRweg
4sJ71nQffBy26IGXaFaeSBqv/UGohB92jbigp0ZgGfjENCL/LHL6ntjiCnrcRtX0pkl0E8Dyr5El
LByiWzxGAZXYQ7CIm/B7eliHVU5X6tWfIHtR5v/oq8rTGO+SnRlb9yXRS8kL6HOGmSHM9Rq2anmk
WbEHS/woVcIe0klTI/PXI6EM/1bTY1KizV0lNbLl8uZiAwvqxppLn5eu3DxqlIo/qQofLy3rIXRp
hUdVfDuLrqk7QMlSk0pWk+X/CCPQgjycDKGJ/bcK3Kj0Yv6M61tfx4Svgj77WyfNMYIXBoph0ymg
ecc5zXSYavP5HNWS87hJbeWMRvoSryJKUeep+m96aMPgl3dFfT1CUz0GXnJqiTy09UT8eFtTqwDg
hXURRk6J2xnGP0EbYK/Zb8GlrTYCeLrMdBeJqQXHXKiKEw9XPbWpsub2NAOyRfG/D0H26Wl4Bpo5
HUBFmjrkk1EZM4jo7pwVP1DQGzrQu12OFgw17m2QScKEC9evuKRx9or/evpQLbhG0jDtWABYkSKW
jYWUi+mEhPOjWVowqHDD77IphULBGiXr1LjQbMJSalVetfDWORYWbrDi1JGZIJdQmP02QyPJPh/q
qDwWUIqSF9rC1xnfh/7RSo5n5N7IFUBVKqgGZIEmAld9xWDt6rjV67OTqNkUhpEVBRLirkuch/Y2
3gaa6Mn6ZcrjSesVyPUPxR48ZgqdSXrSHSvkC3K6cR0Ra/KFCAybqMtaWDh4gW5uXwZHtcxeC6n/
yOmSrtA+qi0jYCbRHyBOzSENkZ9pwbw5xkj/915W4i6k0zAyGNqxIzF+cmTYQIgoOPiGAffC440x
frBv0VXuptjXcpKyGdZS/biHjhVLE7Pam5Z8MSQTgfiQ5VJW/9Ez1YTunGlDQXM1W+crThULLt3Y
F/e6GFjLo+XmhES+lGWcCZBtLf6x/RSlMSEu7ju8Q1MWlY0EjlEnaYsSX/uWbVEOjn2BuamKkcz4
LBIhBazFulSK17rV4Stb5BswW7fusfG4WGzu/meFk0ybuGy78Md4MFCAyDxWq1imwIRijFPdWreS
KOSWoV9m9KHOCyRMJ/vicnDgI+o6h+ulBJN89AreZQ5gONTZnptweG1HJ3TEX3I+utstQUFienJU
vrvZiH8oEsuTCbSMtz2lmKDvS9e5ZAhnYDM4q0LfsWKsnyAzBr2dqYJfiQquvVEACkSb+7uE/2/j
1Ug8/ssl0W/h6QgCWxBEa9tSfCXB8lK1lNbROP0yTfvGsU78czjrMxOt4p743uNyGL4kSFPHF3ZA
feqEAO9HNcRLZTdVW6+HclAoYtuifdenELj/tyQ+rWZ5XarqS37rngHxGCbSH0iFqUEM18tmGbvi
0vP4w68okzkMwKEVTqMU6R1qd+0fUFrw8raG2V5Ex+/CvfrMDIjdZT7XBJ5aA9ur9xkirrl0zkft
8/5wj7lVOe8POzICxxJitTqwGxwodVDA5Hvvg/qC9bXyRE5S1isdQtj2HbPB40jIJTuG1jz8tv8d
gFHD58hdsV/UhVvHKwRcRNlsRx9TDZgjseAt4KpWrHtNFJen+0otUu1OG7Xmwx58ekEvff+G+D+6
Dul6+HfYRucU53zE0zs6YC5b+whbWODbFi+XnDPZjQgpLBWpE7BUGNm8ZYZ45afwBILFqj5/DV67
FtipvRVPwZXKKuiEaK8ejpepph2et9zKHjDuFsA2AMUw8ZXbITa3adiYsK9RAxBeQ/7fD3qocQKA
NFamnQAccMpcik309i8uL5EwDMHtKBsvIm7CxZmPUAXDaQ8yondgDK0/8RTQFd2WUzVKDmOxdRE0
nNKjZqPEb29wa5RZlKxQYt+KvA7SiyFJICAs00HmC7gNkZ5tC6hzUBfUnc0zfu4/Ue4tFYSWkPoc
q65Nm0+CwizCaYP2JTaoG3Gn6HIMfsAJC4R2WfeVDoMxToQk8qh/oUzLHNMPKE2r2nnhHXJ/Qa4F
7Bs6SDpCZZXd14zorqckjwt//Ez3KZDM4/fPEld1ZfB3neZsGkCj7qSKIOuNMDHJttcIFAHzZFXR
T85Qyfpowz9jj9AQPI6vaMKhJhQMnLCfj5UINiIMAgHwhddUNKROQWqdRd88Fyu+GJzhUP1scCtc
KCMzbgfkNijnxN8BzukHuyJixh0LrZF3RsW4sJYve9wLMkAM2rU9s6VqCeDMOPbLaXwn9iEIUFez
y4Xt1hOOPxrmyRSG6AFLwUd0+RpmHlMCD74KoGk3xLPxUsGscvUaCo7zKH8KWIsar342bu3eqBxU
frTM4TudTX1fW1LqSl6WGVPoM/J+IUTCxRmVVftHAHVeZJZ2L7/vAA6h2GtZSP+j7U2RNL0wQFAY
iYJWeiF/MJAfou4rY8Sr6121ZQax+t95izbFt4rP9sH6WDfeynFjFJBJyg4sUr6xHU0GqfXor75V
Lsn3PPyMB9BkHDnvBw9yB9l+vUI/XoRS2o/xJRjdOd1loGRd1vikxzQYgM0OozAcQHaYqd+0yz3f
Kix1Vh+Mf2NKWNhlBDiHJt5y0yXnNz16Tnea7ao3AOxvU74yE8TmTsT+GO5otLchKKLm5Auyh4ob
0gmD0N2oUiaIcZG1j+/oSCzZYuNw7urtUoTc25UXDB/bLHsHyTDbG/zhisdgvXca89HUUvHraf0Z
MVVV7GV8PXCJDAyLB8jhkfdPL3QtLr7Gvuh3/Tdy1MvAppJgBJ1PLXWCHwHBSSzw+uEK5AxKk1p0
FdvI3Mu7TBl/vNA+VxwwXiaUrg3xMuiuZnhuC2lnw3+8lUS6k1HsN9MJCv4f5LgxqdDSlCe9cpV4
gxDwBj0JwSji/wcUiM0n59HOJx0g7XOAANSFvXjjHNNqtBy0ZdQBDVlgKSuW2okmFwzUwkXKZgfx
uuKTsiHXKXXAdm2/9sNuB4o58Z4ZfC3Eq4X2DWrZoAPLyu4MV8wYLJuqqAuXpXUze1CBPh25GhLf
DzWoChK04+vQaAqoSo8lDj2lk9MTL8fZB9QXKgvluZrpbRNYHZeK7D1ytqJFjdvCyty/If7W6Jiu
97GMe+s2Crv86JF9lrvKpzFcHM541QqSBEN+tWUZZqXlmLGZdY6V7lqNIYdcineyb5ZGSPa7obTI
TVDlukbFrdHas/9W6bW9YML7LHZJeLV77kqzy/xsVfLY1gYG79JmHqm5RLnuof+juBq9JeNxhoEW
v1S5QRra5q2pX6kesRDNSmwCVNBh8zTPnqGVjRo/+nH+Kg2bVso5IFSzrc03SNgGKUFWxXkfT9iS
SaYrXL6TmOrHB/7b2CHC+ic7hpfdanzEDEkKCZU1O2wW+nw9MuqSO7bnB3IbHl/90/k4iZTCZ/ag
lWPojfEJcRZ6CmWECdAlklSWwkdhTnnuJEKugHJc19I+kRmn3cAMZGpr4Y0WmLaUbfDELMisRjyN
FsWzGF7fHGe2q/i5+8aiecLzaaRGLPXVtn+po0B5zze2YKMLiFv2uYkoGmUTY+XKuzlVp+ZF1onp
M8rz10QNecoMo3A168W345lcoBTB2Mh/ymmAKYDT6ACPdz9GobJ0yFrz1029AY5QUbI97MJsNPmq
FdnXq2aTO6sxRbYQ8aU409Unaq3KF3XI2Vw6mVBalFOUcEn+g3a+LA7DBR7dZe4U4dsgwhOkLpoQ
qjt7LojIlyMNHBvVBgShVBg+WYaziizd3NmUMn27DJmGH5iFBknb4b+MS5ck65MRgeHvaZiRMt4T
ro3ETso5+pSp7D3sfZvXUDo6aa7Z6oI11S8Dv/yKxemqr+V8AxJUuvLEkNFaGDBnQ4Ct0dTpNxmX
KR9XJCGpp+Fqky1QK57DcYiET6AvgpGrOeqo+U44VM5FZuZIMR30Z8S1pAbhZw5eFnDrmdkJubDD
xR4pvmyebmVLiMriKk46ia0w6bI15A4yidKhHexPA6s14KHgilFPJ421viHe/QBkMMnKN3NYAil2
EFpToUQX6AVC9fLAxtZbk7hwhcX388trmK8iB2mdii8wlahBYeiAAPK/AUNtbcAcp4s5AAXZ2ZyS
mtd+ED4qMU4TSqgKA4kIon4ViiR7S1SV5v6iYP4LGwSW4DdeYx7ExZ/bnE5a/6yzXbLqXMt8+emC
GG219OzZe8//cPkxs3IUVst1ENRTcWERfsrhg2AhOQVTnKcW1BtXfobZrrNNVUbzWqxZSBGAjG1T
YurNH6xexWtQ82v+lDbhM7R3JazIt0XDn/R10yI5W/Tw9XAzVwInXb2sZxL8Od1sBRuyYxr2denr
c2kj6rXAYw+ipR3WqTZ2gyfEKWwdTnk8Jdny8gffIQfcrEGs0SXKC6fbWRxu0e9aVr0xhyGj0Vu/
RMSUuoW1x4Eo7lBXUwaoV9tiTqFr9cTffrnLNmC/tJHmJITv5PNrES0ewqAYxQJ5Z5zI4T7loSwQ
kWmNz5M7jcaDM9qV2OzRc/9aaR8TDjbkeQs9fQTE0Nmun4P2/xyr5pNTBKUucncGjjugZ30WvF0t
b/vS2weDleMhfA76d+p6WiRuqAEQ/ux+pngQownGWvjWQVoPH5HQcxjCNY/ZzLmGyTbvvAvC5QV8
trpLT8GqXIQA3xQYcJUEhsPwqYZ3FN22eLNeG3UEbdvOfei+C9lJ6ThMUvS1KAQ0DbOoLG0S1P/+
peg7JJYfaoyi6/ocyoI8ks9gXkHd96zfqFvtuxS99QIjFA4e6MZbr6fJQByD+kgDoDK12eBMaeLF
RQx3Lkcuu7wl9LX5yTpfCNx6CnfsfQqF9A+/50RwDSTJbCCFz+HtAXwHfo2kREnK5+8sPNHHIhrv
4RLbKbvkS6Vrx1RmDw5upEioNX1j/Siqq93DfvmccQy/ViJGg7qnaKLQgSj7qaRetzIww8cbjcib
dfZfUrJV1C3o/hXqP46ZYiMfCDXIpTpRYt6Xv0I+DOvJMg4h+3wz/LdP5VVALyjBrUOvg1OME4DJ
XVp7vXYVtYrx856QAQd9qxdI7dRLPSu/8J5jezp4Lu2lP/E6z3x87bHtO706RFV1MJECHTm7AY2t
XHlaeABhOj/eg9TTf9nOPyHTYrqS42xKQPglmn4wg+hmeZjAhHOgnxQOn9S1Q7o7/RiobN8+cshC
wRKdj4hr17wrNSUkRWWM2E/k05Da6d+K633Emyao6S68n8q8s+am5NoJCMP5ZN1GBavOM5m6oiGd
2rCIv23WgGNR3+9KqbxSQHInlJeMjC7ZUG8veetdVJDu8FUxVD1O4pWy+iJYORnt7CWwmxEvOEqm
kDMJI2y3TCWiAa0L6/YLRga6OkhUBhFtoK8xt2PvkQEQmijYKwQTYbX4cUeA9PK/pb42V5PNb1a7
aFH+AcTIDTqhBNLR+e/8uWYY6cwMMwSz6DF3ndyuD+LOHb0E4VhoNdni8lePmRUMphK2DOTNuBB7
2VEVI/suX0YANebH5zWG5ys6OjOE/Dfw+TQZaW+UMOoaDMep5Y637bklImAWL6/g0dfHKbA7g1r5
d3/m0wpK194lHoYvy7Dgs5fcWAbxd2anoSVAfC61H8abrs+YB+0QmOBwZLupoHGV3LVhVYa3exFw
8s144F4Jl7floKYHiJBRdBEZBzpJa1q+8lZ4PXOjxJvTOhLA/PR1ZODWwH6Z72PUjY9V2krIq9/8
P8FApGpB78NU8Ikev7UIN/QYmvqiwXiQ3ZiAM3PtQm8s/vx9e8bktCRDdvfqe/kQuIAE2q8lN1Zd
z22KZSJzsnCPwZUn4Rnmxdwt75kNgJj9ccwYsPNnjSg/n+qEViZNAE30pSXXEk9iHVn4TPgwB7Vb
ofEqJA/ipKjw5HHh4DwOGAKt9A15mbfUVatR/+GNXFC/D47o/IXBELSnCBUmtNyALjT4aEmJg/8I
942/RbcVAETf2U6mZFfoZ2aNEiv6wpWiCsZwj6tHs9KvJMuYGeX9Ds4ge2UbidEoHbITsagX6Tgp
aTPS3nXgbUOAU0bINUvfZBTHfTO20C8iyp2R6W9hT/vqisC/eH3gLWxLLMZw8irntfQPkx7sjuNh
2ZzT45EKTswc7xmXzHWKf91Xkm3D94Ys6az0Uufz856Qr/AQVcrs2tXn6ra9tmQI5esDKqsFRN6R
wIIOX8JJlzXvAtgLuyZCXa1cVGc+1BIOxGVqENArtgOkrOfBel9E4AHXNvZeYkHbdJU7dyExTLuY
jkIXG4hVgd1uJK/WGQbt6nmlpftIHxbxUV3gTzU4Tay6hq27VOtQr0PQ56Tq/9gNn8HBGD+H/OwW
APiw72CRA0pmHMZXTAIHmq2tKNNqmRi7KbuOoQM6/XNgGQgkLRBESlSkS4dPVTa3ADCK87wuxci+
0lPD01a9F/driBa75Mg1TigrkxD3bz5Mdny78htLuHQn9pfJThNf45NP7UHwa3ecMJFq4TVGoT+l
xT60lZZJtC8BuxN7F4H9GB8a+/TB1nD0R+w4goxES8YtVA9flZYcTs4SORqm+YS6cX4G75royVdw
WcUROwNl+QDDpIzEMWN5IPWYUW+anMD+getMyOdLAtaGPhurt6xtX+slZdIwI6fqvTG9osxjmQ7T
pGRU7+imdJAP+vgNUugX7nViA9yTbhRP3gTUUnPP07AHx0+JfDF3fk/q4J+RIUl+8iyeYhzcpeKI
M4QZzz6ZusX0oLQGcfnajpyob5ptHHO8eEpTQJxJDJcm7LUW4tOFZ9wlXDK5wLs/LpLJ/gJQyTg5
ULv5SiBsk0zYX1EEYPHnM+tzcwt4pCcQajXzhwYqjkNTHCjic+afsEo7/tGAE0Ugo9dAJOlJjRqb
CjQ1zfpDJNb31+QD0zw1Ev9fENnoChrJgN1PtlNrBgpHo1kHVNd+dibtl+v8lIMsqs6bJJ7DOxxU
RWYWs0xwPWP27EficFPxbT/cTN45kcx66PY/Lnkuk3CjUzZqe7RSFyqjPsQIm7HTP6vh+5ZKD4Oz
ljaqcgN3odWhzgfAtkvz4cMjb3X+pn0TQs6rEV3xcmq0bcxYIsNDikFyurDobmJxGrW9XHZ4foM2
MumKIMt7o6sn489UqNPzTRUdGXJrU3DeBUajDT80S5cja558Y92gHp71jh4+Phzno+riD8lTAnha
KWe4d6rPCVqrGU0Q/6ZEOxTffCv0qbQHzc6CHjRsw/EpP96HZG50qB9v0rdD99dnYLBuNYQ0m9Sv
U3qJ6BlbIx9EeJEHSwBWsCUW8Xa5TtvPC0E6247aw6V1bQaVqlZ930bW3i7JNdmLyff1iiKsSpE1
jYTC6AtS+b9G1zz1h+KV2CvVILnRQR1Mdrjf9DVnIzdnQbXRwZ0R3lYrh17WWo6aqqsZxjkPH6Qc
kRs9ftCQ7nGo34NWEH0gn8VlJxSUx6c9HYQg57YTS8wMxSwomvMw/V1otF2FN+23BZFooAp0bDuA
mxB13vIHO4TW7hmRjHylFab4N8wvNloVl0gAulfoABrTi52H8245QDeAsRMzWK2EEXnJdmF+yw9g
Lwh1Vt3cu2T72L2Rn60D3c/tv2Vfsa03Q6MNiepqCTNKwfpfTvkANJPGs408+mI+evZEpJVBwsWW
bgVrfWB6HvdS/yKtOlyCH1D9ytQrt6jqTQaeR7cNE9Emkt4DtMB5ny/3MBmCQelQF9HeDWehuZvX
Qmw3mT7BQqq+L0ZrZDhZ8nP0fld8389rlYz7HMJQZh1xUG57bPsfDmTHukfUtlYOvi4EISHnAD/C
aER7L54Jtngd4T4IHDo5XVDEd7l0d5u9Mz3+Z3Gvzav8sYpWL8kM0V/dGqXh2I7SCzcK2aWMujit
t0wr97XIbBrZtREGSSQWFnIxINS/e6mRz6FCR28ztJfpCjrRxzUCp4d02eePFJNLi0IUnxvF8Mns
53CmFMsYN4BGRYr1wtNHe6ZGpZQlQ7z8H43mv9Eqadfdto3koFuv1LjGC32u5VGWIczsCQyqoShb
0vqF68VWfDhiSHtOnxFs8zKB2ugO5QVcrjrwkWk24rGOFV9x5MNbqgUXuGrxtDuc8jntFBpZzAap
c5xuFHByfLyOu9zg+FXzAKEtDF5b9m65zR6E8TTVpaRJyKB63XCqC6dkakCkh8ozpdIlsoedJCGB
3INGwaokPHO1AzCzSqOLCunkrIarVMYdz4jC+tQ2nymTUuGkMdeAhFLHv+LtvaCptFBfVbSiGJbn
GyN2XQKd9KYXSvu54wdhFjDua3z7xg8ahtUETHOpjhStdxBfPGTmmxh1nu7fXoJ3pBS42iCs+Q7Y
MiF27wVS//LaNv58uaeJKubwdMypQSpygRpqGo96pyz3/27/Tcy+0wcZppLWGdYThUN5VFszQsNC
XrKFriMOAda8sfvfAIo5++T8nYMRMCZgznH7j6InpnHxH81ZvWWpBazqePd3rsSlKmKyismeTC0J
QZqEjCILVtWrraTOxkEoXCwEAVA1BE8EgsPR3Q/RzO9UAVhVtfnYtq/rhEqvVJmU3WFyyYZrwXvQ
VSgyjZr9ik3tv0PU05/xFvfl+mB9k1xfE9r7561W/f5zY/hvHVXWKz0EOZPOY2sHRVx5weap9KQ9
WEkt33irBAPx814T+G3pXhZDPujUUvIEumKCYadC3EdE2NE18oWXDXdoVl3drLIGoUV9n+bBI6Z+
qoSqDSH1IjzjKEJFO9Xko8OcSihMDhqgGzy8E8BJEvGSSeJt0Uxl7soxnGNytW09VbaOMXyKg2h8
fXQTXfhbTXqbv40hVwZIemuVm71M01TI1bn3LzP2FdML50oqjYnR76wLjj1JZmUlmg6xnrONK6fX
Yfud66xeFxK7BgH4RCrw/rn9DXzFZQJP4lw31uYs03VNyXds8bc42Yfcn1FbczkoSC/QwMmlBkHV
3xMyYT/dxmlgMrLAlZW8khSoHDbDD1+HFklMcKFkV/hWMnvO2vXaOFTdwRKG8AM9EliXGN9dBIyV
NHgASA+2CBPk2a3wrsL8ZctIQsY6sVmhDwjXSAc4Tio8KP/8VrkpjCMLH7OfhAiIigBxSEwKcDlQ
VO3Ec1gdhaH3m0RCxBqrBRmol/9GpiOyQHzOu+ygJ5S8yXaxOjZ96hdQrgOkk7rlpy32vb+IHtdN
JjzE2SUyym+gaQD0mS+5X2VFcHCIKjC1fe+ZQBbhMPZFdly0Lqt6bZTExgu8cdUSuOayrf52VyV6
lgrWhqm8UZZ1KEaWxzkUQh4NAp8YmPPjpyR0jMT8ZRZnHpu240vikKzdCOyxPwLFxKbzMUTe5nsf
HK7SuVrtgcOIg5maCvEVdhIsRCh6XQBdd2z0iNzJvWV/s40wfNMWVHsBxmPWc3n9xsKqTcAJ2LJV
N7sjpHG61chIclMSUz8nUVjP4Q8LvLdB4jVkbsLuh1lsNLeq330dH0OuVWcSBq+xwD/DU0wdsilu
sE6w6o8NA98j1fKjaTzgGT6syUvFmG1/cHvbkCxehQTCK3lkICjwjtj5/d3qSFxCmOgLrXsqOwBq
mkIWVMGZljVo4CLDWlxxJ3JL/4FU0c/SAODGBHUkurxf8UsDMnGnZOrlb/nLyclWRh8AfeYiY7ZM
YUTeK7xKMWZ7R93t3Add5bHPE6GrEjfAdkQHmJFr/syL3zRjLuZP2fxkEFQsnCSusyLAsc2q1bX/
7lUdXHfr82u85D8FmiBntv62yrVdkw5eNsK2RriVk0oGMxE/Ek5zYzdXpEq1xi5avXiI3BG+i3q9
sMdQ7byX+fZPNKGbzu1jtfdL6K6aYpTLoIrJhV+cyD9vXJ5NTsFpSXzV+RjOq5XNX9owB0Q1VX+0
qVYrdJQMwAvyWDBHe6n0ZvUcTi5j3VjrbBllDHNA414Xnx6838bCp9k3pi6YapJVVuPYxMMsoM4p
pJSnZ8ALaz48hH3Lz7CZGJKyqZ2dxOvYEtPpZuDDyhUd+8AS8otooNcIJtjFra/2SrUPj9Mj32fc
rx6bQhUFobYSWXYMrpkuKniL9wdRqPMFijwYqGip1CRs4s2tFF8jv1XeyGmWahBdbOWOWhttjXb0
T1UZAAY4KwPM4dUsaih4iBI5FLZgElO7Mw3FpYXyqQCI90cRUuvzwX09KO+YgiXTeYDJAEGeQA0C
i4iE86IFuBzzAm+mvz1t5CXWQ2l8tAk4E92EBShDUdAPi3kmvjZHsgcXVZPkulkuH7BpufrMaOOA
7i+KLLtsnrJnhWMJgX8b46he7/8u2QmmDuOklz9TsmrpMB4X7IYJcqkMUwU6MEzg/ta/nlFtlVo3
vueU6IgbjjYELM1fx1dvcxBP7tm+xGQgpMhB1EWL5Xbt01aDRMOTrG48NqmOk8f54WhQQAqWAKlh
eglhqzLBp080sZK4X+MlY85xVlTTskv7JUHp1rbJ3VGVy7BHGTRhinmRz7uFNvXN0qIczcUeLapN
Mw0Bc3byvQUBETs3rUetzQxP0x1fSumAczsFBWEZRem799R7Q786J7PBp2nQDarugqEyjq6pbwMC
oUzQ9ELV/93Rk9fF7pnPuaCJKs6mRveRkz8QFRkns1kq64dpwv8tNA0OD97v8w/ZNLlsF4WNixVH
kA/e/jNZf+4T3NUgFI7Wa+Wm9REsL4gdESGxF5XMqT396k/Que+/bhH57yTxBzO9WyISoW7i2HIj
MTCfcg3Uifu1ExndE8KjHU48L8QbB7XeilO2L/CSlyEs2CPRILgxpuYDR3tnxi+LG27NrCrN/2Nv
34az/8k3GPXoC4Yn/G2/S19AdTYNvX3KxWylxzkHmvEyKlS55yOncU0QMZ00yvPSxNKg9MtLvO4g
GAQ5il8O/327IgDbglFOZcQYS+WoXcEqsp4+JqvtXImJjC7e+phtVnPe6XrEQ5VG9XTw5j56JpIf
56Ay7KhXcTq74n1/WxdUbb0QejydvT1oe4Zf6CiexxB9V33+DWyMmt7xRBuePXFCQYrYafF0kicv
f3KOH4hQeJPNeAL1oAHx2idOxaSNWoAU/garSY92ylp9AYEm2L0iqDHh6nlcR6TD6CET8FMZIwlz
qENwSF38OfR4xEdagMSfgqxdorA7j7tmHJTtMyIcJYY/fV6eudOOJ9IG0xjwK6ra9piRvaVDSXLT
zroCN9VnddYCRMpAX+W/97L8KMoVRsnYOsi18PagFza5X+wWz5bfTvOo0g2ou7qESOv+h7y0M9Xh
rvMOtkJkZj4l1//+C5y0EI2N2lReMCeDN2DPPHRsefUX0iT7TlAEofs9Y01eIn3zWhs2za6x/TUv
FWWoD98Xg6caYhm96ctq5wMwWh6YFHLXoynRhZsJjGSbR2vjNq0mLK67VFocYQ8jsfRD9fdQMbq3
Wzo5Z28mmY2tgH4sD0fwAX+Vdxj2NBRvaY0UdyQ9EhGUrEyuU1BjY7QPdhMNWGTRE/BdJAK3NC0a
+yWxFGQ2aouHUnXpmF8DJvCQE+3EfhBrKlgm2lshrBbpupztTbhGZS5R6qtsIB/s3LAUTYg002Is
ClZj4aHlQvVnbRSCmUbZHJT7EBDyNPNg3bgwOooAxMTRG/gKHY+710t/PQonib4SeI9MyxpPmTY7
hEQDalGDpI0sYwsMOxhyY9Y7KEmzLVBmBqSYaxxnZhagSheDJfMopvGd1d+dsM9GLJH/NM5FoGm3
SFrx4GpVwzPJtj3M0fjn5Vwp6Pbh+jiWBbvC2zFM4+KoxkVy1a6vL4hcA+LCPp8VDaov7DnJ3JMS
EuswyEUBXHRxn0eh5FkdRfYxtxWb21SNAYjRzeOpgIAu9PLZ9GKknMc=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
