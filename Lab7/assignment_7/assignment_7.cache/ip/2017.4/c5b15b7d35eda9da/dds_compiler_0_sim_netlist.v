// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Thu Apr 26 20:37:26 2018
// Host        : laptop running 64-bit Manjaro Linux
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ dds_compiler_0_sim_netlist.v
// Design      : dds_compiler_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dds_compiler_0,dds_compiler_v6_0_15,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "dds_compiler_v6_0_15,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (aclk,
    s_axis_phase_tvalid,
    s_axis_phase_tdata,
    m_axis_data_tvalid,
    m_axis_data_tdata);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 aclk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME aclk_intf, ASSOCIATED_BUSIF M_AXIS_PHASE:S_AXIS_CONFIG:M_AXIS_DATA:S_AXIS_PHASE, ASSOCIATED_RESET aresetn, ASSOCIATED_CLKEN aclken, FREQ_HZ 100000000, PHASE 0.000" *) input aclk;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 S_AXIS_PHASE TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXIS_PHASE, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 0, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef" *) input s_axis_phase_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 S_AXIS_PHASE TDATA" *) input [7:0]s_axis_phase_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 M_AXIS_DATA TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME M_AXIS_DATA, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 0, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef" *) output m_axis_data_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 M_AXIS_DATA TDATA" *) output [15:0]m_axis_data_tdata;

  wire aclk;
  wire [15:0]m_axis_data_tdata;
  wire m_axis_data_tvalid;
  wire [7:0]s_axis_phase_tdata;
  wire s_axis_phase_tvalid;
  wire NLW_U0_debug_axi_resync_in_UNCONNECTED;
  wire NLW_U0_debug_core_nd_UNCONNECTED;
  wire NLW_U0_debug_phase_nd_UNCONNECTED;
  wire NLW_U0_event_phase_in_invalid_UNCONNECTED;
  wire NLW_U0_event_pinc_invalid_UNCONNECTED;
  wire NLW_U0_event_poff_invalid_UNCONNECTED;
  wire NLW_U0_event_s_config_tlast_missing_UNCONNECTED;
  wire NLW_U0_event_s_config_tlast_unexpected_UNCONNECTED;
  wire NLW_U0_event_s_phase_chanid_incorrect_UNCONNECTED;
  wire NLW_U0_event_s_phase_tlast_missing_UNCONNECTED;
  wire NLW_U0_event_s_phase_tlast_unexpected_UNCONNECTED;
  wire NLW_U0_m_axis_data_tlast_UNCONNECTED;
  wire NLW_U0_m_axis_phase_tlast_UNCONNECTED;
  wire NLW_U0_m_axis_phase_tvalid_UNCONNECTED;
  wire NLW_U0_s_axis_config_tready_UNCONNECTED;
  wire NLW_U0_s_axis_phase_tready_UNCONNECTED;
  wire [0:0]NLW_U0_debug_axi_chan_in_UNCONNECTED;
  wire [7:0]NLW_U0_debug_axi_pinc_in_UNCONNECTED;
  wire [7:0]NLW_U0_debug_axi_poff_in_UNCONNECTED;
  wire [7:0]NLW_U0_debug_phase_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_data_tuser_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_phase_tdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_phase_tuser_UNCONNECTED;

  (* C_ACCUMULATOR_WIDTH = "8" *) 
  (* C_AMPLITUDE = "0" *) 
  (* C_CHANNELS = "1" *) 
  (* C_CHAN_WIDTH = "1" *) 
  (* C_DEBUG_INTERFACE = "0" *) 
  (* C_HAS_ACLKEN = "0" *) 
  (* C_HAS_ARESETN = "0" *) 
  (* C_HAS_M_DATA = "1" *) 
  (* C_HAS_M_PHASE = "0" *) 
  (* C_HAS_PHASEGEN = "0" *) 
  (* C_HAS_PHASE_OUT = "0" *) 
  (* C_HAS_SINCOS = "1" *) 
  (* C_HAS_S_CONFIG = "0" *) 
  (* C_HAS_S_PHASE = "1" *) 
  (* C_HAS_TLAST = "0" *) 
  (* C_HAS_TREADY = "0" *) 
  (* C_LATENCY = "2" *) 
  (* C_MEM_TYPE = "1" *) 
  (* C_MODE_OF_OPERATION = "0" *) 
  (* C_MODULUS = "9" *) 
  (* C_M_DATA_HAS_TUSER = "0" *) 
  (* C_M_DATA_TDATA_WIDTH = "16" *) 
  (* C_M_DATA_TUSER_WIDTH = "1" *) 
  (* C_M_PHASE_HAS_TUSER = "0" *) 
  (* C_M_PHASE_TDATA_WIDTH = "1" *) 
  (* C_M_PHASE_TUSER_WIDTH = "1" *) 
  (* C_NEGATIVE_COSINE = "0" *) 
  (* C_NEGATIVE_SINE = "0" *) 
  (* C_NOISE_SHAPING = "0" *) 
  (* C_OPTIMISE_GOAL = "0" *) 
  (* C_OUTPUTS_REQUIRED = "0" *) 
  (* C_OUTPUT_FORM = "0" *) 
  (* C_OUTPUT_WIDTH = "16" *) 
  (* C_PHASE_ANGLE_WIDTH = "8" *) 
  (* C_PHASE_INCREMENT = "2" *) 
  (* C_PHASE_INCREMENT_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_PHASE_OFFSET = "0" *) 
  (* C_PHASE_OFFSET_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_POR_MODE = "0" *) 
  (* C_RESYNC = "0" *) 
  (* C_S_CONFIG_SYNC_MODE = "0" *) 
  (* C_S_CONFIG_TDATA_WIDTH = "1" *) 
  (* C_S_PHASE_HAS_TUSER = "0" *) 
  (* C_S_PHASE_TDATA_WIDTH = "8" *) 
  (* C_S_PHASE_TUSER_WIDTH = "1" *) 
  (* C_USE_DSP48 = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dds_compiler_v6_0_15 U0
       (.aclk(aclk),
        .aclken(1'b1),
        .aresetn(1'b1),
        .debug_axi_chan_in(NLW_U0_debug_axi_chan_in_UNCONNECTED[0]),
        .debug_axi_pinc_in(NLW_U0_debug_axi_pinc_in_UNCONNECTED[7:0]),
        .debug_axi_poff_in(NLW_U0_debug_axi_poff_in_UNCONNECTED[7:0]),
        .debug_axi_resync_in(NLW_U0_debug_axi_resync_in_UNCONNECTED),
        .debug_core_nd(NLW_U0_debug_core_nd_UNCONNECTED),
        .debug_phase(NLW_U0_debug_phase_UNCONNECTED[7:0]),
        .debug_phase_nd(NLW_U0_debug_phase_nd_UNCONNECTED),
        .event_phase_in_invalid(NLW_U0_event_phase_in_invalid_UNCONNECTED),
        .event_pinc_invalid(NLW_U0_event_pinc_invalid_UNCONNECTED),
        .event_poff_invalid(NLW_U0_event_poff_invalid_UNCONNECTED),
        .event_s_config_tlast_missing(NLW_U0_event_s_config_tlast_missing_UNCONNECTED),
        .event_s_config_tlast_unexpected(NLW_U0_event_s_config_tlast_unexpected_UNCONNECTED),
        .event_s_phase_chanid_incorrect(NLW_U0_event_s_phase_chanid_incorrect_UNCONNECTED),
        .event_s_phase_tlast_missing(NLW_U0_event_s_phase_tlast_missing_UNCONNECTED),
        .event_s_phase_tlast_unexpected(NLW_U0_event_s_phase_tlast_unexpected_UNCONNECTED),
        .m_axis_data_tdata(m_axis_data_tdata),
        .m_axis_data_tlast(NLW_U0_m_axis_data_tlast_UNCONNECTED),
        .m_axis_data_tready(1'b0),
        .m_axis_data_tuser(NLW_U0_m_axis_data_tuser_UNCONNECTED[0]),
        .m_axis_data_tvalid(m_axis_data_tvalid),
        .m_axis_phase_tdata(NLW_U0_m_axis_phase_tdata_UNCONNECTED[0]),
        .m_axis_phase_tlast(NLW_U0_m_axis_phase_tlast_UNCONNECTED),
        .m_axis_phase_tready(1'b0),
        .m_axis_phase_tuser(NLW_U0_m_axis_phase_tuser_UNCONNECTED[0]),
        .m_axis_phase_tvalid(NLW_U0_m_axis_phase_tvalid_UNCONNECTED),
        .s_axis_config_tdata(1'b0),
        .s_axis_config_tlast(1'b0),
        .s_axis_config_tready(NLW_U0_s_axis_config_tready_UNCONNECTED),
        .s_axis_config_tvalid(1'b0),
        .s_axis_phase_tdata(s_axis_phase_tdata),
        .s_axis_phase_tlast(1'b0),
        .s_axis_phase_tready(NLW_U0_s_axis_phase_tready_UNCONNECTED),
        .s_axis_phase_tuser(1'b0),
        .s_axis_phase_tvalid(s_axis_phase_tvalid));
endmodule

(* C_ACCUMULATOR_WIDTH = "8" *) (* C_AMPLITUDE = "0" *) (* C_CHANNELS = "1" *) 
(* C_CHAN_WIDTH = "1" *) (* C_DEBUG_INTERFACE = "0" *) (* C_HAS_ACLKEN = "0" *) 
(* C_HAS_ARESETN = "0" *) (* C_HAS_M_DATA = "1" *) (* C_HAS_M_PHASE = "0" *) 
(* C_HAS_PHASEGEN = "0" *) (* C_HAS_PHASE_OUT = "0" *) (* C_HAS_SINCOS = "1" *) 
(* C_HAS_S_CONFIG = "0" *) (* C_HAS_S_PHASE = "1" *) (* C_HAS_TLAST = "0" *) 
(* C_HAS_TREADY = "0" *) (* C_LATENCY = "2" *) (* C_MEM_TYPE = "1" *) 
(* C_MODE_OF_OPERATION = "0" *) (* C_MODULUS = "9" *) (* C_M_DATA_HAS_TUSER = "0" *) 
(* C_M_DATA_TDATA_WIDTH = "16" *) (* C_M_DATA_TUSER_WIDTH = "1" *) (* C_M_PHASE_HAS_TUSER = "0" *) 
(* C_M_PHASE_TDATA_WIDTH = "1" *) (* C_M_PHASE_TUSER_WIDTH = "1" *) (* C_NEGATIVE_COSINE = "0" *) 
(* C_NEGATIVE_SINE = "0" *) (* C_NOISE_SHAPING = "0" *) (* C_OPTIMISE_GOAL = "0" *) 
(* C_OUTPUTS_REQUIRED = "0" *) (* C_OUTPUT_FORM = "0" *) (* C_OUTPUT_WIDTH = "16" *) 
(* C_PHASE_ANGLE_WIDTH = "8" *) (* C_PHASE_INCREMENT = "2" *) (* C_PHASE_INCREMENT_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
(* C_PHASE_OFFSET = "0" *) (* C_PHASE_OFFSET_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) (* C_POR_MODE = "0" *) 
(* C_RESYNC = "0" *) (* C_S_CONFIG_SYNC_MODE = "0" *) (* C_S_CONFIG_TDATA_WIDTH = "1" *) 
(* C_S_PHASE_HAS_TUSER = "0" *) (* C_S_PHASE_TDATA_WIDTH = "8" *) (* C_S_PHASE_TUSER_WIDTH = "1" *) 
(* C_USE_DSP48 = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dds_compiler_v6_0_15
   (aclk,
    aclken,
    aresetn,
    s_axis_phase_tvalid,
    s_axis_phase_tready,
    s_axis_phase_tdata,
    s_axis_phase_tlast,
    s_axis_phase_tuser,
    s_axis_config_tvalid,
    s_axis_config_tready,
    s_axis_config_tdata,
    s_axis_config_tlast,
    m_axis_data_tvalid,
    m_axis_data_tready,
    m_axis_data_tdata,
    m_axis_data_tlast,
    m_axis_data_tuser,
    m_axis_phase_tvalid,
    m_axis_phase_tready,
    m_axis_phase_tdata,
    m_axis_phase_tlast,
    m_axis_phase_tuser,
    event_pinc_invalid,
    event_poff_invalid,
    event_phase_in_invalid,
    event_s_phase_tlast_missing,
    event_s_phase_tlast_unexpected,
    event_s_phase_chanid_incorrect,
    event_s_config_tlast_missing,
    event_s_config_tlast_unexpected,
    debug_axi_pinc_in,
    debug_axi_poff_in,
    debug_axi_resync_in,
    debug_axi_chan_in,
    debug_core_nd,
    debug_phase,
    debug_phase_nd);
  input aclk;
  input aclken;
  input aresetn;
  input s_axis_phase_tvalid;
  output s_axis_phase_tready;
  input [7:0]s_axis_phase_tdata;
  input s_axis_phase_tlast;
  input [0:0]s_axis_phase_tuser;
  input s_axis_config_tvalid;
  output s_axis_config_tready;
  input [0:0]s_axis_config_tdata;
  input s_axis_config_tlast;
  output m_axis_data_tvalid;
  input m_axis_data_tready;
  output [15:0]m_axis_data_tdata;
  output m_axis_data_tlast;
  output [0:0]m_axis_data_tuser;
  output m_axis_phase_tvalid;
  input m_axis_phase_tready;
  output [0:0]m_axis_phase_tdata;
  output m_axis_phase_tlast;
  output [0:0]m_axis_phase_tuser;
  output event_pinc_invalid;
  output event_poff_invalid;
  output event_phase_in_invalid;
  output event_s_phase_tlast_missing;
  output event_s_phase_tlast_unexpected;
  output event_s_phase_chanid_incorrect;
  output event_s_config_tlast_missing;
  output event_s_config_tlast_unexpected;
  output [7:0]debug_axi_pinc_in;
  output [7:0]debug_axi_poff_in;
  output debug_axi_resync_in;
  output [0:0]debug_axi_chan_in;
  output debug_core_nd;
  output [7:0]debug_phase;
  output debug_phase_nd;

  wire \<const0> ;
  wire aclk;
  wire event_s_phase_tlast_missing;
  wire [15:0]m_axis_data_tdata;
  wire m_axis_data_tvalid;
  wire [7:0]s_axis_phase_tdata;
  wire s_axis_phase_tvalid;
  wire NLW_i_synth_debug_axi_resync_in_UNCONNECTED;
  wire NLW_i_synth_debug_core_nd_UNCONNECTED;
  wire NLW_i_synth_debug_phase_nd_UNCONNECTED;
  wire NLW_i_synth_event_phase_in_invalid_UNCONNECTED;
  wire NLW_i_synth_event_pinc_invalid_UNCONNECTED;
  wire NLW_i_synth_event_poff_invalid_UNCONNECTED;
  wire NLW_i_synth_event_s_config_tlast_missing_UNCONNECTED;
  wire NLW_i_synth_event_s_config_tlast_unexpected_UNCONNECTED;
  wire NLW_i_synth_event_s_phase_chanid_incorrect_UNCONNECTED;
  wire NLW_i_synth_event_s_phase_tlast_unexpected_UNCONNECTED;
  wire NLW_i_synth_m_axis_data_tlast_UNCONNECTED;
  wire NLW_i_synth_m_axis_phase_tlast_UNCONNECTED;
  wire NLW_i_synth_m_axis_phase_tvalid_UNCONNECTED;
  wire NLW_i_synth_s_axis_config_tready_UNCONNECTED;
  wire NLW_i_synth_s_axis_phase_tready_UNCONNECTED;
  wire [0:0]NLW_i_synth_debug_axi_chan_in_UNCONNECTED;
  wire [7:0]NLW_i_synth_debug_axi_pinc_in_UNCONNECTED;
  wire [7:0]NLW_i_synth_debug_axi_poff_in_UNCONNECTED;
  wire [7:0]NLW_i_synth_debug_phase_UNCONNECTED;
  wire [0:0]NLW_i_synth_m_axis_data_tuser_UNCONNECTED;
  wire [0:0]NLW_i_synth_m_axis_phase_tdata_UNCONNECTED;
  wire [0:0]NLW_i_synth_m_axis_phase_tuser_UNCONNECTED;

  assign debug_axi_chan_in[0] = \<const0> ;
  assign debug_axi_pinc_in[7] = \<const0> ;
  assign debug_axi_pinc_in[6] = \<const0> ;
  assign debug_axi_pinc_in[5] = \<const0> ;
  assign debug_axi_pinc_in[4] = \<const0> ;
  assign debug_axi_pinc_in[3] = \<const0> ;
  assign debug_axi_pinc_in[2] = \<const0> ;
  assign debug_axi_pinc_in[1] = \<const0> ;
  assign debug_axi_pinc_in[0] = \<const0> ;
  assign debug_axi_poff_in[7] = \<const0> ;
  assign debug_axi_poff_in[6] = \<const0> ;
  assign debug_axi_poff_in[5] = \<const0> ;
  assign debug_axi_poff_in[4] = \<const0> ;
  assign debug_axi_poff_in[3] = \<const0> ;
  assign debug_axi_poff_in[2] = \<const0> ;
  assign debug_axi_poff_in[1] = \<const0> ;
  assign debug_axi_poff_in[0] = \<const0> ;
  assign debug_axi_resync_in = \<const0> ;
  assign debug_core_nd = \<const0> ;
  assign debug_phase[7] = \<const0> ;
  assign debug_phase[6] = \<const0> ;
  assign debug_phase[5] = \<const0> ;
  assign debug_phase[4] = \<const0> ;
  assign debug_phase[3] = \<const0> ;
  assign debug_phase[2] = \<const0> ;
  assign debug_phase[1] = \<const0> ;
  assign debug_phase[0] = \<const0> ;
  assign debug_phase_nd = \<const0> ;
  assign event_phase_in_invalid = \<const0> ;
  assign event_pinc_invalid = \<const0> ;
  assign event_poff_invalid = \<const0> ;
  assign event_s_config_tlast_missing = \<const0> ;
  assign event_s_config_tlast_unexpected = \<const0> ;
  assign event_s_phase_chanid_incorrect = \<const0> ;
  assign event_s_phase_tlast_unexpected = \<const0> ;
  assign m_axis_data_tlast = \<const0> ;
  assign m_axis_data_tuser[0] = \<const0> ;
  assign m_axis_phase_tdata[0] = \<const0> ;
  assign m_axis_phase_tlast = \<const0> ;
  assign m_axis_phase_tuser[0] = \<const0> ;
  assign m_axis_phase_tvalid = \<const0> ;
  assign s_axis_config_tready = \<const0> ;
  assign s_axis_phase_tready = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ACCUMULATOR_WIDTH = "8" *) 
  (* C_AMPLITUDE = "0" *) 
  (* C_CHANNELS = "1" *) 
  (* C_CHAN_WIDTH = "1" *) 
  (* C_DEBUG_INTERFACE = "0" *) 
  (* C_HAS_ACLKEN = "0" *) 
  (* C_HAS_ARESETN = "0" *) 
  (* C_HAS_M_DATA = "1" *) 
  (* C_HAS_M_PHASE = "0" *) 
  (* C_HAS_PHASEGEN = "0" *) 
  (* C_HAS_PHASE_OUT = "0" *) 
  (* C_HAS_SINCOS = "1" *) 
  (* C_HAS_S_CONFIG = "0" *) 
  (* C_HAS_S_PHASE = "1" *) 
  (* C_HAS_TLAST = "0" *) 
  (* C_HAS_TREADY = "0" *) 
  (* C_LATENCY = "2" *) 
  (* C_MEM_TYPE = "1" *) 
  (* C_MODE_OF_OPERATION = "0" *) 
  (* C_MODULUS = "9" *) 
  (* C_M_DATA_HAS_TUSER = "0" *) 
  (* C_M_DATA_TDATA_WIDTH = "16" *) 
  (* C_M_DATA_TUSER_WIDTH = "1" *) 
  (* C_M_PHASE_HAS_TUSER = "0" *) 
  (* C_M_PHASE_TDATA_WIDTH = "1" *) 
  (* C_M_PHASE_TUSER_WIDTH = "1" *) 
  (* C_NEGATIVE_COSINE = "0" *) 
  (* C_NEGATIVE_SINE = "0" *) 
  (* C_NOISE_SHAPING = "0" *) 
  (* C_OPTIMISE_GOAL = "0" *) 
  (* C_OUTPUTS_REQUIRED = "0" *) 
  (* C_OUTPUT_FORM = "0" *) 
  (* C_OUTPUT_WIDTH = "16" *) 
  (* C_PHASE_ANGLE_WIDTH = "8" *) 
  (* C_PHASE_INCREMENT = "2" *) 
  (* C_PHASE_INCREMENT_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_PHASE_OFFSET = "0" *) 
  (* C_PHASE_OFFSET_VALUE = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" *) 
  (* C_POR_MODE = "0" *) 
  (* C_RESYNC = "0" *) 
  (* C_S_CONFIG_SYNC_MODE = "0" *) 
  (* C_S_CONFIG_TDATA_WIDTH = "1" *) 
  (* C_S_PHASE_HAS_TUSER = "0" *) 
  (* C_S_PHASE_TDATA_WIDTH = "8" *) 
  (* C_S_PHASE_TUSER_WIDTH = "1" *) 
  (* C_USE_DSP48 = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dds_compiler_v6_0_15_viv i_synth
       (.aclk(aclk),
        .aclken(1'b0),
        .aresetn(1'b0),
        .debug_axi_chan_in(NLW_i_synth_debug_axi_chan_in_UNCONNECTED[0]),
        .debug_axi_pinc_in(NLW_i_synth_debug_axi_pinc_in_UNCONNECTED[7:0]),
        .debug_axi_poff_in(NLW_i_synth_debug_axi_poff_in_UNCONNECTED[7:0]),
        .debug_axi_resync_in(NLW_i_synth_debug_axi_resync_in_UNCONNECTED),
        .debug_core_nd(NLW_i_synth_debug_core_nd_UNCONNECTED),
        .debug_phase(NLW_i_synth_debug_phase_UNCONNECTED[7:0]),
        .debug_phase_nd(NLW_i_synth_debug_phase_nd_UNCONNECTED),
        .event_phase_in_invalid(NLW_i_synth_event_phase_in_invalid_UNCONNECTED),
        .event_pinc_invalid(NLW_i_synth_event_pinc_invalid_UNCONNECTED),
        .event_poff_invalid(NLW_i_synth_event_poff_invalid_UNCONNECTED),
        .event_s_config_tlast_missing(NLW_i_synth_event_s_config_tlast_missing_UNCONNECTED),
        .event_s_config_tlast_unexpected(NLW_i_synth_event_s_config_tlast_unexpected_UNCONNECTED),
        .event_s_phase_chanid_incorrect(NLW_i_synth_event_s_phase_chanid_incorrect_UNCONNECTED),
        .event_s_phase_tlast_missing(event_s_phase_tlast_missing),
        .event_s_phase_tlast_unexpected(NLW_i_synth_event_s_phase_tlast_unexpected_UNCONNECTED),
        .m_axis_data_tdata(m_axis_data_tdata),
        .m_axis_data_tlast(NLW_i_synth_m_axis_data_tlast_UNCONNECTED),
        .m_axis_data_tready(1'b0),
        .m_axis_data_tuser(NLW_i_synth_m_axis_data_tuser_UNCONNECTED[0]),
        .m_axis_data_tvalid(m_axis_data_tvalid),
        .m_axis_phase_tdata(NLW_i_synth_m_axis_phase_tdata_UNCONNECTED[0]),
        .m_axis_phase_tlast(NLW_i_synth_m_axis_phase_tlast_UNCONNECTED),
        .m_axis_phase_tready(1'b0),
        .m_axis_phase_tuser(NLW_i_synth_m_axis_phase_tuser_UNCONNECTED[0]),
        .m_axis_phase_tvalid(NLW_i_synth_m_axis_phase_tvalid_UNCONNECTED),
        .s_axis_config_tdata(1'b0),
        .s_axis_config_tlast(1'b0),
        .s_axis_config_tready(NLW_i_synth_s_axis_config_tready_UNCONNECTED),
        .s_axis_config_tvalid(1'b0),
        .s_axis_phase_tdata(s_axis_phase_tdata),
        .s_axis_phase_tlast(1'b0),
        .s_axis_phase_tready(NLW_i_synth_s_axis_phase_tready_UNCONNECTED),
        .s_axis_phase_tuser(1'b0),
        .s_axis_phase_tvalid(s_axis_phase_tvalid));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R7Su66EFP3j7HdSRwT0ufavHZ21RJuR7GdMa5N1qrx05vZRLzNZT/TrlIe3c6DsFCenpiZCD2noZ
QAoR4Rt+mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
CMEJWch+GbdZ7DIDA14J94rfET0XyGxfytAfvkgCwK+buy8C6yPuTyczckBiUAmLYwq3N0YLZZjn
gsyXn6e48OgTdLuKlj0b1I+R+nOfWP/cHyUHpk91Upohu0q4i+T1Z7YlZ2KevK2O/yOn6S3pNXlM
CA1hIxQSQLLJQcJjXBI=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IDWChuOHJQwebqfYcE88tSCCIBnxLv1aLHU6OnUVlxJuAYH1Wr0uPmJkkVb7CXm2iZXQx/jo6XaT
TumCKxTZIL3ET0tLNKmedouL0GaXfUzXVCSzEoTXiWf2gNPQB6+v0sryyUdggn9CbJglWE9UkluW
rCPI7feYIVKqODl/+/XlmC+0ONTNrMlZjktMivGmmfgFiOaVxlj7ZiVhYDRk2pmK7N0SbS8Yhqtp
tu4XIZyivSAfozOEYzRk3aC5YLPqYEODky8fadXC0TifmV1/9ihpE9MdNVbsAfiU6jAuYaPtixy1
eWfPyz8p770Y8aO4Ymmlv6Cov/zwD1Zr7rP3ng==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
D4tWnXwgYbtbYBATOz3rKT5u236p/210UA0/0NawJUvRuLLRIOY863EXCqmoNKd3cdAJGfRGO/fA
mX3MQnn8fORd5NV0Drcjtq7LVURk4LrUaNUiho8FoaaKgENLoHWz5zN6jL9cfE19cPf5q6X+HSoS
vhMpVULwvEeloyESsidHnjc6Leo2s08QmBHWIJ4gX6Y353OK7qNS3bZaZnw5UMLbMBvsopLT0HMU
QgsF83OsAoA/LETx2kFpFT62GHW7Xr0WQupO68ddkWdncI1pQ1ry5DiS4IAcjHmDYTyo542wmUO5
kUoT65xdo6CgR0mBfndpvcIfOPFrzBLsA3X/8A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TYxxwxeYhuHcZvmvIoDp6PM6jwmqvK/EOpQJuzYEJwksrBgERfR0MxeEKttmbgtW3IAljWYtUY74
488K1yihiHHoprJ33R35ZxUze+TipXVo/GLAiCGp6aVvDPTACRhogMPXLJypmeRU1yO394pPbgS6
wC0P27Oimz3cJkJrwIhG7UV3FbbvFXVTh6Lp9wme459SE3zFnKsJYjUpffIirIVsuN+DETk1csWY
DA9UX9JySwER9tWjcgC7RtzEV1hjIG9WuwYm3zkOqr4FZ/dkK9PLm51AgWpaMXgB/7ws+/P8fkKm
QNdT6izgEuqxwJScjWNpExqD7cRIM9y2FibGuA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Y058Y7qqyKaMCwzJEnFzRJA1mSsdLWRJPV8jeagM24nQRyHL6Of41SQjwa7S6UfHPjaxh3kStD/R
iqFSj7BMeRnjDwKkql9QbQCQ1AEtG8kKMw6X1Sw8vQdkSSWaY8A0qHxlAj9yFFRWps0IUCT20y4r
a1FWV0KSxSpJrwls87U=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BkCcTwW7IOFCvnzvt27BUy3KHmy1QJwSQsGYOAQoWdJnp7bpQCB3MV/YrDTHZ6GeuEjTv+Y4jK1+
AUi7wPge8Y2zeEpQSTFjwsHrg0a6KicpWuoUxj9ZsRjp7lihT95V1Q0eAIg8YhlL39mGtTcQ5Vdp
7z8wKvjx++phq/T2pWg3qojhz3yoqaCG4uvKWuNn2R3f0YfPc7K1qQ8cRTBYuIfje99ZizVelHfv
/gPaALzJb7mtbJVe83NohlYy8IyL0cxXXClT+sW1XPYiN9k5NbywIoRmRDobstBVd3O4Ukd5mT3V
p/qjzuZHyCC/I/jJRQFyZvHI5rcbT8On+yp5MA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pdHiwDjd8GdmR+WxPa/XNSJ2g+kVgLBwmMRoAkSsWKxg318Nbc4dK9ZQXWZboJmcuQNKuMBn6l7V
ovgV8wmBTUhcT4TZoctkW6qLzlHJQzH5bAlTFXAGpWf/U4g7TgxNXn3hQwjrXHw2K88xMb9f5mQe
lNGB35NXhIOuq1u3vtuOJHXIswrd7MZsQvKy/1JhNgLaY9FbAidObxORK2DYQYAkgslvWCpMR3Mq
tnjqjWQBTc9b432Ieur8C8w/J6grqEBRci4LV06rxSb9BrNxrgKTvD+6mDu23hQ+TOzU0Hf5EfTh
IkSRvfmW8Webg1jFmgq1zcnrEM9pCApgS17inA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fbBEf78TAqZWUdoy/lCjPRqPxbdu2uLMTtq6ZneGAY7qqiWtVbu1zQ95qidKMs89ZNHpdOKhzA0K
ZyC8HzW4IUyaHzHbI9mi0YxkZDYB3RhXjEJIJ5GG15zKXWMfTo2pVfwA9E/I3vyzWHTco38mGhaD
u/oGwVUUWYc9fK7zJ9GLmUoPCDAC95nHSsRYdyZR7l5liVBAp3nAKw/vowxXCRbaffBlwXKDIC6P
McpNWTagog90zFREK7zRuLbr/0XEPU2uzLDQwyIQ2wW2hNTcwVYWrntv1YTwmBC3QI9wOUN2RqNL
2E3Ki73aUW+YOKj6oZ/P1v27bZopqwE2JU0AEw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 23872)
`pragma protect data_block
F/nqv3S8OAS3XexO6ZeXIMsdZqs+43ewKGhaAEbggV02drF3DoLHpFcM/V4BdH4efRbN/WbGy/gs
WVB7jeO+Dftnlm1FDSbROKtNwAFUu4Ul7lskj5TVdysxNadfbQSgxeXXoePjMN96f5eJPHz1K+J9
ydKIiRwZszagDOwUdF3BWD+pMxJAvSJeMf7gDjU2u/5FELTSBwB5sRc5ipb1vavFm8gWyA5S21Vp
dYDngu2CJFESGT2Q0ukOMJ+Ic/+YhPy+fqNfonk/YoWLOzk8P5HRzreVnni1W4mEELLmD+a/9pFH
BdIZKjT7dO1vSeWDy2J+vmcsAfmP/hF8kztdKd0zgpr4GvrT1DwhSSIdcZeb4PYuZIxQiRRQ3vZo
yyRkZq62JYFxkR2IJ/sZ8DlrqiSLik5ZOoYD2cbtONv9tLvV2d9Ipu4AEQSVmAeVtyurrv/l6nTS
WBRyUMaDgJOolseSQjZp1XvBDpdEobFRXegu2XeWldYnssvGKlA7Kot3i76AStRoJ0zYEEHjAmb0
QATXcFq9YioF/UDVv4kz4fFr5eLsKQqUCVb1sNnzA7/GBBbFfokhuVvr8C3qsJNacD/PDQa9ZZV6
IJ5bz7GvZz/kjFFlHK23jkpIZU0VexdtNOlBYFcIihdAZ1yUS999oJ115Upsnp/KmXvYNzUh8wn8
G3FxYuZ+ukkV3S8Zdje0xv4nL14S3NGbZBaihu+2siiHWZwHBqp/9y23EEDF7jsIaffwxVe5YY/b
QH3d3ct1ZHK6P90hQoXaxGRZJEPYoZH5hy8llQwMGRSBqLRTgTkOMslII6g4+DuxZudZ7wvSn3z7
jxjrxdzQ7cf7R4gCHXUkR500ZTzGo394lOnfIYOAZ4yBXMkwp77Pdd6w3yphSp4U9+Qpr0OJm7mv
vZXcjoIK5aXmODRVui6LXj0YIuzCGpBnryUbNLDqtp4nF0DZHf0dpoQ5x4m/PRYTthRv35lf+U/q
XELrhqPcCyRWAbXPM4hsdn01vzWNALR+nfiTw7CJNwa5NIRwKI7CThzyruc6T7KBKkonmwU6/1Fc
q4d0EYnFFfQVvVq4uukBZ/ALQs7bSpazUuExF923kUkF5Ou5JE5VyA/THnecGwEiVYHUb8al5P7s
7L3u7hs4yfLIqB4iaQ0PtF5erGVsg6KhqtgCMV7CpYfPYoxtPPtUml9hTqNC06M5ODOLpI5hzNHl
WNV45YreIMqdY+poRfC05QpcBzFqHPHJErdOEAzCeZ9dgelSm864ix1gYtMOamtDT2BiNf0N7Gnx
qasTfuWQ9OjmrNXWSZc1z9JZCSRmLqvJuEmbNoy6VjeKchOWixBU2RX80tWQE6ras1c3MEZGbhZw
m0t4ItuWH8qbHx+gk1SvAoy+cEnoOTfMPfYS1dLIu9FgsNr6uXt0hKBaVRZSe+r/nN3L9vV5R/vD
BtpU+RXj3CKA+qRdZDuKJw2yvpOWYb81JFmw6Z7zK9A5Vn05bwA5YktMehNsf5GzIS8+hmkhUuhx
CZDDtOJ8jHDkB6ZyhUWe/BMHncUgCOGQYjbU/ucDWxuoXU0ZVA7C6iNiN/aFH7crOkKxnSJZ3E/X
NOPZR02erkOUvCjM826qQRhSWMZJYbFlRNbfVFFoBJMXW4ANlyIqfQPs9V2gB3I3tBVOyO8xFsaD
nYwbke6sVeiSWgyv9eITvvDmHgy3YDS/+tGCZ61G5LFcMg8W3wadgEFe2Yw+DZOvLzXxayVM+bg6
I00AQzQhdxtnLudpLOYtsbjZniTf5rpu0F2N0+kZ+yVNFbLBEzI3kykGytn2W4XQuz5WoZyGjmck
ghBWtBPw0GgX11GU9sogiqi4l8gvwwaGXu75z7Enm4GeroY5Ul5x6tT5DFGMByu4XVfCEW7OJHvJ
sFko+nAg+WDQxdzsD8y+Gwdo+AtJ5wB6NlQ23hDVM4l51O1FW60ie68fNH3/dSTJGMXWH5GjQ1uj
hNNLq+7Y83b6iwBKL17WtxgGEe992ebvcPf/LpuH2HEZfywzB0nkagy62Or3brcdni7K5b3S2OJz
eS4ScIkI+nJLpxvcjETcWrKxqODM/6gvnCr0mpQLRQE8sg8iw4MUAfW1KIb15+hqwBxKpQFgL06M
RZhOp2p7/Lc0ow02YcxfNmAbIE0Cmd/F9mwdf+YiaPP2dcHtu05+B7+r8nyFVbK8n6EH1ctKILhA
SVKXvqFleEM8U7Pf853vRj0iz7civgz+YtjPTzKBd29fWNtFOFkqa71aHp1cFOEsnxRT8SqkmYTz
LCEn/ESpMJgg8CRLG7NJGbFSaLspW60XyQ66sR4nOuqfFuT5xwnGP6umVrhtmLSVkx+/EJzGVrSb
fTXhNzuSx2nOKnwpCNjDENkYhWlzmxnlEW8m4nI5BJinU49A8rhe62u2l+FZ8FbBpXmTdLpqQWrg
bK0ZwUXazqBnGFi2nr81DgYMHeWBZyMEkaIhMdErXW6fBVVZWhNXJqVBvSdpiKpYQ5btAA88xlI1
KSD6W+ES9MK7eY6RPbl0xr3+fhbjytGwCK2g3WvZgvKwrjjrDDQV5lZQDBAd48h8w9wtjQDZrF4o
Et9H2nJKUvt3Th6LIB0QBsIvsZkXBohk7P9ahFNS6qWUBUj28LMIAnZtwxAXk4P+4e5P5DYupOxf
0x1oFpatu5LIlZCPVOO4jO0eq7hizY4XaorFkOwkWU4WqnJnAKUkhNxb+if7jsO6NV3lVyNC0HSd
oeot3fNeqkgIVbc4xoDdZlIL/twn0KRoV1PStce7gCNqj8AI7aDx0SmCwmSNC0py4q/W9faxUmYF
lcWBZe/8Zmq7vAq8WzJ71+fKCB891nPj/DWgfL2nJmGgZET0YcWBRrKytlSnkUG+zBPytSByfrX2
uCJy6+Hg7fpxJQFyHYKZZgXB3iiMcwRpKjPS7wfHt4RJB3i9AWkLe1kccuS+3X6yFPnbmx4VUaua
Gq0bTkbKNUpom6Cnskn6J+bRUj8PAhG6v4VNK0TBMyZll1TJ3ME0Ijuva+mnhcrWmYkVUE/a6M4S
DrtOZBdJUgsc17/buw/vJUWnGqN+kpXr8QKYRc4ZlV6yLuZ5YZWaT56qG5R/GP08z3GpWgyDuIcu
1SXrCoj5RuuhbYIxdTvZ5njrsjhHdiKGEcbvbCariwmoIjftKu1a46ANyoP5vorpOExgfJaznK2l
bqBfWAwsD4B320utOIcq3zd7Ca8yWc2xK4iHEPt5F5xej9RXZ+it8PMFcLyvysOigaazNwn/+6ZI
wMoC1hvdu0V18pafC2fxYu2n5+sQjr9pGnLiFKrsx+HhhZQsRb07sY+/jkPixpAGeiJLzpV6apXd
YZ+jgnTbO+7Kxoc5i6xNL6LHdcsB0cGgtlhnhRCTbNEkOBrwk3UZzkZ4aV4xdIomvylLoRu2axZM
dufRBo4XmzEPZuN0Fq8/JUJbq+EMyvpNf0Hjwi4vTOeM/lVyadT4KbxJOoZR5QjhV7/1md7Qme56
WtITP8do1dTcozHiiUrzldJkGLAZk+PWT3bwzOOTpnsOBfcm4lNUKU84ED7Cer8aJnc91GShB8EL
972Bx5xD96CEthilGOXJnHY0oxxzwoU11bG6k7ozo/uGeG8kXy/x5d8WBc3G1MZrXnlKGbIg0dkm
3R0Sxy1JyVBDdPOUEe9nq1L28Qb88gJp9KzfGnG5ENELqFT8gh5abf25Smt6TzQk3LsVDMHwtVun
JVPPxStXhRqSorN6Fu60IxFXfuJb9Wmv3aoSBrQfczJRagUEUtYXyRO0Mx7gc50jCN7cWBLI/3Ok
NvD5bt52SD+qMmXH35KhZx8UrB2dhDJfmsYX6cTSKfh9BAjiMwdylxLJ+kpZehe9ixk2tDX3M7uV
miXr3f61ZtIjELiPBcEuF/lNCnad9ZylyWHQBFZuojAOYAzAeZkOOQ6T+QMV9BZqlgKKJgz7p++S
GOLK92/dh7vF8z4Y0tdE74AjpHaLqyDud5cRxQGZFJUcN8RKh6PP4fycG/lKtuxaDv282UNTafp+
eVQcbvC73SUJ/MXJz8YEYOOaIuYqFMkDGFu8YK91lPDm/N+LrrAyofyAdPyyx9G9EPtUHXNVkQhY
sIPFnSvnP0sWWrdZ5zqQsBgIs7mrZhja/bK9gc55Nh+k1SHwg5v3SahEbr9GzYK8gzEgTsN4GX1N
SHcdUCNI5Tu+h+D2a+kmoBBXpAFHsR/9sBygXKnNu7xaNhjdDG0NnHxEVEFxt4Yq7P//sx8MB8XD
hMFIVQwQQj2ShPR8QOMqxwVDnfSMRkiIB98sSYzzxpW+pNYy4dkz91KQ0i1+FG3xZ/hVtO6blFa2
rcQ31pUNbvj1LO9o4Dff6BfmAMUH8t5H6sIGx7I8ep9BvCUIzoWGzXeaxefCam1qhhFiI6K5/20B
dLY3NRqmjH69oD3XUcDDDfEZ2HV8VQsw/Ew44kwQHahcvn1Zl3qlz2wGitw924wwA2wrmwQsU+X6
1HOXkylSJhP3DNrLFpgSqLQ9Tcb3mQBWAFd4sWDAfrLAJJAol3aJoH+kgi1qQqKkhedcEM4i3a2x
J6xUquUE1+2+rac1MKPr1VnnvHTQxR1ePfz3zbYyFSDF1sT9Oe4uohDEzl+xEdBbnH7iNpR+4U/A
eM/5WBIY/V1n2ff6YBLvPflgwUgSEgDT5KH+qdk4VIM3hd2C3aTck+hiA5fcVvtAuqWv30KorDF7
LaBEPMp2LKNQOCAnXkml8ORKZk65Z0uugysiqKClwsPxnABd0bdusZm37qP3x6DpsBXbGIGrdgUV
DZfTar424KzBvf7V9B+kZLjPSPHIR8lNf6iaxhlb2ZAxWYJuX+60O5muKjX69/REpT6Y5Dys/FUb
No8fsbiU9VYAMfU4y/BApCicBB5/rMQ5arbvo7RL5NVRF2op9PSkyQfkvlnD+HQ93J2NjTsEDtCY
T65ehSqsnmQXwSE6y5hKLm9igoCDkw8W333Nq4PToBtvA78lxLJ9ALKnNGGfqTLN5gkI3hsgz4uZ
41e1Gn2QTo7kSHUPfouXss9QvI8X1qlP/GMWGsgtyvWUZwX2ungDpHlsM2pMSBg4Zdu7+D6l4PaT
ehytXfeQJgdoWEsybC8ACas9eHJjusjE/g9RxvJHMLmWT+OliwDGKMXs5YtxKaQGKJe2JDyZFqMr
1qW/R379cDh3G2ASAOFhlrgM3ZslboW4KTkPF2zMgZLmsSRhaQLjKaS4L7/UWsQn09+AV0ZkqHoK
MPzbGxUfxWuCKviix4qynvOKvCNwa9jwP0a6wBJvqu7f12o7JZ/APBXo7lJXaryT2skxrKAt+8Xv
4CWSmubmv/0d11ISI+C9u0Ae8iFHMFSX1Hl3ci7CQJJAjfxHbjnmXU20NUDQplGrrp4IIi3myeLR
4Os1B27e2+LNKM406zIoLZTX+Y86kIze2us7oKL0EG1jYqfHkHX21Zv62un8LLl/KSXR3Td3Qisz
FOGbcnjxSGiZ8BVhhlvqNnW5f8tNz5AbVnFwZKoRm/KeACOE0LVlqB/jSvPveruUz9XvSvSVlWNj
IxGZ9EFXlcvSzflyFYFRJYM3IpxM1w9WGbitPNUNr79y0NtcOCzzUwsiFnj8SpIFYK8LFHlsOaFu
J3Ciyxbsy2Nhn/MQG8M3SxuR/9Wi7TPYhje719DmxFstJ5vmjrY7P3uXYj6Zj7sHBdB4KhLVITg2
c5mfqH7XtNzFrHfYdatPWg7XzFv1SINNM9UmHe0RnKkl+A0fEV0Z2PDmeGA/+vYC2UOwYyKT42B3
DIGF+d6qqCXplLoZ0Yt11wKlU9EwddwjL9WKRY2Jc0+YquvE/pVP+3hNlyiA6BDypqdc/PhlSlEE
Ng5nhAwJk94YJw9PA8FkLRLzAxgngfe8KcGMlzMKAbAb1Oc5eZOWdjuG1kytj9AbPY8avbGITF3R
DDCmtAD33l5M0WNsyWJZBvvE9mN0SSGVf1pI8vIDMLpDXNc1BNd4/OCKBk2+q10rnlDcwFyh/7ji
lLkfglMJpVU8y/lcC9kOitrhfTXOaYCL/ybohZzqoVO/d+7PLmHc3T7IRSBpbXyE5sPUksiTnsPb
RFhuiJH7BWbW9KV2A4gh7CxQ5a7/G0J8q0G5Sx3CTeP3Pe2Qwu8BtIdcScpDdP2uj4OjIwNKS6eV
z8fXydwwA2WVXJR7zM18nGIhiJ295Btm+zlrW1HwS3XUZyRvnpsWNAZqCC0ylTQSZnSNyYMv4hXx
21MUO1QiKzfTKDTWFtKPk27dhcRkyzIq7Go3v9tnbb3xZbowGjeA9eoLxK+Ea0F9HX73W8bQEbv2
bnHdI8mH+UbJOaWErV8sMKI2eeElOo7SNqm4THrz3PoHBA/Ui5t3u/X3vCCPuCQzAxm610UlPWaz
DB5CYiBI34USijFRh+6oaWILuj5KH+b0iKL8MtV+dWAn903aG55Bm6cEU03ReZqHo67ux05IJbRh
JXTyOOrSSfREPPAOQb8V8+8VW5WuD5Yal8mG0LQOGh/GchaZf+savme6vVLxJlMNrZdqEOhwbDGK
KAsxY8u5n4XFb+1UQHly+uVQwwVHlK2NoI1w1FVA9sKntBtDK32XfY7vDCDspuNlmjTpMsJENQRF
AgdSEKQBg94rher1o6dRSeq+oi4j7qVqiiaRTxapHVRE0z4VBRhm6eNMtkZFw9Nj/UY+H8DHj2+7
Ff7dNAL9GsFkY2f1Z5RreY9CgpVwVPZ52O7YZ8yLOJ0y9crQAadYTi84XEmz6ln7u67Zx43opPVo
FtyzC7hVXdCpsVJZfCB8VlZhSD7wvTmQWpch16mjk3IOsCE0U2ORX1/KnwW4Mmc6NlUnjYDbMC7D
zZWiKnjoF4qb19+d0Bg3C32SC1D/KCc0AST1dkHuu40H2SG5EnZYYYLe32zfM7wimUQ3GwenJ7Sf
B85Scr4xACvIHVNEK21fJCgLQogZXEUM96qsSRbome1RgwuSeHVC1+UFxChv+ief30iYEWj6cv8S
W7vG9vFtQZNpZsmpUxubOHTjS/VuF8yOjASdagWZ4kHskvv9RMgDS/LAfh9966xjjGfVgbdCxMpR
3/TjaSFPakGvrQXbGwRswLeOfh7JDpi1Y+Rqqhva4qpjAIVX+oDoJaEqaEi3xGMC6oy5QBLEhPLv
lgxfLcdfgdA7+HFkeB6v4g6/CE7SbSCVfyQi9VBaJnULeHMWavxmLXbx/+pgDnj696BmugtXIT5i
iHGrt+cb1dMqsiuy+ltqHY5+jZOF5dm5cW8l9jLxd4Kq2U75R8wr8B7r6j0K37ozq6xHKDH/jv4s
3kO6v1d/VXG75rEQCYLkbc+Mra7BSxfpvf8MlaGMTb885VAt6iMSSIS1+X6dSkE+TCT6FLTdMVk3
aiBYClcbMaPVESyyxNeAoc+Kw543YEyfLx6T7N1SSeETUAjfmY9N0Zkv+QhXtL7rtsG53Crgy29o
aKpG0AYDqH+SGVyzPrzUGUxh4iJAA3wh8NwskFNLSopqjUskGaoSaYV11ea51fEoFhf5fcjKakY2
MS3kIMvaFmAT2ELcPpCpNH8sSSWbAweFtCO/xLzhE/LxhLPf7W6GGm6AZBOOwHoCCVCsDUa2L9TT
TStZd2ZhLJCtPP726FwlhIR9NWNxQSxVkV1MERLF8IqyCzamjrjEQyXWSnWR4M2U2nxxRQgrZXca
AmoQKJTZ8iw8tQcMGTNlQjn01WZDEQqcGeDsS0bp92RlKkJDGzaHgo/3HiGSkAi9hfHeQLcwjuWk
nWG1r7m97q/djBElNHZYuBC3FPBTdGjXJZGzcDoYUzqY+c6Pu34yfZEmFb83oNSeH3Gn83/XU7WT
z5ACnfda66Ato27dcnzI0LYsSSfPXn8vPu9hvs5Ov47NTZcmrHU0ZVbqms29WN3+/ClQ7tZF/IcG
49BbXmzX8RBrWwF2wdexyIteP0D0h+Uq8kPOkx1Jwl3nwKr6IOIeKwreTwIYjMdWB/K4Z2WNrrp/
TlFVP/troLD71Jw+3cCQ2eiM3+yrzR8zsPwmkDp1iiMRFJR9KPbdMbk5gGyymDdZWfE0Uqu7LAs+
TTQFT5SQXUUjc+e9PnVQh7QNVTsuWhl3XG+oZxWlyCLLquqelsjNyzAvGCTXOdYVEbHr09fWW/hL
57MrlkxfU+Jt2C/NtTFj+rsf8P7AzYbIAXtKaF+XsdkepNbQ/gtEY7VDk/tsGm9cMjROc0p7lup3
/HveCIqB3fyGrKMHVoxr+Uv/gATAF528lmhRpDIo55GerXBGH/o6iMw4vfKxFlL0Yg6E05YfoeUW
XbbxApJhdJgbt+4t9z4lTupsmBze0PgTD3qgvoH6tpxQI9nDM+FP/S8jdMrYWsEtiXyvy3mu3JQM
l8JLy4+j6bwwk7iQWcPi3x+e0BY6gUQoOuhUATqO+Whxw9heH0KiU5xhqGAyv+D/G47JwYntktVZ
gXoHeeOWLQQjmsFS1tJFqvMxPYe7Id627vGbA8yJz6nby5htgRgi4/pPshgQKYXYdq2J49zsjCXJ
Q0i1Eixia6klBrKdOxT+rHowSr1FBRN/nHKbE9gTIPn8Sqs/DViBSdPYW8A6VHddG+HXiVCoeMXG
VEhiRNKymuJyJ4O/bWJ0Uqx/KC0/rWoQC5HZ2xkBNWpLTi7ZutsV0ryLbTSExe4as/mayC0KYaH/
YhQoaWWepU7J23JBCNGupYzDN7qsdW6RQgXQzikEyrMPiWtAp9y1ENBythFF3FxI9Z9ARcn7jY6o
7fPyVWnBYY8lWDUQRog2svBkHB+UNk0EcoMgwwBGsS5lD/VslwEa18iJ+CqaGOAN6d2jvbbnluze
wEamz2JwvCT/UyNoC9zQmcHLiYbht9pJYKr3OlE2086+K5NZbA0BA9ZKtQ0edZ3gHTNRR0D+6DvV
HwzeN6Ihy2cKpKxnGgD9afDXuQeAUwk9qfPZ65dKcYYOYS7UG+F+m/EQLHbbXjyKmYwH8DOXb0kG
BCP30gI99+VkYFHGLu43BV9ERoOgq/N0FYl5/sfBdC572wPmzK0cjX8+lfvMsh4tZDWBv2VP8aln
g+fy5famkzMuhvtlKGiSXwRbAwVWmfebZVlilee16Xt1vp4AY1RSZOIN7CbZtOMiwPCgptGi6VJE
FuczSC9YQPUygPipyl9H7xvuaTuaJjg5AnC15iU+B2iHQ//3u1YvaPtIdRUPoEX8fSQp09fV3Ms5
BYwU/3in7A78qgYKB2RwrKVDmb82LJn8oy5wTCJ/8j/WJZ+2nrmN0x+NbC5yVeq1ON6mHD+6jAix
Ju7urzc7zWih0lX32Ffm2cUcAhAZAhmOPtKq9iWjUaVYp7AIII4TiaE9ekLg2i8PJcAKBdoChigf
TABF8eobO/eTnP9n7q73ukcupq+RYw135aue6FklLOchWYbA5X7gK9Nn0JgucM62wNSZ2iGR6yA1
MPTtym3UzHhw8TsfxHR+oSLhFcRcwQOmJuuayIj7aSHE2Jcm66d2kSNenaE79PWiQtruIJdUHOQ2
/yyansEBeZxfBI8/cmNa3o/CggKzQRx2CdwUScc4ibmR5lRHdAntIoKj1y7isxInw8AWMVyOPl9r
I/dXq6jQl04JIwff6+gbMULhkL+vYryhyAnWxV509HK95YcbEp8bAk/TLvDoH7OgTphU6BGi/qf0
6FAvrxmphwvkZvW+2/ZnBG2p6YmpJhUC90TgGmZhoY5cJqNMnAB0JkUJxOlROfT/xfTBBXs1Tk42
w1Zj5pj9mS/Y/TwqSX6GuNdjPSk/SeuFbme57AQ9E70UtE6ozmjQg5mzyOggPnTSQ88UjAB+XXXh
0jNPupC9LPUnZtkwPN8yRVN5B9t/EIlIjdeqgXLQGcVO4gw0WkT23AHtPcB8YPD1qoHGh925cB45
Jd+BhQDdkTPsumfXp66MvuH1G/Js5PzGzz0KzAV75R1YiaawAXP4B3G3Ng/JaGYKscT7Qt06fVcq
GNSWcAIgYn3H/b5snBvyR72igYerBLg6pksu6zQz1O2vvu9Ii471MiM6VYvuTZbdjyMlO+irHSrt
7exdYb9HfolDG1TgTZh0qepJoqGnB+LYFyoH7NB6LHOt4h0otb3d5TpsQB8uEla4EM4I9RjmlD95
I30+pPGKaMfDa/gF4rcFFzScvK+gL35eFenYqNQ93FyaPFdmvjvL/OqZygzIUWi47PQntooNlLzy
zj4X7yllXfvuldUWpezqlXse72e7yWEXvZQqpasq6e95hfvorb9Sze7vnxCr+piTXIEeRRRZHhG8
5JvXrJvKyPj/KI9pPGp7y1Btl0t+rfXO43aRa7hqJD2zN4L8yqHkQSCPdGFtMc+OS57GA+mO4/md
Ad+OJSV2BwNhU4F5ZPUfsA8/yP0HM6h/KdIErKP9gGRDC4C1S45cenrpCfpFvQJhoTLlPZp6U9gK
1raMYqa6v1nzfC2lZXS2Wy75voTOL0FT7YGPmJ2Z9/0HGadUjSGWq5PhidcEARL8kFp7fWZjN3eN
9rA24l/dUx56RHEhRo7LzVW1R4uT1WqppxDXfRECZ38EGF5ffxssF9KBcj/W2mLZmpQ5xXuV/xiu
A3NJL19SspNhIJOHx1IQAk8vETXIBDi1Nc5/Cn7KpYDTMjEvu9gWvP+GeOjfGXZaLORnWk4uSydB
6Z8C/oONLVGFPI1gN1CBmq907RCMo1oek84qhhvT4+sleJ04ODNtnlypJAH+r5P8gygN2a+n6io3
rjPH6k+WscD5F5Qv7w41pO/4FlAegm8YDfViCgwaT+RVXwdAyfJ6zw2X2LKyHvRVbrtY9NQTGfO7
7qYV4TyY5s8+7Fu3k+Pjd2xCmRevFy4zbR6fe4vI3fwacbaWtfWUziugYfHYQNQ0gYaGs97+1fG4
Pxdh+F3Bu/FqB47J9YNYpjNCh0IBT345JHmLR06H1daQGgfNP+SOy84SzNvMlWeGiUHLhsMLawM/
3lqlk42vOjgz5BdXlpyY9vK47BXz8e9M2eabvlRvP8f7wRtgcQOBjxN/wn0h/JxH5xJZVdUHSX2T
B5EEoRMuFfWqfjtdG+96NuhAIgilawaayBzH/1ou4uz5neM8myr8phwTFzV1RzHt7ea9TuVizb5X
pfX7XNsIEFuAWidGN79uVj0FM2HcsRDU99eCubHnrnejSPKGnZKSG/WWqn8JvLUfJyjd3AsuFYq6
gRAP3WJn/FndzipplM2HBtefVUu40c0Vtde4uerqG66mpJFzPqL3uSS34XvLTk8/qaeKZDn1RPR3
MD2AeGaQD8hfCFpqMBUUyaPPLqwSP0Ow65EyPxZa9Z5yDUgfhbng9icNCR6A5664t63pc2ViDygy
6COCTEVP1H6CYt4ShvTqTtU50D7yet8PgkP6C58y+4mciOAjWmGSoS828elUpjW2DQg4MWKhzJ0R
8CJi5Ur5WZ4Sj2PfIcLM3M0qA2nPwwsUYedD7AZcMuHO9mmE4h8vNbYtGfLxucP3AzziMBgsmOKc
R0/g9IX20AqOWLb9MMrKvanlcxj1DWlyY8rCLshLo6VLcaSRrDwAL9GQMPtEFbufm02i1hlSTz4W
l043pKHbETRwGmDOgMYzabV378Up5ywJjZiZjIyAdLsUxBlXn4hOHu1IUOUWKJIKsjDHKUNqEh1u
MPmXWBYcS44SCoNf4XbPQMH3K4GXn/F48LE/D0JvYMCtPwaMn50PjMIF/TUCbd2IH5tjCwHZjGvA
yMZMCDFoMJXIJgAmVy170Hh2u8poWIDdDqvF4J2gwRTkh2Of0UoDFa4RIf0w/5ichLB7kSImIdM+
toC+gvEDoEGp2GIxxb5+p4gO9yfWqLSO9umdKmG3+flRFLPEn8ihhV5Yls6XrbAJtSWv8kKHMg5/
Nmu06AjrCU+cYFltkHZ1XIQBnQKgps6MZDNLIlUZeysgpJDxkwWcIobgbly911JVpE5P2geWyGGU
dRskkIJv/QXLqK2iRxVOmQ+HEh4x5eZ1Kn7uAKO42mPxD0mrVP0edr8rLzzMQsRAuI1MxDZfFEIB
8RkWIr0fd8L0acNIp3eMim8ynTu8C8bQQMIn1ai1LHNvzcwVfjsgPrs0F4KYh3H4+FzDoDw/HkBb
g/LY5HxgmoK3LkBzWn2tt7x2tQcMuzoEb9jSmqlpugvPegtWzFH91I+hH630DOAXmzj58uTSE1bs
8btCa6LMeYPp6e9UbVDKqvdj8x/CwzcyjQ98Ul+c8h0v0ennqxlefSw60e3g4tRaNux2CHvpdXS1
W9/fI89G6JRs1FTcV9sttZbSNyYfQL+XUcPlKN31JhzfO99wGuQYsPpWDumhGeoO5pUvpithLKRl
pNdQVf+zHnJEfqXhTYeCkyW7kU4VolZoNp7emlJEihze3w83ZaGGBMpJ+m6TTdetglGjWqXLIaqV
wZ4Aya/1s4d3THtBo8lQ6ge1homShJ2eOll3lB4byZq1oiilwvh2TCky+xJikJ3fY76Z+Auk/zFt
/CVaHpyAFWCa1cdeypb1cJVHqPxrFekyCo4wbJmD6eB9oKLJ9VUTQEBV2sS231oc32ErYrjaK9+b
8sa6sifKzDFk6YlDXBZYsD/7Zo6tDmFVGYTs84+ofRgJhLkIWkDiUwOF6UAY6iAdjq5Twdtjajiw
I3IPBfRVxjS1sIK91rVwFhV24xekedHbzNlfu5BWZSqjCnWe/6Kn4K8e4Q5OJfftHp5s9cRVp8eM
N/1VXnn3zezCzMT4dgmyME2NKMOdcR1eRAlWf97ZlHR6EtkJMiPZnNIeGDJBoesmuk+R/Uhz4mcO
1fVxAY7S5jYtpcvCpNvZm7GNAQ6lvG2+FYxD/1IlK/cFlu+Iu2ulV6Hpj3flu6N5230RGcSvZ0g2
3mgJh+R+vBMP/R23wKTaIbbbl+cmXuG0mFAlSCE7LwOEF9ZgIUZ4MeIx8VU699bW19LT1fUjR/l9
yj8UoV5ok0RHKRdr0tf/d7yo3BLoA2XeyIVjqzcnzPjQ6L9CTtTXJ3XnjOHE9lBHNavHmraCwHvc
WA+nKhZIULdp6Tt+UI1jLeeJZ89eUqdk/1b34rQEIrsiS9JMGjVPCvE7bJOU8PogYnBOaVtMVTIb
94MV0rpEM5BZ3eSBGHBbOWsBun9oWJfNYdaSZefmeW2doCbHQmt7XlL2mZ//m7z8EwOmQpCBkcYz
e37V74yt7GxSZsyHvAmccjSi6Ruq7XIgRZeSQCfEMHF030S85PQ00MG6qC77bo4PbIzzx3Z9mSf+
mVodGjZVe2yHOlpyqLFyAfqfj5vaf8MVzfq7nj6STqjbJ9EhGDnkrRkhnyvzT4eThGI1caxbfyCS
Z8m8yHEoN54Pjl9zXa3Q3P3DjaIZGis7YpEsyiBOrR8KTF+6XGzU/jktw40Yh7ciBe8GwJIOcM1J
ZUn4j28yLNcl6D1Eor2etYapuvLrReQpoDRX6/GM/KKR5MSPALVPW7VctY5pcgaaltj+2TQkZ267
U/TbhLBcf4UadyMG51TQxaqulV7LQSM5e+nRI0kKp65N793FLRQ8hbLiWVcVIXNekgcmKqPeDBf4
RP/MBchSmk9evaUIVO4nxrYNVPq4hknjJRFgIpr0Ow9tdjKK68PPBNkLngKHGco0wbmczibTxIgw
EibJy6GwuP6sxpS/Iuez3rj463P9y2kg8nl3emptvrUPPgfXWgCN+yMsZf0fWjkGDKEJex2kFQu/
tUW8XcpgYwYOLW/2H/rj9xWzplZczaM+wBkSobSHcEj71/zVCOi6qDR15b86YT3HP1s0Cnv5H3Eb
xemATJKDNBCgrduaaabxjb8IKr88mLEikQWCvNb4d6StqE9ElKHv5H8bcMvO6R1i1TRxiKKAK1X4
KYwip9lkfDOng3B1Kjo36ViFWJc09gqOYi5KPyusPgTjWrOlhaIPU7WHYtdySk6BZww7s1JWHJzC
EpqkcyoAxZoYbzimggftC2G3KvF+wIBRkZlRwRmkQiYZf92NKPhvD/AIV7TOKdOU84+0Ky7FzRev
VzAfRzu4g7CX44EASc7U6YXIlqckIwkNGdWJgfkLOUNWsxh49EFaOQTHrLbE9dKYnTlPmJa5wv50
YeOgRUx6ifuIK3vWGxEtXIA64aiZYy/oIOdISzZW63wHMm25ii7il3kv/N8VyhKFQk11QBlapFxs
IrFWDn9Bl2SBqeuI9z21oTns8ywpZ6KZdhWSvmtoISqhW4KiUdX4UyKEFWNBgADtK+NGoy5eemAW
VUWk5efi68URuRi3xCtDVqrFbh9m8ThsDpvzdru7EWHzXTHN67ruFBb89Ih2mYjznJW4KUm2bXZW
DdE2MEvyQUaVna9qfBoE+Ug+ZCk0byHH/QwcktUn0a3oXJ/NCGZ9acn0Fgxv4GbAocRRXQxWyPpJ
orhWwRTQH7Di8XiuIByAPib5lILlhvwxNPt45owiZV01UzpEKQPfj9EMPWypXJxekA2Pj0F4PNeK
Uk4m967P6cNtAd0DHKttyGSLAdNdZwq/ctARmd+/Vw7jmVQTgDJNMe1Rp7WDP/WGIonG9/ckL+ic
kSdVnrYSxq4wHnIHChlcDKh/v/IT2geTPJFQyhSVCDmMl3bqehzscXtlxOgxCfG5J1HO10HDZ7s9
zEBL1JrrJJPkw0VoIMe3nJ2Zd11AOuI8WsMf7Fnkj/S3Vcoa5W+rIlflubRlhHklrJsiPcR5o+bf
hHVkWDwX99XUltWtp+x2U+gWRJQ4TTUBYwom1Kw9kMPTw8lLIXwM3gxEGT+XiiHazLf9aC3zmkB1
oLIc4iXJexkg/DjDlb+KZbFqEKjQbyeEJJ/oLkWoJfcixyWinNTlnzUB8x8h1c0b8n9K+j6Y+dk2
P9ifTD0MjftqEPUzAZ7dLQ6V8clocou4GC9imMvLthoXbcnE4b3MECqbv/rK1DL5qEN+Yh1HFy/Z
7YfQZw4hwg080geXZtr7ODXWJYS2sHGT5Wx4JXRFKrYICC8ApnzFlljLShLu6YzvmBHf1Ybd9hYn
XqeVd0BZDjxrhLuhIJ5rVdo2OvzQ0pFEtA8rMxlTexkt6vzHK8Wj2ilNZOZREJLN3TK2+tzqmzE3
ax2HoDGDllwIwbBkMqFenT+3Cmsbtk8LdFgD7hKQ7PVimszIis6xMp8/WkvGIvOCRrgiBWM8h/1y
2xYHKhlLiIfpHrdSGcdOJgr6CTM2sqSkRWp6rD9oq+1EHinMeRdOmVgqkZJcfad93WDD3Mc0qkQv
6qSv7b8km+vFksER5juUnS/i0b5fMPROxY7EM7Gxp2pCa/QCRhQ17SvNU2IyueamWvUfG8A/GQaj
Ap6N1DD4+C5LYQbqpOPmyOSRjvILVAyOFvYkDWU6S3Rw4t3ZEbfEhaE+PRi4W5yhGfmNYyXbXGcn
sjGv7Rx1ABxa4fzglEoxbDebIPbWDk7pWs8161dSlsZonI26DBGBTLsIqaMxSRFRXSkzAEFWJ9/3
J9CXKci2Iz9uAFt+twnvyykAytszfL39e7nPntU0B/XiJ3Rhc42v9TYOxbvGBirLg1RDYviqIH1m
jXypr0saSoQbO4qFDAEJNS/w4ejcJoCcFDEWi9CAEh1zfgBSVJeTHK9XUL+w0oNFM3tHEjdsxOxb
7lvp0d3fwpBgUI8pIWVJAyWq51tGZXhk1TFYkO/MGzmx/Ymo0L/QdzWiisV1kFmN39NnDweFvNZS
137hdC7D5PPUw+piXSZWwvRZQ7gVgyT7/DfKAmx9fVKF63yL7nqZL5ClS6xb59WT/047NZ06TfY6
pKI0fHn0JnE0H5B0A0J8akRy4CxELFtUV9gBJDuk3WhqBzl5YwJtjzux0w6IHXnZ3SP3d6zeuh87
aFmrBEFUBlMBiJ7iWFPXOmDZ5NQRfgZqCFsaz3Hujx5FoCYN4Zw9TevwRbMcOW6UkaPLt49Lw0cl
WTfLdXgq78k3YvtaNfXG6jzHgKx2Byz2qcw3C8BBT/9Curx/aZejD8CQ/PzjSeOOFBw/oAnbnCLg
qGa2XFW7FeEWXdLndCPJhcSATCBF5JDbcvwLhbNFhaRS54VHaL8QnURnw2w1+TJcE3GvYA/STiYY
2xp5xpbypnJPnyIzi3KwxatBi0rjZ57s34Z9RaDRx0bjqSLDycnarzKiUIf1eLDWGrCsPZDC9R4L
5KaSvj9c9n5PRTR2batrbrEyyNAjMtMC3G3OXWJH7da+jYrccLc/UjZhicBrCDdHjS1n32z6iQPn
ih8+UXWghDuWqt9fILokoeHLm1LYok99tn9qT7xgpHsdcX0fxTYO4zE+I9x3Dw2OtR39jHYYS2oV
boZqGbRk2/YS4rEH42uaf1BiJDI16w3FjUirpcC3QyGD9Mw2+wsfg9qdEtMSyBn/AAZT7xOjW6in
QU5kLhLi/0xNkMzMsP9ajFYx+HAFNOQf/1Bx5LUWLaSfR34SSZCSsaetSP6c+mEzdOQyqpM2edXy
sq7PjxVBsPtbL8JnNS1HSfGxEElGEIeVz2V8XW2sy34pxNiHDpnqBn881LrK+vKmxYoUfj91sj+8
7QJBInLNDcdVU7YL260lCvjOaZryUZJzhMedTyEVjuxxWu66Oa7BjO8iYLi7OZwRoPzZw7Ef5Cj3
uZmnEtf8MGnNix/vfsX+L73F0Etnih+nN4yamHQ/mOzHKUFwWYF8Ak321R0QHOxyTBHI8mdhibHA
EQ/4z3+91qEnHqgJbxJRG0nNaza3JYAjf6fhU5WGAzZQKpg39SoAF6UCjzhIXUeALZaXX/fQJYZO
BBhIpAoSsrW0XaVe1v6UuP2L6nylKDtfQ0/QaDotzyOQ2ACtjrdRMCwhmfOHo9ibjZO/4DPXoIVj
uzHqk2fnDH4GycxbOdE9MTFTLBmoPupI3BjRj4X7r9AqskCmtDBasonk72bvc71AxZ7nTBayESUu
PeizUL5eQfPv4ZQQ8IRMs2FBiCsiKQLt6OQifMgSiYiQ9ezDBz6bOpbRmHncQ2JgwW0mY3iH/KtO
dxu2AIgq4V/3RURx5GbIqHjQkRaxa6JX4kj1YQhI9ao4lP/7Vlh2DU1X1JVjLCvQIXRxt4nzL8KL
BowLbv6LTD9+8rFF2Qavb+GIKIxwiMJKWVruAaxHCWGeYO5ZYtjyrH1bBHcjvyMjkiVlTLxk/hUi
Sy+71KhplkM1g/aSvHjCqTnF6msylKcFAgnkWqm9wV6YZk0o2KTXgT4QxE52PTJ/S/FMO18k3Q+i
LuX0XNSMQOcOvsA+solvqUBnwOPTgVYD+CIZui1lPq5ETnzRGjNFcAEvN7DTDTuPX3BKkiNod2uV
7EXFvuDDB27QJ3tSv/HlyqnmB7Gokw+IH8dQhQxvG+OEnE8+lm1SYSofg8QON3YB7DTvoUfI/Jvc
YVKRLvxvawJQ4AjhE/Sbs2HJwxUsMMXLJOtaFvHiFU+vn96Elo3OPIHKdG8pxy145ky4EnNpAtxA
wWwUqXzcvRhsXo7rmW+7wVteExY56uhOlEvgrKhx6WLc7Zk4C+cVccuZxmXAQ8I2NRgJPxA2ORQU
xXQoJfNmkaD6sz5nvaqWqr7YhAO/dXK9AEDRJuri7u7wQDD3EliNKfMhh1gkt9X0EIkBiC7nxsFc
diirjYMsbqK2DGzjvn+HIT3MqqeqR28wWMhFQeMNFhPO0uZYmrrpvq400mp+4IQs5R7f/pQ7BYrh
wxJq8TucUwkFc1RB2syhIufKpnSA38ougNh0BSLCqydbUJGFjEhWwPUuaQDjofUxRjbfX+S1Zopl
QHkRCIVaAVwtogs9GzCue90sMJpgyP8MnKtfTGxNke6CBke9iEjbJVgeKixwFf9LEBABBHDS7kgN
VrVPIskDbENJGfAKKT6qphRWwBW83dErskQ54GcB0hJdTwJOIOphXjp4UGm69IoQmZS8iE6s/96/
R0W9QKYzahfxCaIrzhEsjRWS1gGzKQqFduzT9WRC09ISTHTsJkHJL0FZJ4W7xm8lysBvKd9isSov
ulX46KEmwejWNiZFRG5/tXK0evRA4UdbKZ6g6x90Jut8zOrWdW7r1WfB5RM4vjqELBawO1HhCzZ4
SpgA8bqktrHI0JURiePE17h/8jH3lm6rMUJCx1CvZiy0YVfPJ66u5rUcbiXQChvO7FiQ0nCXX7lH
DpdqyWVmXp9JWUJkDJnMlSfGBnclCiDayrj1DYvYbM6bT1bLHw6IWfX4W4VWoHGgMZR//HJKXr3T
xkFBYwILIJL3NoKSNn7SXnolrm4dEmoMxcuOOrHga1nQCF/j5t69SwaKU7GAlIT/ytKhyZF7dRw+
IFqHQrDV6013r+DyDobfgkXnYvgak3a0v+U1RZfqDgAbyHSJew+Tqrue1UbtoBcxK5InLzZQL0kA
lMvGyD2yIhR1FnxI3bJU7pdiGPpmuZF9/vjciuNE9sTAtX1RIxndPvu1p8CIflbSMzVHJxlcIWRp
HEfVLEahK43YRka8lbKFiJ3NelIu3NlG4F1u5jbud5wlsAxsP3RVR1nJoYUuo31nUvTJzeeXFYHU
EwjWpjaNCIlcxfE18mX2LYLj0JpnSsWyNnv1SVi6CcOcPv9XQnVQWI3E5Gd0Tw+WGg7eWj3Lr7ju
oXknBulkAl0zQ9+YEEQFrU8cgmvzoY9hX+UCOQH3lQF4ZHFGeohCeOalZNFDNjntk5Vaolg77PPq
SXMJJf0n7CDbrUz0fI6fUt/1pD6Fg7DGjKPcxP7pPdbZ1k/KwS73P1CYBZKwxwqQQfj86KEumutv
5qDzZmoiv5seABmg2evMAg5mUXTmLjCcUawPXm+XxvsCrXCbxhLv9Y92vLk8oItrvG4LKNR9+Wmx
5zPMRfExzIAnYNVQdDG/O5LsdRNSh2jc3/3VLITrizOW7k1Wn4CueHzktcTlJ4fPeeLIkChAwh2d
+UiuXuGihZS3LbbBbJkHSYfrUYBP+AHt8+HHc1ahVoBtLlzZDigbrYRwkdjmhG77JLCxn/YU55pR
pBk66jx/o0Mf1gQD5RYimv9ayrIxpwH6odffLEZX+pWv5wj6U7t52tArtTW3Lsbv79E+GYG6fjDK
oS07dnMcL9ppczqqkmhjy6LcumTLt2EmsaiM/5iRei6oxqHUxrwRXQLDcuZqb2OzF9UIW4nIua4E
HnhDGYMMVm4cUcHSx/BqCEwvk689oBf2wYauF/Q0ahDf089Pf65mU+qbcEI3FHjcW7qgHvbDEVMM
gewilGVdrZPSYYjjwRr28r4bA5FZ/01h6HNQL1dgZ6SBWdzhuH/B1Dwzt/AD/Ec6DG8sQcJL9c0m
MiwiU994Muk9XTRYUiFYkO+RBdS8soJpyEosIjHnWg4hD0X54IGJKKxuhpNaLu6jOvqyx+rRaFTX
oPHUusrm39nPwvuWhLdPW4eo+7PVqzSTrAToM+RJuVqotRvcsNzIVOfix/FlO0/igS20U69K1dOo
rVXU9L/aAY7bk2GzNUz8G7Qmh/ZzcChk49qTbr7EKFKFvP0Q2sXZHk4i7+g9Scpwcnx9JmsYjrrR
xnOuO/AX1N5VeQSKLRbqgRRWJlKTjz/f+U+5lqEX7BQtUaubWyiqEIpsR1j0BBPOYvHhaRc2K6gQ
GnSi6pSh3PO8gMYn5O+s+jnV3mboiuNg6JhHIMJT9VFnGOhA3b85/W4/XxVkIsArhXv9tSA7mi8X
KSPD/sJCM3HvNyjRJBolCbo8JdAP8DZwcwYegpizoEMqLN1ozbqVqzEopmfH8QNUTxSbMLCDcaMa
RDbcRfTj/fdndou5NMaS1kWcMyClIPBFYblrMvbsSAV1E4DCHLYgYxR6dnE1zxk0LO9w/HY5QrB4
pjkNE6f/wF8s3Ay1mqbmUj/WRNCn63vdt99rsS4kDOTHaPKHXA3hLCqtMV92/kS6gC9f3JL9AX15
Z4FFojazrO+dzY74KPHMfWh/3VuNIIZzkzwhSrkFn8F2eNyHpa+fSt3PEmz7tsjd4ALZVeX62KDr
v+kvEcKYnDx/c3HSqPOx9A+jG6APe0+tX8MQ2kYI/i4DtxN9AnlDW/gjNf0ohOd8h0amicOCmIln
eVOA8NV8lsHWOiCqyXas7/FZGIc2w5hateZdX5XZu2d4HMlXQDiGYZDsGsx2lBZk/Jqfu278Hulj
xUByYH+V3nkMn9tMyBcw4wrAlYkpIbECKeX242562LoOROmXvR67i/aCwm9+la0bs2f3cOZZ05JW
8pavCRMLMjCGyJP+DY0DYhvusVYDn2sJj31owwFSQ09u1A4taSGatIU6Rw0w6JOijVjdnzlnaKf7
jk3T4jtyaJAInRWcKDbgzKOkxBtpq4d0Q1feMZ/YYRi9GocCMDeqFtmOznRzefx9fuzzvuu0wMqA
/k8ZLjHP76ZfXgR0ndtRuYR+s1RInc2faTb7gFetOHVAIv4FBbHev6Y4GfrNLj9TZJPG0dy83SBu
70wf25P2lGatxIBI4EmyMLqQ+fl3PZ/yIRTdB+3Q7PctFf0NmiNMnSPRiq9ps6a+/bKJNvrhVeev
8RgIePnz3/rkHPsYIynRq4TNSbWk+JCyT1X3O21NRTuucdjL0fAFK/VyWnPXdfuvRnmvnJ5S7phh
mzb9HBdTH2eLZgX4y+BEhQy0CpwUvdiVwAxJYHq5GSsgiJq39Un174ekcbvL4qMu+O3ag7JqPXA1
iUHpUseIR9v6kkuJqPifedmHQGimQ1y0ra12O68I7Ns1D6Ltb2NTyU0FDIa+Ogxsb5awnz229R++
jLrhtSPScfdvBmbCaqJ28zP5g11D2ru0Eqp4TH8D2UjFw16sUEWDZqZLhfxardw61fDrBmkwsG/q
mcJakuM6d0CIa3/CXJkZwh6ezBDSJjPyWPJdW1/ufG6IZ+FCmRGhuegySvbrJJ78CO0FwubAkV4O
UBOuMizm3eVjM0R/M4WxIobpoHqrNKFQMviyGU/58CX3ioeC7FQaqi6dgvHvydd3gizEVfIV50Y6
cBYLE62x6+zfSD41PLmKNELYqmGzZNi+sJwy95QeQ9w6EvmZefEDER2ICDlpTWjsajTS9rGVnxYW
zTk7AHus+3I33Yu7tVruMzdtPx2P/5BhiKVtrW2J4HO0LNpEUIpyJXQcKCKBpCPjTdPiS/P+6WfS
T3uQFX/E45TYcuv2duSIw9wgz0iq/U0+li/Hbn7Sou7ydJpRYXwS0hTJXkslsT1sDj3wd64xDSws
V6USLiwF6rkAffRjKVCXBB+ZyE4QkI7fPWlXw1aoFqRS8G2gNYB25YYQjWz1maFtwmh/k08cMTaB
AX2/fRWqP81qrrAYvzfsv4bxMZoagXbATJ33Ddtm5YlAzGPcdjXdttXYLHzT/tPM0OvDGCUkbQzm
AwbKY8g3ITimB0Yu+0c0jIOzj3BW0jl1OOZjOiAeFvThnfUr4/E400JdjSt/AnYJS6hW8iCKS+h+
nyD7bstbQOqC9EfIb8pJdzERXSvN8ixGRFVBDF9exbZ1Xm72TgMaUcIRefMNJnIyU8yTT/zqtvqS
iz/K/pQmfIqoeysiaDeS9KEuowdxe+kXkUtQkIYfg/QLXq1VIpsYByGxzigj5L4NKrU7IkLHbtFW
HfeaKAMEE4sCCLN6SzMPUXi/PrspU+LR8wFvvYof33b3HEuL9KdvnNQzsRVVbO8D4wJ9Td0H60gF
ZDOSfqTzxdtgUpjcxqzE0Hy0PQftf+wBeE5QBbcJpIH4YMtvcz48JTXZLw+5KoF9Ekhjgo8mZg0t
qMRwNj4DDvJKj1GJtzJRU05u+KnUAXr6Nbx/7cH/yd/v11LexHkLoMXfg3Ez5nVZ5lq377Sdn4R4
0zagYE2QlhL9Ma4HHCFzkTwuB1gm2m3MvQd0EX79ZUlnfc4zBYFaoQfMlB9n0XxMU0VMmDQShr5l
1PN85zGkyy5HJBE9DqWm9E9UCxcO+ehhnoPgonBkkQLgGDfOlYK1rHfhgS0vSimMwHV0fpo/aa1R
7QOza4m87xLvgGkhcnJBUV1Sn+Ozlr5i9VNBNZDvl98MZOtSZlwzFyLkGSNViHoPmvnc7+IidFGP
O6ULpEyrYgSAValf+5AM+yZ0pG4GKCSShaYnUAIx5NFIjWZZ32D+UHWh8GXycigXNfBnv1w9f1nx
4v6OfxjwZFKo9aSiuMJMOV+ucEav4ffmpGKGPMzxfscU83ifVZRHgT9b98geeSkpCd1dCFDK5Vwh
ZIYg0kRnuMzMvw8iagLbaRBzrvo0FJ92yWa13TpIlDNMFzMADUvQ2KEDeRMWuWNnzGIJNm2g/p2m
rOPwJkd36/Jqkri3jiOxqADpEOK4sdoeR2LmUhCH8iq/2uNrbuSY2bOrhyRKJPKxTkHspI0D70yQ
HYGjkPxIvj59jVTlEq9IcyZSvUN+1nUwlSNmnvCVoK9Otibg7OMMcYKbjLGABZLnVjrLJpLV7zwe
LVSY9ikaspxFXSbECOYz69/YrA9AXX55G3zL14wc0wd/x8NxTW04bfursiNBrIfspGlE7H8lfiaE
caB7qeFLgwyzeFTzDXS3ZbfJLol2kXvpWqGyrJuaWjADBbxKLi56COR4pnwa12mBy0lU1gDxZtsx
/KMmQWycbw/RamJyN5xCeV+AKfCLcgx0U72Pn9+mfzg/l2jXVr5TqZBoFfrMDkZJG3J0Sxb5EhZz
etzUnwFBLNIA0fVlyYFN0DjyL8NvbbDNCqVeDFezH4/hMtqq7PtwNSIKyHFANH/9umJ2C49QtcMs
mGA2c0JAh+Gqirup1gWmEO9f9Htu6vSGEAN7xP5wgPCcB9SZD2rw7Yuy8yfl8z7Ua4amJJnz5FsM
LVg5iFd7YOiSSJEXMlJjxK7Cv4h7+3hr5u5httDGR0KXA1sFCXcC9qjg0Wo+BvIbGeWLEGdFZPI+
polNxajvqCdKHdpvWMc6gSd8iSk7Ai2q6OUz+F+KC/ZBCj1Wl5GXkULfSxGh3a3q7qNLm/EA0zPm
o0rH3uHiQ2Qb1njk4dmULpXY1iC7cfYR2yD/y9F67nUc9UiBagL2JQYLQ1jpd4T/YTm63AdZ2NBJ
woaaTlzy3VJHpS0GBawv1fcwHJgCff00sJhps1zdcNms51abN/fgLdTxh1eHx/MZyds246XcmpeZ
GIOT2PD5Lbpp4h9uaNqJZ0mcrfxc5BNNZ6ZoFilXgVYbe74xHsoLeCg0InMSpOS6VZTQVLvLjYOO
PCDF7OQVPxwJqRaRnr2nB2GnpDxP7YS0iufaNu3ii1a+r6VlzKrqn5w06sS3PlQy4O0UKxG+o8q2
ELr3Q/ny3K6VB13w18OV79d0KY2OssoWNxvi/mnKfDF/ds7mSmqYsCA5ipRcdSXX+oFx2f+k3Ly9
Y8UfSkidZ7uoMOFMRh+T82ySeaZy/LgA/fSB1Paf2/FECS3LrNNm4iXvZr8iJG5c6DWb0ps7HaWh
Xud028Vyw6pp+v8QPi6f/1Y2cWe4XdJOXHO+HNReZtlcsiYx+We8CRxNJDhDHV5xS1s79ZO+keFd
N5F0Talpj6p4oHHMkBk2Qegg2fpz9az3E/6dpj+9TySzggVBg90XCjOGnTASFTrtbg1j6Sx4VTSm
+5tEN5OdgmGJH7ZSsp7aIz4Ep0zZIFKR6293eqQ1Hfy552bMu4QYc5EP9buaObVhRt0qjPim2L1e
7Tn4jO61XTiKmgk4OBYxK/oPtFhQ/+d/lqh8QqTVfqGI+S7rYNTjPVh23NH7Jvo0EsIAdmTyYk+7
WYTUJWMkyhm9YBdFq++EVIr7TgO1qnMovg2+aBoNe8h/am/5Vjed5lLwHPBTjWvNB2Pn7PEHuWwz
2FBGEL+ADkjq/uCeFzsNfCscbFbpJkE4nnLlAToJq7JCnEkIDR+69Kw3fWBn5eDEsOKJrZ8L0lc6
1Ym6xt9QQKsSXAs24aIXhlsbMo7F3JqFRlI0BGRqgAKLLZkLHw1x+h0gBz8yQ44oZlxbL91RXHsT
nTj1WoHDBjzmpJ9Ss5YRE2tEKLocEKVVrK/a0My1aIsVVQ4S5nzB/+42Ce6ItR9boxEBctkffk9Y
GE14lCTlivzFiiHwQYkXsMZycWMQmgwvjWhLOmkUV8vE6jHBu36RaciiStdsednEVaPXE1xI5HeV
yXovVlw3oJ0mSFhThOvoAe2M/ekFbhmXC+7WE1jr229LWeJMoXe3PLdLEcEiE7yrjzu1uTKV8KkU
eXyojFYDbLPQgNd+FaWQYYwV4pYGcZGkNRhmoiHwlta2hO1ShqPIJzB2YfeQJPVSzUZRbA6B3OYz
1AlsKOdSFCrTjrOdmMgCYU4dC3T3rzlRzroA6eS2JUiA7U6rqqIAUeAAbDvbBzWThynk6s8ZzHnM
Pq24PJ7ehx4KgNS/OS2dxyTYVnzRTYJKvD6C9G337xVwbIvGk5ajmfgNZP8qLyNm4Q1WJf7Nwm4J
X+8EkDVFt+RxK6e2n7m+IZmcB+HBDG+gjMbI4DDe3qso7Dax4hO9EhxBBwJWvxXGy7dIGZO4ZWaE
7bQ/bEBBeNlAAL00gd5PZrIn0lDbhy6tNONxKP4KEV8/LgtdrudIkEPtaQPE5Ap0pRqOKN3DbMww
c3IRxEeR/VkRuGYQuzNgJl7CPeeqKAAffmDZZIt2brxT5L/EejDQw5Pcbk9XHw3ExDfTdrtgefTL
tKLvoJQTQ7BB48Os+X/2nlB/9cMlX7tJcpONE19ojuIGV2ux8K13CtXiWImEtQH9NE+GCyEMufl+
vCO+jat0ab57yZBXa875ehrVZcXy3OAZc8dA3hl8lhVtd2geRGqoAVQlw2vNWppgftCzYOOy/ekT
S4NY1AUr9qkMi/DboDTcKNvruZ3UTNj+ibw01szvjx7Pj/11vIXo3V1KlIUO/1kfmTfmdnhr4Hf/
fvPkM+5qgo4cazS3YavCbfoE614vVDu+ShrtGQeaQZ6bFiioLm8Xla9zKFoCqpxshEr4T8zJ4lyy
4joIGS0vQyI3ZgvDPUxCBa5JQPig4LCUE6V27tJBhWn5oV46t2ICNciWAgpd5Fev/LFKS1fyKNmt
+7AUN8uIqC2qnzuBo+3i6l6TJ9Q/ZjcZt/frlDCTk2IKTUntwB0Fw4jX0nx+5nWMoATbQvpdQLnw
KeW7gfORlVP8EmssHilx8ElDAWhFM/45OaTIvj572aARXy3XQnupuyqp7NRKQNixrFYCI/xTx+V0
loalm2Bygq2FCJn6qbAki1liloL8kLH6Vhh9yfyRu7IxRxDAoBBKHSQwsBqxhLN25wDPZokD2ZTf
nJt48KRNgXIJRKIpSLPk+xBNP6OQ0oNgUuvw5+osLxmuO121wdNnLJ9nESdqpktJbzdY0mwhIug/
tTQxdyaMhXDE3LuxmH4/iXn8M/rZwGbnAQidgfLjS2hN0Pp75SKWnXzkOJ2Jl5qdpDpmsqG/GTLH
froiyCmnkv0C0PtGnXW6oZq3E2/sLEjwfwJ5FBhzttuVCkikuT6SNRsgBPG872+5PR2mGhBuZh+J
fN32c41c+CZsRlunVuLDB+b8A3V254EltL1P/byAeGf6w1qR8INbbI61qkEr1okraU2jcAs9bNuq
0N0ODAtDtAmoHPD8N70CJGqOeZ5LFkPSDJ0rcfRqPIOEYMLod7wdxSWIEAwsrmO5Bxshbjsxy0dj
+Q0ywitJjafrSfxJqWt40bknt7XQZ9z1SOgxgLVczZQfR5LP+g/Vm0rw3XGS0ODah8655S9LD9Ob
ndMJHVInwf4VBHXCA6NIqOVBQUMkn3wwaUozDyTdDgoUGKIgFngJ1QNN03f2tA3rDP8goRjrxPDC
x7YviVxjarsd0afh8fN3fVUHyTfpdC04VSCLKvIFY+AQOnDe7omzZ0THVRhqc75ZOr6e+NaqnykM
BrPbiyV0gMtePKF5UrWeWOx0POooJS9RDYm7yapqXZ037Cuplb+XDiSW4EfBMGutce5N6nnaHEKY
VB14zbzvblDetLx8ahGLJ4kFc7kcG7xUiOAgDxXWcmp12c6YtQMSKL4iNJCh9/mNwDoq7WoebTWZ
EaeFHQ2f5m76YjASE1dHFAo+q0Hj281BLYnzFL85WGox3kE1r5ahwPuxSbp3eBtdPZvQ0qVxF3ff
M9FuDNJJOAiDZAXZ/QdNzrDz0xVEhL71u5bOXjToSbJOrFOwmAlCamyLXkJUcWz2KeYC2PTePcbI
w3+mxDwD0U4GlmGr6TQ3/4s7fXSKdvJ02DfgJiwkFgZUwRdX5RvwIpcjU9Zss95fURLz8BQcZjP+
ZHpx+dMh+gUwvE7cMfMdnkwHevHh90ZZ5m7SVSUqYfy0ztJ97UdXJ0LJDFse8kvb/TEiJKgEJbLs
GO0WpSnqtoyS0ZAuNmOWUDBkE73fGPGC+UGOvYEdGpwszCB1JcHwwXO2Hvmg/10ZnHR6+fspx9BF
pT3etsHAN1wim/nuuXP49lF8QPk352DmGlNlKpcDx8rL++qWI0a8G3en3Jc5shQyA44FY6cZe7PB
2XrWN6bnNAe1hLO7sPWV2xVfKVXS+MQH1EjZh09zPfs+o6RyRE9ykwi68i3lUrMfrWKTeyJRHNr5
6BubMtO4a2nkXU9e8KKOcHUfELb/irv7jHx7/5jxKR6SQcRvjQTQ/36tN/ehdztdzfNM6ijVOLy2
dGLbvWOD8EtteO01B4TQ9YRtXmI9D47vTjpln3aF1QC70N9lTXCXN+vDGh7oWnzcCKfhYMvtYOdE
BnXOI/HM86y/0svvZf+pvkaKgE8SFSSWTT//cd7qMUcHg7bzBheXFU+rdhbaaMrdBKvo/M7Gc1dj
CVMBKCEgMJkjrEK79iK/rEC9TGnAiTZi7RMrYW4hwp2PIO0CD17Atv14wIhCTaSrTlfEvC5eJqOA
qFdYt9IwUaX8p17NNoIKqFkZxq8Xis03TA9hhpRClp78LSsLSDqSNB9jUL9Oluj1WPNiGz9EEXsD
PRBe704zdCR8eG3YHPbbTgunBorxuKGTcFHk3dyvIGx9FOp4tK7+ISJuASblUM9WfVmozSS3Lb1P
yB98Qd7RGRCI2r3OXdX1sDpRHSHiuQQTVDX5mRuFbPtwXEZiWMdsWwmPqotfttjTB1YX+583pdyE
gC53INfTa/bw7iDXT4TNYV7typb1T60sol/2TdHo79drGG5UVecfU3IS9aT4iRLbMvnmlk0D5Mcz
0r24ix45NxMCzASu/vIJZa+ArbSmNFl3cNqgOaHx2RCmqcDXR/+PJSTyciQkg0UxB1jH3Q31ZNtL
p7mltcEFbX5b03b2d6nqdQpxr5VI9/lOQO9cXcVa6+paqP1U4qbd3dLe0iXMCHSTsLaM8c6s8nzJ
lWenHsQANvgK+u5zoaoNnyCS52YFgf89RjWqZocEHU8hA7rF2hcKvsz6aG1mpkOmM78rq8X4RSDE
zzXMm35rZDot+32Ly25ckeseRvluknpo5oO7FW7XD4SLNVdB21LrvPpx40NajORERFwdLCZlMIHH
h7qap8hRH3NIrlEUYQ5PKXkudzqVO89rfylLIBtmsWAqokFzxYdHSrZLiSZ/aHmbf25gsnSo2E6z
MNBFVmEbawCQE6KJ4L3OzTk73Mr+QX7T/mQQ4ix0dXGjJBcm5wPJNBQhTGJpZ7wzFeacaAbscJCC
sHivGxh92oR7sl7SG8Uqg5YVQLBb3FGIOjZeFwi2FXYckkUV0DiJPwV01s+bKgT4cKucILBEAKOB
CoYcLno85sifzq9SNeZsZGgdtvW+axTXMOr5Gi6QRUYBKcOpUL1OPiT9jD1NKDg4MqkUcaHipFQv
Q45H99OpGjx+PeR+utOh+AQqefB6nMo1lpNT6GBArNG/cRcNAI2J7ny55dqdQMmgTRWrQ8MpZp1S
hctU3LTHgIzEcjDzCb21Kz4YkU+fTQ5xvoWX0zeNNXpjZqEzGuH5BmkXuXBZhnDLhR9qW2NjV9a1
+6X/ZMox14gPfvEy8KG6K55wyt/jpoK9Jlf0ZDEMEd2nuOtw6zTBXu+WOFWpPgKYOOTHbTChIxK2
5pQhn1y5i6cOv3luo2rOLsqMrW4Ihq03f/KNNo66dVjtTfS7HnsbPNUONV2v+uffzG4IiJ4lI6xp
i2lGV4kEZRimKRhWJkq0EWym5H19RsKx0d5MMJZQfVmLQORp7i/9FYT9BBdIRkF8iDgjHsfhjJNo
1hq0ILlMPpF9Jq+jXIpA4XG+XcdipsqrDr/p/e8jkagliG523plrF/lbZtfQCPeh3cyohxOJvNZn
cCkN1aAeXjOVSj6OQvTn17N/qU7KTU+LwEmDCAtQHXB3QxFTOpw/te7WikLpxmpntQaQoJV91lyR
+FJcorGe2IZfyWBweDWHwoHltW4MslaR51jVJL9sn4dkHeVABCKmkejhNDVAP9bSnDetxo9GK0/4
uaY9PkLjvmNxDwgSLFq0rv6KY0d0p/m2mydeGCnf1v9e5Kcsha2wm1Co5WTNcCWWwPLhBkak9Wln
K1d5LZaZmtRM3f6k5cgZIxZmJO34eoNkyzem5FvKJZV5skM+X3Tm5AiRzbTzRJKOS56gDldsO0Y6
EnoRdXRGv7vajG7yePlrSmcTEBT21HSsE+BWiyzfKPBnQbqn86+o/hTaohI6IZI2Kp7YSFSpuoFq
Ce6UPkT4VaH5zqg+XhUU0Lf20TJn7KtNEdgIqZVosVs/6xeqmTmVHnMNqU4Q1cNLMJysgy0o4PYY
evAUVekCqQzzVp1a2Wf4y4DWxffq2WTUhw+131VRikiU9c+zhpbYE8bKqvhq3R/a2p7qrjiTu72O
rgxN5jlqHXbP+joCpD7XLZ/xGAoT/0pv/UBmIqBIBf1asrYQDdf/N9bRoi/dZQEj/vkcRqwEgnNE
p0GWHj0oI7OhB4lXhncH18epGp2j6FkAmyffNKfaS9f1sYdou342esFGJUkH4S+1ZuiTpikHpGbE
lT6kp9VOeZeGlkQV/2z+luiyFS/MiQJIbEOd9Rs0dSn95AeiwTZdBhwmFptG8fzb8ZlSjxMc97u8
PqoEBR4tXWooFW0gh2tukwjsryXgR1GAUN4B736rNNCLoaEVY6Y9SYmJs/sjZhqdPpuFmwmL3vFQ
0XUcbD5scycB+G/i0YwMqWoAw67neHBQIhinNzoNkDpFo6dmMbL9MtCW+Na/QrKXRrKqc0yf3Mlg
xYRIx552HpP0xb+PtOrQP+WfwWsXFXnfmVu6evTVTEqx9+OFe/Dxd7DkLDU/anSUjkNoQn26x7Dl
6UkJ/yBd6qOd5jhdWEquSoJ9zWzGnY35UPDYciIGTz/WxWEmQCBYlhKfRpX13YLYl6TssWY4T85J
V15yvPk6/XnwP/lkhD1WZvCWiDgYeSrjr/dImU3evQztzNiCmeZPy9n4eGv9fmVp6jfR6uVsjLHt
Dx88JZQFdS3E2gnpbxhvcRjxXN/CYjUYkLqYev5xzT4ZPMZbBfvpAuJi63tiY2vvCnCclEpQHpW0
StXLVRzbjUCeIpbLnZAxNVH/Tvp12bKoKYmV78dfWiKEikodYFRsWdItKkVDy8PIIrd+FMuvFEZM
iurstKWwRwGFv/gLp21F5qAU+QxXugi+8tkl16LdjJVv4E2sHrlw011+kmkHxSXEP7FmbHnWdfS0
9LBqVX9bfOrxg3MWoU4VFOZZKZfWQYzhvFZ4iI9++HECLNFoYBNVUvVmKRpolAGlFHSLIsFlw7SQ
cpzWISV7MO19zNC6C2PBgeXCd9Nm/nb/qasABKjMxkUJXdEg39Wc2scXePHD0hURPwLH8MWhapAg
noZ+8qr+D1miMU40+LRgAPXtIzhaAVNFLe6p56PqbEXZ7aepS9iSg+lMmjke2ooRxPSrDobFrbNj
PWJeUOohnQswZG12tX26/ZBC1NuhnshqEnvTjsypBru9T6iXyDvaDuCHREQhX8I5O2e1hOQ779Lb
WPymJ5SQIrRMcfTD3Z5l41TWc1GAxdKBnM1s10NtuCQI989GtVLbQLshYioabxMdtZHfIQP0uqZp
cRb9jyNoRWvTs8wXPsCkhynmm9tLVsWlm+7GE+zAA0sfOy0Gs6QQ/buqLab3V/8nHKYwUcEsL9te
GehrdDy4wjE+tcOjhAzSj7HMCI/Gf/iEkKkPc+FQtfptA3O6UKTd7+f4f0/vZbCNddwwia84AyfM
jOtmeM/lUbDjeXGkkGZ8/3zpZwDpPi7wrFHxDqnulFFgzTGnbTRTVfw15yxVYnXHkOqIul53c2kN
17aJb1JXoyyK0HQCfBKyymVswcwQF/zPOgaq2SUoDrm1nQ4hC6g4ZtGVYzqEthng/X7WyS6P2z6p
l9Dw+pUUNlDmDAVk87hYow/tUMg85Mls1E9UilBfDeQAa1iIp+j8TqoG8IsCjV1WXXsg/vdNpvSB
WX/dFgZ2mWccb9JfUKr7Do4pnKAb4TDDSP+durbZaFnksmHGKPi1f2o74hEnwffRrvpP2mGom8cq
Su3vuAG7DieoCz/GUEgzIS7eV2Sn6xxjDe9XX+jGIXSDUf9GNDmI8jt2L+Dw3Vyo/u+iTSkIHD8I
B+84nS9WUsdXduZaxyCSRKYByJEdfYk6Nxg2F5KBJW2d+oTPJ1WWc4H0wooZ8totRzGuz4mbCeMW
7wxpXcgLMSC6nsBDL0fGplfq4+J7w98/PSKoknspavNyHKS+XphWh/9D3H45MYo5MQLsMEU5POfr
029+0g00J9b3dwK+bAVh+Je4OwePHx/OVyaLTJdPWu29xx9lyHUyU1wYjJeE+gE+WDWegmAx0hSM
P0fGu9gJp6n3STaOwn8PKnpSZ1zed8lA3fNjLiarvQx1ZQ2da+clclaatSSHHxJw0P4KsoBWPGJT
ud3tnWF+CwQqRTrjpeoNty3Ty1sO6aVrM5+TBkYJ2+I1sX56YQjpNq/YEA4uTmJM+GSVtRsQqSzD
KYcARxKwIoqQrNJm/1QG2wwRISjGdqyKVTj5zFWj+Q1NuDvxHj7R/MlzNJueaQqeFkHZ6Jo5sFWp
YxLunB0HCSzyGc6JZnOoIxZ4r1JQs/cFhDMrKfGNkRPZpuouCYD9iWsO3bjus/ToPne5UeBqvZvo
NeH3SedxIoLI6BLgzsrIkeiHVXLMVh4gTNim1ei9+P+ks689KRY3vGK+RywsvFtvdxbENreumWcB
f8XdA/p0CIJ4Ep7rZ1D1U8NDzRIYytiKnWl9nmJhXx6TDsDLNxY2v+Ow7vScVEmLDr5UCaXBw5kS
iQcQ+cAY8jzvGpqBNVSi6clI2CXkEBrF+RF9dKhBhg4yfRPsGcGoBIuW75khekjNAzUDW7zMvsku
V+4elx+B5FNqW9gjjubteBHynkBf9C2cyuAWwZyphT2ol9osgjwQEjtXRvBSxthrlqs//+CwUnH0
9q9qiqonBu3qr7lkSAPYaNWPASqO6r92FW01a9SGuelOUGgFDpwxVZwMVZ3KLoj3Y42F6+pjgQqj
kmYkLTgRoPxCWvwfYg+tWKnxwhbUL4tQMph95b/Uum5mL/HGKTVrgFCNcU4ip/9CA2Ar6KzWCnM2
dsAEKLaNq8AQLqKvJdWpe21AUfEQd6jEugZAy0Z6S1pdLIK8axyJBw2V/BiDwM6FCOX0MkWC17bS
gP7BGSv4UWcNmw6YlVbRT+/ubS9ghxpYHCsXURTqCavIk0LbIIY2B4Rh+Z1AANjZgP/O/b6vhO9D
PKV0zhSthXqUmSYFwcBFT+cQVN3JW6ob0oG+nHUdMAK5gT/gv9jUQlAumpie0cAB7erERuVNFfFo
oqRmSlA/Iw7aJrXOEw8AIU0POlQGgo7EpfVhrOMX2eAjd30UnVSSUwcwKfTkN3eQ+53OL/0/7dJ2
R1F4LffFWx4iU7jbCCJrZfbIox6W7i8mIrrny6+w3hGqoKdIi3Pi8xb8G8U9nfz3yoEmwq+LSAtr
J3TVwH4ao+xaIRZ3fdFrJGZ1fmuKC/fsB4q/inqbTushmoJqaHQ+nih9mTXvhw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
