----------------------------------------------------------------------------------
-- Engineer:    K. Newlander
--
-- Create Date: 02/14/2016
-- Description: Digit Decoder to Cathodes of Seg7 Display
--
--              **Updated to include 0-9 and A-F from ASCII input values
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity seg7_decode is
    Port ( digit : in STD_LOGIC_VECTOR (7 downto 0);
           cathode : out STD_LOGIC_VECTOR (7 downto 0));
end seg7_decode;

architecture Behavioral of seg7_decode is

begin

    --Create the digit using the array for cathodes
    with digit select cathode <= 
    --       gfedcba  - segment names
         "11000000" when x"30",    --0
         "11111001" when x"31",    --1
         "10100100" when x"32",    --2
         "10110000" when x"33",    --3
         "10011001" when x"34",    --4
         "10010010" when x"35",    --5
         "10000010" when x"36",    --6
         "11111000" when x"37",    --7
         "10000000" when x"38",    --8
         "10011000" when x"39",    --9
         "10001000" when x"41",    --A
         "10000011" when x"42",    --b
         "11000110" when x"43",    --C
         "10100001" when x"44",    --d
         "10000110" when x"45",    --E
         "10001110" when x"46",    --F
         "10010000" when x"47",    --G
         "10001001" when x"48",    --H
         "11001111" when x"49",    --I
         "11100001" when x"4a",    --J
         "10001101" when x"4b",    --K
         "11000111" when x"4c",    --L
         "11111111" when x"4d",    --M
         "10101011" when x"4e",    --N
         "11000000" when x"4f",    --0
         "10001100" when x"50",    --P
         "10000100" when x"51",    --Q
         "10101111" when x"52",    --r
         "10010010" when x"53",    --S
         "10000111" when x"54",    --t
         "11000001" when x"55",    --U
         "11100011" when x"56",    --V
         "11111111" when x"57",    --W
         "11111111" when x"58",    --X
         "10010001" when x"59",    --Y
         "10100100" when x"5a",    --Z
         "11111111" when others;  -- display off

end Behavioral;
