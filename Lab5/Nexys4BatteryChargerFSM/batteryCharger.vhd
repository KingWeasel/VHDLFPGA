library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_STD.ALL;
use work.ALL;

entity batteryCharger_top is
    Port ( 
            --System Clock
			CLK100MHZ   			  : in  std_logic;
					
			--Center Push Button
			BTNC			          : in  std_logic;
			
			--Slider Switches
			SW	                      : in  std_logic_vector(1 downto 0);
			
			--Seg7 Display Signals
            SEG7_CATH : out STD_LOGIC_VECTOR (7 downto 0);
            AN : out STD_LOGIC_VECTOR (7 downto 0);
            
            --LEDs
			LED				: out std_logic_vector(7 downto 0)	
        );  
end batteryCharger_top;

architecture Behavioral of batteryCharger_top is  

    signal reset : std_logic;
    signal char3 : std_logic_vector(7 downto 0);
    signal char2 : std_logic_vector(7 downto 0);
    signal char1 : std_logic_vector(7 downto 0);
    signal char0 : std_logic_vector(7 downto 0);

    type statetype is (idle, init, check, slowchg, fastchg, donechg);
    signal curstate,next_state : statetype;
    signal MaxCounter : unsigned(31 downto 0);
    signal startFromFSM : std_logic;
    signal doneToFSM : std_logic;

    signal battery_attached : std_logic;
    signal highc_thresh : std_logic;

    signal resetCheckCntr : std_logic;
    signal incCheckCntr : std_logic;
    signal checkCntr : unsigned(7 downto 0);

    signal dbCntr0 : unsigned(22 downto 0);
    signal sw_db : std_logic_vector(0 downto 0);

begin
	
	-- Reset
	reset <= BTNC;
	
	-- you have to debounce switch 0
	process(SW(0), CLK100MHZ)
	begin
		if(rising_edge(CLK100MHZ)) then
			if(SW(0) = '1') then
				if(dbCntr0 < 500000) then
					dbCntr0 <= dbCntr0+1;
				else
					sw_db(0) <= '1';
				end if;
			else
				sw_db(0) <= '0';
				dbCntr0 <= (others => '0');
			end if;		
		end if;
	end process;
	
	battery_attached <= sw_db(0);
	highc_thresh <= SW(1);
	
	state_logic : process(curstate, battery_attached, highc_thresh, doneToFSM)
	begin
		next_state <= curstate;
		case curstate is 
			when idle => 
				if(battery_attached = '1') then
					next_state <= init;
				end if;
			when init => -- 2 minute initialization charge
				if(doneToFSM = '1') then
					next_state <= check;
				end if;
			when check =>
				if(highc_thresh = '0') then
					next_state <= fastchg;
				else
					next_state <= slowchg;
				end if;
			when fastchg => -- 10 minute fast charge
				if(doneToFSM = '1') then
					next_state <= check;
				end if;
			when slowchg => --  2 minute slow charge
				if(doneToFSM = '1') then
					next_state <= donechg;
				end if;
			when donechg =>
				if(battery_attached = '0') then 
					next_state <= idle;
				end if;
		end case;
	end process state_logic;

	process(CLK100MHZ, reset)
	begin
		if(reset = '1') then
			curstate <= idle;
		elsif(rising_edge(CLK100MHZ)) then
			curstate <= next_state;
		end if;
	end process;

	timer_0 : entity timer port map ( 
		clk => CLK100MHZ,
		reset => reset,
		startFromFSM => startFromFSM,
		MaxCounter => MaxCounter,
		doneToFSM => doneToFSM);

	MaxCounter <= x"11E1A300" when ( (curstate = init) or (curstate = slowchg) ) else
	              x"2FAF0800" when (curstate = fastchg) else
					  (others => '0');
	startFromFSM <= '0' when ( (curstate = donechg) or (curstate = idle) or (curstate = check) ) else '1';
	incCheckCntr <= '1' when (curstate = check) else '0';
	resetCheckCntr <= '1' when (curstate = idle) else '0';
	
	-- process to count number of time we checked battery level
	process(CLK100MHZ, reset)
	begin
		if(reset = '1') then
			checkCntr <= (others => '0');
		elsif(rising_Edge(CLK100MHZ)) then
			if(resetCheckCntr = '1') then
				checkCntr <= (others => '0');
			elsif(incCheckCntr = '1') then
				checkCntr <= checkCntr + 1;
			end if;
		end if;
	end process;
	LED <= std_logic_vector(checkCntr);
		
	process(curstate)
	begin
		case curstate is 
			when idle => 
				char3 <= x"49";
				char2 <= x"44";
				char1 <= x"4c";
				char0 <= x"45";
				
			when init =>
				char3 <= x"49";
				char2 <= x"4e";
				char1 <= x"49";
				char0 <= x"54";
			
			when check =>
				char3 <= x"43";
				char2 <= x"48";
				char1 <= x"45";
				char0 <= x"43";
			
			when slowchg => 
				char3 <= x"53";
				char2 <= x"4c";
				char1 <= x"4f";
				char0 <= x"57";
			
			when fastchg => 
				char3 <= x"46";
				char2 <= x"41";
				char1 <= x"53";
				char0 <= x"54";
			
			when donechg =>
				char3 <= x"44";
				char2 <= x"4f";
				char1 <= x"4e";
				char0 <= x"45";
				
			when others => 
				char3 <= (others => '0');
				char2 <= (others => '0');
				char1 <= (others => '0');
				char0 <= (others => '0');
		end case;
	end process;
	
   seg7_control: entity seg7_controller port map (clk100 => CLK100MHZ, char7=> X"30", char6=> X"30",
                                                 char5=> X"30", char4=> X"30", char3=> char3,
                                                 char2=> char2, char1=> char1, char0=> char0,
                                                 reset=> reset, cathode=> SEG7_CATH, anode=> AN);
	
end architecture;
