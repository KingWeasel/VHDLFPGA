library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_STD.ALL;

entity timer is
    Port ( 
			clk   		: in  std_logic;           			-- 100 MHz clock
			reset			: in  std_logic;				-- push button
			startFromFSM: in std_logic;
			MaxCounter	: in unsigned(31 downto 0);
			doneToFSM	: out std_logic
        );  
end timer;

architecture Behavioral of timer is  

signal cntr : unsigned(31 downto 0);

begin

process(reset,clk)
begin
	if(reset = '1') then
		cntr <= (others => '0');
	elsif(rising_edge(clk)) then
		if(startFromFSM = '1') then
			if(cntr < MaxCounter) then
				cntr <= cntr + 1;
			else
				cntr <= (others => '0');
			end if;
		else
			cntr <= (others => '0');
		end if;
	end if;		
end process;
doneToFSM <= '1' when cntr = MaxCounter else '0';

end Behavioral;