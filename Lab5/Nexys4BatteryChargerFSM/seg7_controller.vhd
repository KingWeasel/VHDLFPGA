----------------------------------------------------------------------------------
-- Engineer:    K. Newlander
--
-- Create Date: 02/14/2016
-- Description: seg7_controller is designed to display eigth characters on a 7-seg display
--              that uses the same cathodes for all displays.  The characters are sequenced
--              at 1kHz per character
--
--              **Updated to handle ASCII characters values for 0-9 and A-Z
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.ALL;

entity seg7_controller is
    Port ( clk100 : in STD_LOGIC;
           char7 : in STD_LOGIC_VECTOR (7 downto 0);
           char6 : in STD_LOGIC_VECTOR (7 downto 0);
           char5 : in STD_LOGIC_VECTOR (7 downto 0);
           char4 : in STD_LOGIC_VECTOR (7 downto 0);
           char3 : in STD_LOGIC_VECTOR (7 downto 0);
           char2 : in STD_LOGIC_VECTOR (7 downto 0);
           char1 : in STD_LOGIC_VECTOR (7 downto 0);
           char0 : in STD_LOGIC_VECTOR (7 downto 0);
           reset : in STD_LOGIC;
           cathode : out STD_LOGIC_VECTOR (7 downto 0);
           anode : out STD_LOGIC_VECTOR (7 downto 0));
end seg7_controller;

architecture Behavioral of seg7_controller is
     
    signal char_select : unsigned(2 downto 0); --count from 000 to 111 for 8 different characters
    signal pulse_1kHz : std_logic;
    constant pulse_1kHz_max : unsigned(26 downto 0) := to_unsigned(2000, 27); -- decimal 2000 (only needs to be 11 bits)
    
    signal output_digit : std_logic_vector(7 downto 0);
    
begin

    pulse_1k : entity pulseGenerator port map ( clk => clk100, reset => reset, maxCount => pulse_1kHz_max, pulseOut => pulse_1kHz);

    --Go through characters after each pulse
    --Calc current character
    process(clk100, reset)
    begin
        if(reset = '1') then
            char_select <= (others=>'0');
        elsif(rising_edge(clk100)) then
            if(pulse_1kHz = '1') then
                char_select <= char_select + 1;
            end if;
        end if;
    end process;
    
    --Determine Anode based on char_select
    process(clk100, reset)
    begin
        if(reset = '1') then
            anode <= (others=>'1');
            output_digit <= (others=>'0');
        elsif(rising_edge(clk100)) then
            case to_integer(char_select) is
                when 0 => anode <= (0=>'0', others=>'1'); output_digit <= char0;
                when 1 => anode <= (1=>'0', others=>'1'); output_digit <= char1;
                when 2 => anode <= (2=>'0', others=>'1'); output_digit <= char2;
                when 3 => anode <= (3=>'0', others=>'1'); output_digit <= char3;
                when 4 => anode <= (4=>'0', others=>'1'); output_digit <= char4;
                when 5 => anode <= (5=>'0', others=>'1'); output_digit <= char5;
                when 6 => anode <= (6=>'0', others=>'1'); output_digit <= char6;
                when 7 => anode <= (7=>'0', others=>'1'); output_digit <= char7;
                when others => anode <= (others=>'1'); output_digit <= (others=>'0');
            end case;
        end if;
    end process;
    
--    anode <= (0=>'0',others=>'1') when char_select = 0 else
--             (1=>'0',others=>'1') when char_select = 1 else
--             (2=>'0',others=>'1') when char_select = 2 else
--             (3=>'0',others=>'1') when char_select = 3 else
--             (4=>'0',others=>'1') when char_select = 4 else
--             (5=>'0',others=>'1') when char_select = 5 else
--             (6=>'0',others=>'1') when char_select = 6 else
--             (7=>'0',others=>'1') when char_select = 7 else
--             (others=>'1');
    
--    --Mux to determine digit to use
--    output_digit <= (char0) when char_select = 0 else
--                    (char1) when char_select = 1 else
--                    (char2) when char_select = 2 else
--                    (char3) when char_select = 3 else
--                    (char4) when char_select = 4 else
--                    (char5) when char_select = 5 else
--                    (char6) when char_select = 6 else
--                    (char7) when char_select = 7 else
--                    X"0";
                    
                    
    --encode current digit
    decoder : entity seg7_decode port map (digit => output_digit, cathode => cathode);
    


end Behavioral;
