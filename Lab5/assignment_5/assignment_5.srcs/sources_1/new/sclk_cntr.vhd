----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sclk_cntr is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           spi_state : in unsigned(2 downto 0);
           sclk_cntr_out : out unsigned (4 downto 0));
end sclk_cntr;

architecture Behavioral of sclk_cntr is
    signal sclk_cntr : unsigned (4 downto 0);
begin

    process(clk, reset)
    begin
        if(reset = '1') then
            sclk_cntr <= (others => '0');
        elsif(rising_edge(clk)) then
            if(spi_state = 4) then--incr_sclk_cntr
                sclk_cntr <= sclk_cntr + 1;
            elsif(spi_state = 6) then--set_cs_hi
                sclk_cntr <= (others => '0');
            end if;
        end if;
    end process;
    
    sclk_cntr_out <= sclk_cntr;

end Behavioral;
