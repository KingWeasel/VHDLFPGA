----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all; --for pulse gen

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_timer is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           timer_go : in STD_LOGIC;
           timer_max : in unsigned (16 downto 0);
           timer_done : out STD_LOGIC);
end fsm_timer;

architecture Behavioral of fsm_timer is
    signal timer_curr : unsigned (16 downto 0);
    signal timer_done_sig : std_logic;
begin
    
    process(clk, reset)--this process is basically the same as pulse_gen but with a stop/go line
    begin
        if(reset = '1') then
            timer_curr <= (others => '0');  
        elsif(rising_edge(clk)) then
            if(timer_go = '1') then
                if(timer_done_sig = '1') then
                    timer_curr <= (others => '0'); 
                else
                    timer_curr <= timer_curr + 1;
                end if;
            end if;
        end if;
    end process;
    
    timer_done_sig <= '1' when (timer_curr = timer_max) else '0';
    timer_done <= timer_done_sig;
    
end Behavioral;
