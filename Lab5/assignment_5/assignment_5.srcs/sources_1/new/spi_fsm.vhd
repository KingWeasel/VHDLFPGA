----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spi_fsm is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           spi_start : in STD_LOGIC;
           spi_done : out STD_LOGIC;
           chip_sel : out STD_LOGIC;
           sclk : out STD_LOGIC;
           sclk_cntr : in unsigned (4 downto 0);
           timer_done : in STD_LOGIC;
           timer_go : out STD_LOGIC;
           timer_max : out unsigned (16 downto 0);
           spi_state : out unsigned (2 downto 0));
end spi_fsm;

architecture Behavioral of spi_fsm is
    type spi_fsm_state is (idle, set_cs_lo, sclk_hi, sclk_lo, incr_sclk_cntr, chk_sclk_cntr, set_cs_hi, delay);
    signal spi_state_sig : spi_fsm_state;
    signal chip_sel_sig : std_logic;
    signal timer_go_sig : std_logic;
    signal sclk_sig : std_logic;
begin
    
    process(clk, reset)
    begin
        if(reset = '1') then
            spi_state_sig <= delay;
            sclk_sig <= '0';
            timer_go_sig <= '0';
            spi_done <= '0';
            chip_sel_sig <= '1';
        elsif(rising_edge(clk)) then
            case spi_state_sig is--spi_fsm
                when idle =>
                    spi_done <= '0';
                    if(spi_start = '1') then
                        spi_state_sig <= set_cs_lo;
                        chip_sel_sig <= '1';--disable, active low
                        timer_go_sig <= '0';
                    else
                        spi_state_sig <= idle;
                    end if;
                when set_cs_lo =>
                    if( (chip_sel_sig = '1') or (timer_go_sig = '0') ) then--we just got to this state            
                        chip_sel_sig <= '0';--active low
                        timer_go_sig <= '1';
                        timer_max <= to_unsigned(19,17);
                    else   
                        if(timer_done = '1') then
                            spi_state_sig <= sclk_hi;
                            timer_go_sig <= '0';--reset for next state
                            sclk_sig <= '0';
                        else
                            spi_state_sig <= set_cs_lo;
                        end if;
                    end if;
                when sclk_hi =>
                    if( (sclk_sig = '0') or (timer_go_sig = '0') ) then 
                        sclk_sig <= '1';--sclk_hi
                        timer_go_sig <= '1';
                        timer_max <= to_unsigned(49,17);
                    else
                        if(timer_done = '1') then
                            spi_state_sig <= sclk_lo;
                            timer_go_sig <= '0';--reset for next state
                        else
                            spi_state_sig <= sclk_hi;
                        end if;
                    end if;
                when sclk_lo =>
                    if( (sclk_sig = '1') or (timer_go_sig = '0') ) then
                        sclk_sig <= '0';--sclk_lo
                        timer_go_sig <= '1';
                        timer_max <= to_unsigned(47,17);--as recommended in hints for 50% duty cycle
                    else
                        if(timer_done = '1') then
                            spi_state_sig <= incr_sclk_cntr;
                            timer_go_sig <= '0';--reset for next state that needs it
                        else
                            spi_state_sig <= sclk_lo;
                        end if;
                    end if;
                when incr_sclk_cntr =>
                    spi_state_sig <= chk_sclk_cntr;
                when chk_sclk_cntr =>
                    if(sclk_cntr = 24) then
                        spi_state_sig <= set_cs_hi;
                    else
                        spi_state_sig <= sclk_hi;
                    end if;
                when set_cs_hi =>
                    chip_sel_sig <= '1';--done selecting chip
                    spi_state_sig <= delay;
                when delay =>
                    if(timer_go_sig = '0') then
                        timer_go_sig <= '1';
                        timer_max <= to_unsigned(1000000,17);--100ms apparently, comment out during simulation
--                        timer_max <= to_unsigned(10000, 17); --for shorter sim timer, comment out for real testing
                    else
                        if(timer_done = '1') then
                            spi_done <= '1'; --pulse high when delay done
                            spi_state_sig <= idle;
                            timer_go_sig <= '0';
                        else
                            spi_state_sig <= delay;
                        end if;
                    end if;
                when others =>
                    spi_state_sig <= delay;
            end case;
        end if;
    end process;
    
    chip_sel <= chip_sel_sig;
    timer_go <= timer_go_sig;
    sclk <= sclk_sig;
    
    spi_state <= -- until I learn how to use enums in entity declarations, this is how I do this
        to_unsigned(0,3) when spi_state_sig = idle else
        to_unsigned(1,3) when spi_state_sig = set_cs_lo else
        to_unsigned(2,3) when spi_state_sig = sclk_hi else
        to_unsigned(3,3) when spi_state_sig = sclk_lo else
        to_unsigned(4,3) when spi_state_sig = incr_sclk_cntr else
        to_unsigned(5,3) when spi_state_sig = chk_sclk_cntr else
        to_unsigned(6,3) when spi_state_sig = set_cs_hi else--this clears sclk_cntr in other entity
        to_unsigned(7,3) when spi_state_sig = delay else
        to_unsigned(0,3); --idle is a good default state
        
    
end Behavioral;
