----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--module to provide a debounced button output
--from a mechanical button
--debounced signal 
---------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity button_debounce is
    Port (  
            clk : in STD_LOGIC;
            reset: in STD_LOGIC;
            btn_in : in STD_LOGIC;
            btn_dbounced : out STD_LOGIC
         );
end button_debounce;

architecture Behavioral of button_debounce is
    signal cntr : unsigned(24 downto 0); --big enough to represent 25,000,000 if necessary
begin

    process(clk, reset)
    begin
        if(reset = '1') then
            btn_dbounced <= '0';
        elsif(rising_edge(clk)) then
            if(btn_in = '1') then
                if(cntr < 10000000) then
                    cntr <= cntr + 1;
                    btn_dbounced <= '0';
                else
                    btn_dbounced <= '1';
                end if; 
            else
                btn_dbounced <= '0';
                cntr <= (others => '0');
            end if;
        end if;
    end process;

end Behavioral;
