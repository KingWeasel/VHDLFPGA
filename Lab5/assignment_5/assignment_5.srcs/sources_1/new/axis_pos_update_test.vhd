----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/11/2018 02:44:23 PM
-- Design Name: 
-- Module Name: axis_pos_update_test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity axis_pos_update_test is
    Port ( CLK100MHZ : in STD_LOGIC;
           SW : in STD_LOGIC;
           BTNU : in STD_LOGIC;
           BTND : in STD_LOGIC;
           AN : out STD_LOGIC_VECTOR (7 downto 0);
           SEG7_CATH : out STD_LOGIC_VECTOR (7 downto 0)
         );
end axis_pos_update_test;

architecture Behavioral of axis_pos_update_test is
    signal dbounce_out_up : std_logic;
    signal dbounce_out_dn : std_logic;
    signal position_out : std_logic_vector (9 downto 0);
begin
    up_debounce : entity button_debounce port map
    (
        clk => CLK100MHZ,
        reset => SW,
        btn_in => BTNU,
        btn_dbounced => dbounce_out_up      
    ); 
    
    dn_debounce : entity button_debounce port map
    (
        clk => CLK100MHZ,
        reset => SW,
        btn_in => BTND,
        btn_dbounced => dbounce_out_dn      
    ); 
    
    seg7_out : entity seg7_hex port map
    (
        digit => position_out (3 downto 0),
        seg7 => SEG7_CATH
    );
    
    AN <= (0 =>'0', others => '1');--first digit always on, others off
    
    updown_count : entity axis_position_update port map
    (
        clk => CLK100MHZ,
        reset => SW,
        incr => dbounce_out_up,
        decr => dbounce_out_dn,
        max_position => to_unsigned(15,10),
        step => to_unsigned(1,10),
        position_out => position_out       
    );

end Behavioral;
