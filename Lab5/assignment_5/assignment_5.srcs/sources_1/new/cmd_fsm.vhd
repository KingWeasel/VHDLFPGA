----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
-- 
--SPI Commands:
--0xA2D02 - power up device by writing 02 to register 2D
--0x0B0000 - read the value at register 00 (device ID "AD")
--0x0B0100 - read the value at register 01 (device specific ID)
--0x0B0800 - read the value at register 08 (X DATA)
--0x0B0900 - read the value at register 09 (Y DATA)
--0x0B0A00 - read the value at register 0A (Z DATA)
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cmd_fsm is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           spi_done : in STD_LOGIC;
           spi_start : out STD_LOGIC;
           to_spi_bytes : out STD_LOGIC_VECTOR (23 downto 0));
end cmd_fsm;

architecture Behavioral of cmd_fsm is
    type cmd_fsm_state is (write_out, wait_spi);
    signal cmd_state : cmd_fsm_state;
    signal spi_bytes_sig : std_logic_vector (23 downto 0);
begin
    
    process(clk, reset)
    begin
        if(reset = '1') then
            cmd_state <= write_out;
            spi_bytes_sig <= x"000000";--send powerup
            spi_start <= '0';
        elsif(rising_edge(clk)) then
            case cmd_state is
                when write_out =>--write next 3 byte sequence to output bus
                    case spi_bytes_sig is --pick bytes to write for next time
                        when x"000000" =>
                            spi_bytes_sig <= x"000001";
                        when x"000001" =>
                            spi_bytes_sig <= x"0a2d02";
                        when x"0a2d02" =>
                            spi_bytes_sig <= x"0b0000";
                        when x"0b0000" =>
                            spi_bytes_sig <= x"0b0100";
                        when x"0b0100" =>
                            spi_bytes_sig <= x"0b0800";
                        when x"0b0800" =>
                            spi_bytes_sig <= x"0b0900";
                        when x"0b0900" =>
                            spi_bytes_sig <= x"0b0a00";
                        when x"0b0a00" =>
                            spi_bytes_sig <= x"0b0000";--go back to read ad_id 
                        when others =>
                            spi_bytes_sig <= x"000000";--go back to send powerup on undefined byte code
                    end case;
                    spi_start <= '1';--pulse hi when leaving this state
                    cmd_state <= wait_spi;
                when wait_spi =>
                    spi_start <= '0';
                    if(spi_done = '1') then --if spi_fsm is done
                        cmd_state <= write_out;   
                    else
                        cmd_state <= wait_spi;
                    end if;
            end case;
        end if;
    end process; 
    
    to_spi_bytes <= spi_bytes_sig;  
    
end Behavioral;
