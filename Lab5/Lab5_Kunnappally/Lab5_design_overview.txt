----------------
----------------
Design Overview
----------------
----------------

-Used numeric standard library heavily for counters, type converstion,
	and comparators (allowed avoiding calculating binary representation
	of integers in many cases)
-Debounce circuit implemented almost exactly as detailed in lectures and
	utilized 4 times (once per input button)
-Also designed a press detector: output <= curr_btn_state and not
	prev_button state
-Utilized press detector in entity to hold current red square
	coordinates
-seg7_controller utilized like previous lab, but had to mask anode
	output so that unused leading 4 characters would not display
-Screen drawing skeleton laid out as described in Lab4Hints
-Used 2 5-bit counters to decide when to flip a color bit and determine
	whether to draw blue or green at the current overall pixel position
-5-bit counters pre-empted by red square position coordinates 
	(calculated actual bounds using multiply and add 32, and drew red if
	horizontal and vertical pixel counter were in those bounds)
-Hsync and Vsync logic unchanged from Lab4Hints
-2 primary Moore FSMs took turns figuring out what command to send andk
	shifting 24 bits of data into/out of accelerometer with generated
	sclock
-1 minor Moore FSM to determine which register to write shifted in data
	to


-----------------------------
-----------------------------
FPGA Resource Usage Estimates
-----------------------------
-----------------------------

Clocks:
	-1 100MHz Clock
	
	Estimated Total: 1 clock
	Real Total: 1 clock

IO:
	1.Input
	-4 buttons input
	-4 switch inputs
	2.Output
	-4 lines each for VGA_R, VGA_G, and VGA_B
	-1 line each for VGA_HS and VGA_VS
	-8 bits for seg7 cathode
	-8 bits for seg7 anodes
	-8 bits for LED refresh counter
	
	Estimated Total: 37 bits of IO
	Real Total: 54 bits of IO
	
Registers:
	1.Button Debouncing
	-25 bit bounce settling counter x4 => 100 registers for debouncing
	2.Red Square position Tracker
	-1 bit each for increment and decrement press detectors (to keep
		previous button state) x2 position trackers => 4 registers for
		button press detection
	-10 bits to hold position along 1 axis x2 axes => 20 registers for
		position tracking
	-10 bits to hold start bound and 10 bits for end bound for one axis
		of red square x2 axes => 40 registers for red square bounds
	3.Seg7_Controller
	-27 bits for counter in 1kHz pulse generator in seg7_controller
	-3 bits to hold current display anode in seg7_controller
	4.Display
	-27 bits for counter in 25MHz pulse generator for pixel clock
	-10 bits for pixel counter for each axis x2 axes => 20 registers for
		overall pixel counters
	-5 bits for count-to-32 counter for each axis x2 axes => 10
		registers for 5-bit pixel counters
	-1 bit for color choosing
	-8 bits for screen refresh counter
	5.Accelerometer SPI Controller
	-1 bit to hold cmd_fsm state (2 states)
	-24 bits to hold next accelerometer instruction to write in cmd_fsm
	-3 bits to hold spi_fsm state (8 states)
	-17 bits to keep tick count in controllable timer
	-5 bits to keep sclock count
	-24 bits to keep bits shifted out to MOSI
	-24 bits to keep bits shifted in from MISO
	
	Estimated Total: 358 registers
	Real Total: 347 registers
	
	
	

	
