----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--paints blue and green checkerboard pattern on screen
--squares are 32x32 pixels
--direction buttons (and accelerometer) control position of single red
--square on board. Red square position rolls over
--when it goes past the edge of the screen
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab5_top is
    Port (  CLK100MHZ : in STD_LOGIC;--Clock
            SW : in std_logic_vector(4 downto 0);
            
            BTNU, BTND, BTNL, BTNR : in std_logic;
            
            ACL_MISO : in std_logic;
            ACL_MOSI : out std_logic;
            ACL_SCLK : out std_logic;
            ACL_CSN : out std_logic;
            ACL_INT : in std_logic_vector (2 downto 1);
            
            VGA_R, VGA_G, VGA_B : out std_logic_vector (3 downto 0);
            VGA_HS, VGA_VS : out std_logic;
            
            SEG7_CATH : out std_logic_vector (7 downto 0);
            AN : out std_logic_vector (7 downto 0);
            
            LED : out std_logic_vector (10 downto 0)
          );
end lab5_top;

architecture Behavioral of lab5_top is
    
    --signals to feed between debounced buttons
    --and position tracking registers
    signal up_dbounced, dn_dbounced, lf_dbounced, rt_dbounced : std_logic;
    
    --signals to feed between accelerometer outputs
    --and position tracking registers
    signal up_accel, dn_accel, lf_accel, rt_accel : std_logic;
    
    --input of position trackers
    signal up_in, dn_in, lf_in, rt_in : std_logic;
    
    --output of position trackers
    signal rdsquare_horz_pos, rdsquare_vert_pos : unsigned (9 downto 0);
    
    --output of spi controller (accel data)
    signal x_accel, y_accel, z_accel : signed (7 downto 0);
    signal idad, idid : std_logic_vector (7 downto 0);
    signal chip_sel : std_logic;--accel_spi debug
    signal sclk : std_logic;
    
    --IO for seg7 controller
    signal seg7_ctl_in : std_logic_vector (31 downto 0);
    signal seg7_ctl_an_out : std_logic_vector (7 downto 0);
    
    --display
    signal pulse_25MHz : std_logic;
    signal horz_cntr, vert_cntr : unsigned (9 downto 0);
    signal horz_32_cntr, vert_32_cntr : unsigned (4 downto 0);
    signal is_green : std_logic;--select blue or green
    signal red_horz_start, red_horz_end, red_vert_start, red_vert_end : unsigned (9 downto 0);
    
    --LED refresh counter
    signal refresh_cntr : unsigned(7 downto 0);
       
begin
    
    --button debouncing entities
    up_dbounce : entity button_debounce port map
    (   clk => CLK100MHz,
        reset => SW(0),
        btn_in => up_in,
        btn_dbounced => up_dbounced
    );
    
    up_in <= up_accel when (SW(1) = '1') else BTNU;
    
    dn_dbounce : entity button_debounce port map
    (   clk => CLK100MHz,
        reset => SW(0),
        btn_in => dn_in,
        btn_dbounced => dn_dbounced
    );
    
    dn_in <= dn_accel when (SW(1) = '1') else BTND;
        
    lf_dbounce : entity button_debounce port map
    (   clk => CLK100MHz,
        reset => SW(0),
        btn_in => lf_in,
        btn_dbounced => lf_dbounced
    );
    
    lf_in <= lf_accel when (SW(1) = '1') else BTNL;
        
    rt_dbounce : entity button_debounce port map
    (   clk => CLK100MHz,
        reset => SW(0),
        btn_in => rt_in,
        btn_dbounced => rt_dbounced
    );
    
    rt_in <= rt_accel when (SW(1) = '1') else BTNR;
    
    --end button debouncing entities
    
    --spi controller
    accelerometer : entity accel_spi_rw port map
    (   clk => CLK100MHz,
        reset => SW(0),
        DATA_X => x_accel,
        DATA_Y => y_accel,
        DATA_Z => z_accel,
        ID_AD => idad,
        ID_1D => idid,
        CSb => chip_sel,
        SCLK => sclk,
        MOSI => ACL_MOSI,
        MISO => ACL_MISO    
    );
    
    rt_accel <= '1' when (y_accel < -30) else '0';
    lf_accel <= '1' when (y_accel > 30) else '0';
    up_accel <= '1' when (x_accel < -30) else '0';
    dn_accel <= '1' when (x_accel > 30) else '0';
    
    ACL_CSN <= chip_sel;
    ACL_SCLK <= sclk;
--    LED(10 downto 8) <= chip_sel & sclk & '0';--debug
    
    --axis position trackers
    horz_pos_update : entity axis_position_update port map
    (   clk => CLK100MHz,
        reset => SW(0),
        incr => rt_dbounced,
        decr => lf_dbounced,
        max_position => to_unsigned(19,10),
        step => to_unsigned(1,10),
        position_out => rdsquare_horz_pos
    );
    
    vert_pos_update : entity axis_position_update port map
    (   clk => CLK100MHz,
        reset => SW(0),
        incr => dn_dbounced,
        decr => up_dbounced,
        max_position => to_unsigned(14,10),
        step => to_unsigned(1,10),
        position_out => rdsquare_vert_pos
    );
    
    update_rdsquare_horz_bounds : process(rdsquare_horz_pos)
    begin
        red_horz_start <= resize(rdsquare_horz_pos * 32,10);
        red_horz_end <= resize(rdsquare_horz_pos * 32 + 32,10);
    end process update_rdsquare_horz_bounds;
    
    update_rdsquare_vert_bounds : process(rdsquare_vert_pos)
    begin
        red_vert_start <= resize(rdsquare_vert_pos * 32,10);
        red_vert_end <= resize(rdsquare_vert_pos * 32 + 32,10);
    end process update_rdsquare_vert_bounds;
    -- end position trackers
    
    --seven segment display entities
    seg7_cntlr : entity seg7_controller port map
    (   clk => CLK100MHz,
        reset => SW(0),
        disp_chars => seg7_ctl_in,
        anode_ctl => seg7_ctl_an_out,
        seg7_out => SEG7_CATH
    );

    --show all 8 bits
    AN <= seg7_ctl_an_out or x"00";
    
    seg7_ctl_in (31 downto 16) <=
        x"00" & std_logic_vector(x_accel) when SW(4 downto 3) = "01" else
        x"00" & std_logic_vector(y_accel) when SW(4 downto 3) = "10" else
        x"00" & std_logic_vector(z_accel) when SW(4 downto 3) = "11" else
        idid & idad when SW(4 downto 3) = "00";   
    
    seg7_ctl_in (15 downto 8) <= std_logic_vector(rdsquare_vert_pos (7 downto 0));
    seg7_ctl_in (7 downto 0) <= std_logic_vector(rdsquare_horz_pos (7 downto 0));
    
    --end seven segment display

    pulse_gen_25MHz : entity pulse_gen port map
    (   clk => CLK100MHz,
        reset => SW(0),
        count_to => to_unsigned(3,27), -- 3 or about 25us (25MHz)
        pulse_10ns => pulse_25MHz
    );
    
    --display drawing logic
    
    LED (7 downto 0) <= std_logic_vector(refresh_cntr);
    
    count_whole_screen : process(CLK100MHz, SW)
    begin
        if(SW(0) = '1') then
            horz_cntr <= (others => '0');
            vert_cntr <= (others => '0');
            is_green <= '0'; --this makes the top-left square green
            refresh_cntr <= (others => '0');
        elsif(rising_edge(CLK100MHz)) then
            if(pulse_25MHz = '1') then
            
                if(horz_cntr = to_unsigned(799,10)) then--if whole line drawn
                    horz_cntr <= (others => '0');
                    horz_32_cntr <= (others => '0');
                    
                    --vert increments when horizontal resets
                    if(vert_cntr = to_unsigned(520,10)) then --if whole column drawn
                        vert_cntr <= (others => '0');
                        vert_32_cntr <= (others => '0');
                        is_green <= '1';--this makes the top-left square green
                        refresh_cntr <= refresh_cntr + 1;
                    else
                        vert_cntr <= vert_cntr + 1;
                        
                        if(vert_32_cntr = 31) then
                            vert_32_cntr <= (others => '0');
                            is_green <= not is_green;--if both 32s rollover, switch color
                        else
                            vert_32_cntr <= vert_32_cntr + 1;
                        end if;
                    end if;
                else
                    horz_cntr <= horz_cntr + 1;
                    
                    if(horz_32_cntr = 31) then
                        horz_32_cntr <= (others => '0');
                        is_green <= not is_green; --if just horz 32 rolls over, switch color
                    else
                        horz_32_cntr <= horz_32_cntr + 1;
                    end if;
                end if;  
                  
            end if;
        end if;
    end process count_whole_screen;
    
    set_out : process(horz_cntr, vert_cntr) --every change in pixel
    begin
        --set Hsync and Vsync
        if((horz_cntr >= 656) and (horz_cntr < 752)) then
            VGA_HS <= '0';
        else
            VGA_HS <= '1';
        end if;
        
        if((vert_cntr >= 490) and (vert_cntr < 492)) then
            VGA_VS <= '0';
        else
            VGA_VS <= '1';
        end if;
        
        --set colors
        if((horz_cntr < 640) and (vert_cntr < 480)) then
            if( (horz_cntr >= red_horz_start) and (horz_cntr < red_horz_end) --if counter coordinates
                and (vert_cntr >= red_vert_start) and (vert_cntr < red_vert_end) --are in red square bounds
              ) then
                VGA_R <= (others => '1');
                VGA_G <= (others => '0');
                VGA_B <= (others => '0');
            else
                case is_green is
                    when '1' =>
                        VGA_R <= (others => '0');
                        VGA_G <= (others => '1');
                        VGA_B <= (others => '0');
                    when others =>
                        VGA_R <= (others => '0');
                        VGA_G <= (others => '0');
                        VGA_B <= (others => '1');         
                end case;
            end if;
        else
            VGA_R <= (others => '0');
            VGA_G <= (others => '0');
            VGA_B <= (others => '0');
        end if;
        
    end process set_out;

end Behavioral;
