----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--module to generate pulse of width 10ns
--everytime counter with max value count_to
--rolls over
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity anode_scroller is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           scroll_enable : in STD_LOGIC;-- 1kHz clock for Lab3
           anode_enc_val : out unsigned (2 downto 0)
         );
end anode_scroller;

architecture Behavioral of anode_scroller is
    --can encode 8 values, which is how many anodes there are
    signal enc_feedback : unsigned (2 downto 0);
begin
    incr_scroll_val : process(clk, reset)
    begin
        if(reset = '1') then
            enc_feedback <= (others => '0');
        elsif(rising_edge(clk)) then
            if(scroll_enable = '1') then
                enc_feedback <= enc_feedback + 1;  
            end if;
        end if;
    end process incr_scroll_val;
    anode_enc_val <= enc_feedback;
    
end Behavioral;
