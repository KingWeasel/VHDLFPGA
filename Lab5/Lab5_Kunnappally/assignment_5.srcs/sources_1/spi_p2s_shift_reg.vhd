----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spi_p2s_shift_reg is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           MOSI : out STD_LOGIC;
           to_spi_bytes : in STD_LOGIC_VECTOR (23 downto 0);
           spi_state : in unsigned (2 downto 0);
           sclk_cntr : in unsigned (4 downto 0);
           timer_done : in STD_LOGIC;--include these two so no numeric comparators are needed
           spi_start : in std_logic);--this wasn't in hints diagram, but was in the hints description (as a hint)
end spi_p2s_shift_reg;

architecture Behavioral of spi_p2s_shift_reg is
    signal spi_bytes_shift : std_logic_vector (23 downto 0);
begin
    
    process(clk, reset)
    begin
        if(reset = '1') then
            spi_bytes_shift <= (others => '0');
        elsif(rising_edge(clk)) then
            if (spi_start = '1') then
                spi_bytes_shift <= to_spi_bytes; --only do once when we've gotten to this state
            elsif( (spi_state = 5) and (sclk_cntr < 24) ) then --spi_state 2 = sclk_hi
                spi_bytes_shift <= spi_bytes_shift(22 downto 0) & '0'; --do the shift as soon as state 2 is reached
            end if; 
        end if;
    end process;
    
    MOSI <= spi_bytes_shift(23);

end Behavioral;
