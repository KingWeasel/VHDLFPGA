----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--SPI controller module
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity accel_spi_rw is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           
           --captured data
           DATA_X, DATA_Y, DATA_Z : out signed (7 downto 0);
           ID_AD, ID_1D : out STD_LOGIC_VECTOR (7 downto 0);
           
           --accel ports
           CSb : out STD_LOGIC;
           SCLK : out STD_LOGIC;
           MOSI : out STD_LOGIC;
           MISO : in STD_LOGIC);
end accel_spi_rw;

architecture Behavioral of accel_spi_rw is
    signal spi_start : std_logic;
    signal spi_done : std_logic;
    signal to_spi_bytes : std_logic_vector (23 downto 0); 
    signal timer_done : std_logic;
    signal timer_go : std_logic;
    signal timer_max : unsigned (16 downto 0);
    signal spi_state : unsigned (2 downto 0);
    signal sclk_cnt : unsigned (4 downto 0);
begin

    cmd_fsm_inst : entity cmd_fsm port map
    (   clk => clk,
        reset => reset,
        spi_done => spi_done,
        spi_start => spi_start,
        to_spi_bytes => to_spi_bytes
    );
    
    spi_fsm_inst : entity spi_fsm port map
    (   clk => clk,
        reset => reset,
        spi_start => spi_start,
        spi_done => spi_done,
        chip_sel => CSb,
        sclk => SCLK,
        sclk_cntr => sclk_cnt,
        timer_done => timer_done,
        timer_go => timer_go,
        timer_max => timer_max,
        spi_state => spi_state           
    );
    
    shift_spi_p2s : entity spi_p2s_shift_reg port map
    (   clk => clk,
        reset => reset,
        MOSI => MOSI,
        to_spi_bytes => to_spi_bytes,
        sclk_cntr => sclk_cnt,
        spi_state => spi_state,
        timer_done => timer_done,
        spi_start => spi_start--wasn't in hints diagram, but was in the description
    );
    
    shift_spi_s2p : entity spi_s2p_shift_reg port map
    (   clk => clk,
        reset => reset,
        spi_state => spi_state,
        sclk_cntr => sclk_cnt,
        MISO => MISO,
        ad => ID_AD,
        id => ID_1D,
        x => DATA_X,
        y => DATA_Y,
        z => DATA_Z
    );
    
    timer : entity fsm_timer port map
    (   clk => clk,
        reset => reset,
        timer_go => timer_go,
        timer_max => timer_max,
        timer_done => timer_done
    );
    
    cntr : entity sclk_cntr port map
    (   clk => clk,
        reset => reset,
        spi_state => spi_state,
        sclk_cntr_out => sclk_cnt
    );

end Behavioral;
