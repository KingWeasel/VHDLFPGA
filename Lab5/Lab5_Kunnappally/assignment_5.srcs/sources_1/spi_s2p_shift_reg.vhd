----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spi_s2p_shift_reg is
    Port (  clk : in STD_LOGIC;
            reset : in STD_LOGIC;
            spi_state : in unsigned (2 downto 0);
            sclk_cntr : in unsigned (4 downto 0);
            MISO : in STD_LOGIC;
            ad : out std_logic_vector (7 downto 0);
            id : out std_logic_vector (7 downto 0);
            x : out signed (7 downto 0);--change this to unsigned or signed when you find out which
            y : out signed (7 downto 0);
            z : out signed (7 downto 0)
          );
end spi_s2p_shift_reg;

architecture Behavioral of spi_s2p_shift_reg is
    signal MISO_shift : std_logic_vector(23 downto 0);
    type MISO_data_state is (powerup_state, ad_state, id_state, x_state, y_state, z_state);
    signal MISO_data : MISO_data_state;
    signal ad_reg, id_reg, x_reg, y_reg, z_reg : std_logic_vector (7 downto 0);
begin
    
    process(clk, reset)
    begin
        if(reset = '1') then
            MISO_data <= powerup_state;
            ad_reg <= (others => '0');
            id_reg <= (others => '0');
            x_reg <= (others => '0');
            y_reg <= (others => '0');
            z_reg <= (others => '0');
        elsif(rising_edge(clk)) then 
            if( (spi_state = 5) and (sclk_cntr < 24) ) then -- spi_state 5 = chk_sclk_cntr
                MISO_shift <= MISO_shift(22 downto 0) & MISO;--shift 1-bit              
            elsif( (spi_state = 6) and (sclk_cntr = 24) ) then --spi_state 6 = set chip_select_hi
                case MISO_data is--fsm to select which register to update     
                    when powerup_state =>
                        MISO_data <= ad_state;--no writing occurs
                    when ad_state =>
                        ad_reg <= MISO_shift(7 downto 0);--only 8 LSBs matter
                        MISO_data <= id_state;
                    when id_state =>
                        id_reg <= MISO_shift(7 downto 0);
                        MISO_data <= x_state;
                    when x_state =>
                        x_reg <= MISO_shift(7 downto 0);
                        MISO_data <= y_state;
                    when y_state =>
                        y_reg <= MISO_shift(7 downto 0);
                        MISO_data <= z_state;
                    when z_state =>
                        z_reg <= MISO_shift(7 downto 0);
                        MISO_data <= ad_state;
                    when others =>
                        ad_reg <= (others => '0');
                        id_reg <= (others => '0');
                        x_reg <= (others => '0');
                        y_reg <= (others => '0');
                        z_reg <= (others => '0');
                        MISO_data <= powerup_state;
                end case;              
            end if;
        end if;
    end process;
    
    ad <= ad_reg;
    id <= id_reg;
    x <= signed(x_reg);
    y <= signed(y_reg);   
    z <= signed(z_reg); 
    
end Behavioral;
