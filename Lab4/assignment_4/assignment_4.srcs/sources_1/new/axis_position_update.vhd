----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--module to keep track of position on a number line
--where the line goes from 0 to some maximum position
--in increments of a specified step size
--position rolls over in either direction
--(i.e. position goes back to 0 if increment exceeds
--max position and goes to maximum position if decrement
--makes position less than 0)
---------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity axis_position_update is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           incr : in STD_LOGIC;
           decr : in STD_LOGIC;
           max_position : in unsigned (9 downto 0);--allows pixel-level control if desired
           step : in unsigned (9 downto 0);--initially thought I could do the pixel scaling here, now just for generality
           position_out : out unsigned (9 downto 0)
         );
end axis_position_update;
    
architecture Behavioral of axis_position_update is
    signal curr_position : unsigned (9 downto 0);
    signal incr_pressed : std_logic;
    signal decr_pressed : std_logic;
begin
    
    incr_press_detect : entity press_detector port map
    (
        clk => clk,
        reset => reset,
        btn_in => incr,
        press_detected => incr_pressed
    );
    
    decr_press_detect : entity press_detector port map
    (
        clk => clk,
        reset => reset,
        btn_in => decr,
        press_detected => decr_pressed
    );
    
    process(clk, reset)
    begin
        if(reset = '1') then
            curr_position <= (others => '0');
        elsif(rising_edge(clk)) then
            if( (incr_pressed = '1') and (decr_pressed = '0') ) then --Seemed less messy to not nest
                if(curr_position = max_position) then
                    curr_position <= (others => '0');
                else
                    curr_position <= curr_position + step;
                end if;
            elsif( (incr_pressed = '0') and (decr_pressed = '1') ) then --Seemed less messy to not nest
                if(curr_position = "0") then
                    curr_position <= max_position;
                else
                    curr_position <= curr_position - step;
                end if;
            else
                curr_position <= curr_position;
            end if;
        end if;
    end process;
    
    position_out <= curr_position;
    
end Behavioral;
