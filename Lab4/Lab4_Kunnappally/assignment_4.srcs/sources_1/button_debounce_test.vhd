----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--top-level to test button debouncer on hardware
--
--------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity button_debounce_test is
    Port (  CLK100MHZ : in STD_LOGIC;
            SW : in STD_LOGIC;
            BTNU : in STD_LOGIC;
            AN : out std_logic_vector (7 downto 0);
            SEG7_CATH : out STD_LOGIC_VECTOR (7 downto 0)
         );
end button_debounce_test;

architecture Behavioral of button_debounce_test is
    signal cntr : unsigned (3 downto 0);
    signal dbounce_out : std_logic;
    signal incr_cntr : std_logic;
begin
    
    button_debouncer : entity button_debounce port map
    (
        clk => CLK100MHz,
        reset => SW,
        btn_in => BTNU,
        btn_dbounced => dbounce_out       
    );
    
    detect_press : entity press_detector port map
    (
        clk => CLK100MHz,
        reset => SW,
        btn_in => dbounce_out,
        press_detected => incr_cntr
    );
    
    seg7_out : entity seg7_hex port map
    (
        digit => std_logic_vector(cntr),
        seg7 => SEG7_CATH
    );
    
    AN <= (0 =>'0', others => '1');--first digit always on, others off
    
    process(CLK100MHz, SW)
    begin
        if(SW = '1') then
            cntr <= (others => '0');
        elsif(rising_edge(CLK100MHz)) then
            if(incr_cntr = '1') then
                cntr <= cntr + 1;
            else
                cntr <= cntr;
            end if;
        end if;
    end process;

end Behavioral;
