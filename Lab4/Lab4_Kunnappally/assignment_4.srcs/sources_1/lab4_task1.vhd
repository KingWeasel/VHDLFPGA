----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--test VGA controller for painting screen red
---------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab4_top is
    Port (  CLK100MHZ : in STD_LOGIC;--Clock
            BTNC : in STD_LOGIC;--async reset, center button
            VGA_R : out std_logic_vector (3 downto 0);
            VGA_G : out std_logic_vector (3 downto 0);
            VGA_B : out std_logic_vector (3 downto 0);
            VGA_HS : out std_logic;
            VGA_VS : out std_logic;
            SW : in std_logic_vector (2 downto 0)
          );
end lab4_top;

architecture Behavioral of lab4_top is
    signal pulse_25MHz : std_logic;
    signal hzntl_cntr : unsigned(9 downto 0);
    signal vert_cntr : unsigned(9 downto 0);
    signal plain_vec : std_logic_vector(9 downto 0);
begin

    VGA_HS <= '1' when (vert_cntr < plain_vec) else '0';

    pulse_gen_25MHz : entity pulse_gen port map
    (
        clk => CLK100MHZ,
        reset => BTNC,
        count_to => "000000000000000000000000011", -- 3 or about 25us (25MHz)
        pulse_10ns => pulse_25MHz
    );
    
    count : process(CLK100MHZ, BTNC)
    begin
        if(BTNC = '1') then
            hzntl_cntr <= (others => '0');
            vert_cntr <= (others => '0'); 
        elsif(rising_edge(CLK100MHZ)) then
            if(pulse_25MHz = '1') then
                if(hzntl_cntr = "1100011111") then--799 binary
                    hzntl_cntr <= (others => '0');
                    --vert increments when horzontal resets
                    if(vert_cntr = "1000001000") then --520 binary
                        vert_cntr <= (others => '0');
                    else
                        vert_cntr <= vert_cntr + 1;
                    end if;
                else
                    hzntl_cntr <= hzntl_cntr + 1;
                end if;    
            end if;
        end if;
    end process count;
    
    set_out : process(hzntl_cntr, vert_cntr, SW)
    begin
        if(hzntl_cntr >= 656) then
            if(hzntl_cntr < 752) then
                VGA_HS <= '0';
            else
                VGA_HS <= '1';
            end if;
        else
            VGA_HS <= '1';
        end if;
        
        if(vert_cntr >= 490) then
            if(vert_cntr < 492) then
                VGA_VS <= '0';
            else
                VGA_VS <= '1';
        end if;
        else
            VGA_VS <= '1';
        end if;
        
        if(hzntl_cntr < 640) then
            if(vert_cntr < 480) then
            
                if( SW(2) = '1' ) then--set red
                    VGA_R <= (others => '1');
                else
                    VGA_R <= (others => '0');
                end if;
                
                if( SW(1) = '1' ) then --set green
                    VGA_G <= (others => '1');
                else
                    VGA_G <= (others => '0');
                end if;
                
                if( SW(0) = '1' ) then --set blue
                    VGA_B <= (others => '1');
                else
                    VGA_B <= (others => '0');
                end if;
                
            else
                VGA_R <= (others => '0');
                VGA_G <= (others => '0');
                VGA_B <= (others => '0');
            end if;
        else
            VGA_R <= (others => '0');
            VGA_G <= (others => '0');
            VGA_B <= (others => '0');
        end if;
    end process set_out;
    
end Behavioral;
