----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--Top-level entity/arch for assignment 1
--Lights seven-segment displays and LEDs as
--described in assignment
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity lab1_top is
    Port ( BTNC : in STD_LOGIC;
           BTND : in STD_LOGIC;
           BTNU : in STD_LOGIC;
           SW : in STD_LOGIC_VECTOR (15 downto 0);
           LED : out STD_LOGIC_VECTOR (15 downto 0);
           SEG7_CATH : out STD_LOGIC_VECTOR (7 downto 0);
           AN : out STD_LOGIC_VECTOR (7 downto 0));
end lab1_top;

architecture Behavioral of lab1_top is
    signal encoder_input : std_logic_vector(3 downto 0);
begin
    
    --encoder gets 0 when center button pressed
    with BTNC select
        encoder_input <=
            "0000" when '1',
            SW(3 downto 0) when others;
    
    encoder : entity work.seg7_hex port map
          (
            digit => encoder_input,
            seg7 => SEG7_CATH(7 downto 0)
          );
     
     --LEDs are directly connected to switches     
     LED <= SW;
     
     --This is very brute force, but I don't know VHDL very well yet
     --the not around everything is because displays are active low
     AN(0) <= not ( ( ( SW(4) or BTND ) and not BTNU ) or BTNC );
     AN(1) <= not ( ( ( SW(5) or BTND ) and not BTNU ) or BTNC );
     AN(2) <= not ( ( ( SW(6) or BTND ) and not BTNU ) or BTNC );
     AN(3) <= not ( ( ( SW(7) or BTND ) and not BTNU ) or BTNC );
     
     AN(4) <= not ( ( ( SW(8) or BTNU ) and not BTND ) or BTNC );
     AN(5) <= not ( ( ( SW(9) or BTNU ) and not BTND ) or BTNC );
     AN(6) <= not ( ( ( SW(10) or BTNU ) and not BTND ) or BTNC );
     AN(7) <= not ( ( ( SW(11) or BTNU ) and not BTND ) or BTNC );
  
     
end Behavioral;
