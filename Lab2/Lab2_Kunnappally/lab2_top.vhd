---------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--Top-level entity/arch for assignment 2
--Implement the same logic as assignment 1
--but use processes for all case selection
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

entity lab2_top is
    Port ( BTNC : in STD_LOGIC;
           BTND : in STD_LOGIC;
           BTNU : in STD_LOGIC;
           SW : in STD_LOGIC_VECTOR (15 downto 0);
           LED : out STD_LOGIC_VECTOR (15 downto 0);
           SEG7_CATH : out STD_LOGIC_VECTOR (7 downto 0);
           AN : out STD_LOGIC_VECTOR (7 downto 0));
end lab2_top;

architecture Behavioral of lab2_top is
    signal encoder_input : std_logic_vector(3 downto 0); 
begin
    
    --instantiate encoder
    encoder : entity seg7_hex_process port map
        (
            digit => encoder_input,
            seg7 => SEG7_CATH(7 downto 0)
        );
     
     --process to figure out what encoder_input gets   
     set_cath : process (SW)
     begin
        case BTNC is
            when '1' =>
                encoder_input <= "0000";
            when others =>
                encoder_input <= SW(3 downto 0);
        end case;
     end process set_cath;
     
     --LEDs are directly connected to switches     
     LED <= SW;
     
     --Paraphrased from Module 2E slide 4:
     set_disp : process (BTNC, BTNU, BTND, SW)
     begin
        if(BTNC = '1') then
            AN <= "00000000";
        elsif(BTNU = '1') then
            AN <= "00001111";
        elsif (BTND = '1') then
            AN <= "11110000";
        else
            AN <= not SW(11 downto 4);
        end if;
     end process set_disp;
  
end Behavioral;