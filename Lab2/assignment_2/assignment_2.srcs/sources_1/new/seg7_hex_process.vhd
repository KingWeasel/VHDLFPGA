----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--small module to represent the setting of seven-segment cathode for LED input
--this time using a process to describe logic
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity seg7_hex_process is
    Port ( digit : in STD_LOGIC_VECTOR (3 downto 0);
           seg7 : out STD_LOGIC_VECTOR (7 downto 0));
end seg7_hex_process;

architecture Behavioral of seg7_hex_process is
--    signal display_digit : std_logic_vector(3 downto 0);
begin
--    display_digit <= digit(3 downto 0);
    set_out : process (digit)
    begin
        case digit is
            when x"0" =>
                seg7 <= "11000000";
            when x"1" =>
                seg7 <= "11111001";
            when x"2" =>
                seg7 <= "10100100";
            when x"3" =>
                seg7 <= "10110000";
            when x"4" =>
                seg7 <= "10011001";
            when x"5" =>
                seg7 <= "10010010";
            when x"6" =>
                seg7 <= "10000010";
            when x"7" =>
                seg7 <= "11111000";
            when x"8" =>
                seg7 <= "10000000";
            when x"9" =>
                seg7 <= "10010000";
            when x"A" =>
                seg7 <= "10001000";
            when x"B" =>
                seg7 <= "10000011";
            when x"C" =>
                seg7 <= "11000110";
            when x"D" =>
                seg7 <= "10100001";
            when x"E" =>
                seg7 <= "10000110";
            when others =>
                seg7 <= "10001110";
        end case;
    end process set_out;
        
end Behavioral;