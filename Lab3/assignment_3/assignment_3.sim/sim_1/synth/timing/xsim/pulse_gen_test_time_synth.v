// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Mon Feb 19 10:52:36 2018
// Host        : laptop running 64-bit Manjaro Linux
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/jk/Documents/Homework/VHDLFPGA/Assignment3/assignment_3/assignment_3.sim/sim_1/synth/timing/xsim/pulse_gen_test_time_synth.v
// Design      : pulse_gen
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* NotValidForBitStream *)
module pulse_gen
   (clk,
    reset,
    count_to,
    pulse_10ns);
  input clk;
  input reset;
  input [26:0]count_to;
  output pulse_10ns;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire \cntr[0]_i_2_n_0 ;
  wire \cntr[0]_i_3_n_0 ;
  wire \cntr[0]_i_4_n_0 ;
  wire \cntr[0]_i_5_n_0 ;
  wire \cntr[0]_i_6_n_0 ;
  wire \cntr[12]_i_2_n_0 ;
  wire \cntr[12]_i_3_n_0 ;
  wire \cntr[12]_i_4_n_0 ;
  wire \cntr[12]_i_5_n_0 ;
  wire \cntr[16]_i_2_n_0 ;
  wire \cntr[16]_i_3_n_0 ;
  wire \cntr[16]_i_4_n_0 ;
  wire \cntr[16]_i_5_n_0 ;
  wire \cntr[20]_i_2_n_0 ;
  wire \cntr[20]_i_3_n_0 ;
  wire \cntr[20]_i_4_n_0 ;
  wire \cntr[20]_i_5_n_0 ;
  wire \cntr[24]_i_2_n_0 ;
  wire \cntr[24]_i_3_n_0 ;
  wire \cntr[24]_i_4_n_0 ;
  wire \cntr[4]_i_2_n_0 ;
  wire \cntr[4]_i_3_n_0 ;
  wire \cntr[4]_i_4_n_0 ;
  wire \cntr[4]_i_5_n_0 ;
  wire \cntr[8]_i_2_n_0 ;
  wire \cntr[8]_i_3_n_0 ;
  wire \cntr[8]_i_4_n_0 ;
  wire \cntr[8]_i_5_n_0 ;
  wire [26:0]cntr_reg;
  wire \cntr_reg[0]_i_1_n_0 ;
  wire \cntr_reg[0]_i_1_n_1 ;
  wire \cntr_reg[0]_i_1_n_2 ;
  wire \cntr_reg[0]_i_1_n_3 ;
  wire \cntr_reg[0]_i_1_n_4 ;
  wire \cntr_reg[0]_i_1_n_5 ;
  wire \cntr_reg[0]_i_1_n_6 ;
  wire \cntr_reg[0]_i_1_n_7 ;
  wire \cntr_reg[12]_i_1_n_0 ;
  wire \cntr_reg[12]_i_1_n_1 ;
  wire \cntr_reg[12]_i_1_n_2 ;
  wire \cntr_reg[12]_i_1_n_3 ;
  wire \cntr_reg[12]_i_1_n_4 ;
  wire \cntr_reg[12]_i_1_n_5 ;
  wire \cntr_reg[12]_i_1_n_6 ;
  wire \cntr_reg[12]_i_1_n_7 ;
  wire \cntr_reg[16]_i_1_n_0 ;
  wire \cntr_reg[16]_i_1_n_1 ;
  wire \cntr_reg[16]_i_1_n_2 ;
  wire \cntr_reg[16]_i_1_n_3 ;
  wire \cntr_reg[16]_i_1_n_4 ;
  wire \cntr_reg[16]_i_1_n_5 ;
  wire \cntr_reg[16]_i_1_n_6 ;
  wire \cntr_reg[16]_i_1_n_7 ;
  wire \cntr_reg[20]_i_1_n_0 ;
  wire \cntr_reg[20]_i_1_n_1 ;
  wire \cntr_reg[20]_i_1_n_2 ;
  wire \cntr_reg[20]_i_1_n_3 ;
  wire \cntr_reg[20]_i_1_n_4 ;
  wire \cntr_reg[20]_i_1_n_5 ;
  wire \cntr_reg[20]_i_1_n_6 ;
  wire \cntr_reg[20]_i_1_n_7 ;
  wire \cntr_reg[24]_i_1_n_2 ;
  wire \cntr_reg[24]_i_1_n_3 ;
  wire \cntr_reg[24]_i_1_n_5 ;
  wire \cntr_reg[24]_i_1_n_6 ;
  wire \cntr_reg[24]_i_1_n_7 ;
  wire \cntr_reg[4]_i_1_n_0 ;
  wire \cntr_reg[4]_i_1_n_1 ;
  wire \cntr_reg[4]_i_1_n_2 ;
  wire \cntr_reg[4]_i_1_n_3 ;
  wire \cntr_reg[4]_i_1_n_4 ;
  wire \cntr_reg[4]_i_1_n_5 ;
  wire \cntr_reg[4]_i_1_n_6 ;
  wire \cntr_reg[4]_i_1_n_7 ;
  wire \cntr_reg[8]_i_1_n_0 ;
  wire \cntr_reg[8]_i_1_n_1 ;
  wire \cntr_reg[8]_i_1_n_2 ;
  wire \cntr_reg[8]_i_1_n_3 ;
  wire \cntr_reg[8]_i_1_n_4 ;
  wire \cntr_reg[8]_i_1_n_5 ;
  wire \cntr_reg[8]_i_1_n_6 ;
  wire \cntr_reg[8]_i_1_n_7 ;
  wire [26:0]count_to;
  wire [26:0]count_to_IBUF;
  wire pulse_10ns;
  wire pulse_10ns_OBUF;
  wire pulse_10ns_OBUF_inst_i_10_n_0;
  wire pulse_10ns_OBUF_inst_i_11_n_0;
  wire pulse_10ns_OBUF_inst_i_12_n_0;
  wire pulse_10ns_OBUF_inst_i_2_n_0;
  wire pulse_10ns_OBUF_inst_i_2_n_1;
  wire pulse_10ns_OBUF_inst_i_2_n_2;
  wire pulse_10ns_OBUF_inst_i_2_n_3;
  wire pulse_10ns_OBUF_inst_i_3_n_0;
  wire pulse_10ns_OBUF_inst_i_4_n_0;
  wire pulse_10ns_OBUF_inst_i_4_n_1;
  wire pulse_10ns_OBUF_inst_i_4_n_2;
  wire pulse_10ns_OBUF_inst_i_4_n_3;
  wire pulse_10ns_OBUF_inst_i_5_n_0;
  wire pulse_10ns_OBUF_inst_i_6_n_0;
  wire pulse_10ns_OBUF_inst_i_7_n_0;
  wire pulse_10ns_OBUF_inst_i_8_n_0;
  wire pulse_10ns_OBUF_inst_i_9_n_0;
  wire reset;
  wire reset_IBUF;
  wire [3:2]\NLW_cntr_reg[24]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_cntr_reg[24]_i_1_O_UNCONNECTED ;
  wire [3:1]NLW_pulse_10ns_OBUF_inst_i_1_CO_UNCONNECTED;
  wire [3:0]NLW_pulse_10ns_OBUF_inst_i_1_O_UNCONNECTED;
  wire [3:0]NLW_pulse_10ns_OBUF_inst_i_2_O_UNCONNECTED;
  wire [3:0]NLW_pulse_10ns_OBUF_inst_i_4_O_UNCONNECTED;

initial begin
 $sdf_annotate("pulse_gen_test_time_synth.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[0]_i_2 
       (.I0(cntr_reg[0]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[0]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[0]_i_3 
       (.I0(cntr_reg[3]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[0]_i_4 
       (.I0(cntr_reg[2]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[0]_i_5 
       (.I0(cntr_reg[1]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \cntr[0]_i_6 
       (.I0(cntr_reg[0]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[12]_i_2 
       (.I0(cntr_reg[15]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[12]_i_3 
       (.I0(cntr_reg[14]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[12]_i_4 
       (.I0(cntr_reg[13]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[12]_i_5 
       (.I0(cntr_reg[12]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[16]_i_2 
       (.I0(cntr_reg[19]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[16]_i_3 
       (.I0(cntr_reg[18]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[16]_i_4 
       (.I0(cntr_reg[17]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[16]_i_5 
       (.I0(cntr_reg[16]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[20]_i_2 
       (.I0(cntr_reg[23]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[20]_i_3 
       (.I0(cntr_reg[22]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[20]_i_4 
       (.I0(cntr_reg[21]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[20]_i_5 
       (.I0(cntr_reg[20]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[24]_i_2 
       (.I0(cntr_reg[26]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[24]_i_3 
       (.I0(cntr_reg[25]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[24]_i_4 
       (.I0(cntr_reg[24]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[4]_i_2 
       (.I0(cntr_reg[7]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[4]_i_3 
       (.I0(cntr_reg[6]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[4]_i_4 
       (.I0(cntr_reg[5]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[4]_i_5 
       (.I0(cntr_reg[4]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[8]_i_2 
       (.I0(cntr_reg[11]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[8]_i_3 
       (.I0(cntr_reg[10]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[8]_i_4 
       (.I0(cntr_reg[9]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cntr[8]_i_5 
       (.I0(cntr_reg[8]),
        .I1(pulse_10ns_OBUF),
        .O(\cntr[8]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[0]_i_1_n_7 ),
        .Q(cntr_reg[0]));
  CARRY4 \cntr_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\cntr_reg[0]_i_1_n_0 ,\cntr_reg[0]_i_1_n_1 ,\cntr_reg[0]_i_1_n_2 ,\cntr_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\cntr[0]_i_2_n_0 }),
        .O({\cntr_reg[0]_i_1_n_4 ,\cntr_reg[0]_i_1_n_5 ,\cntr_reg[0]_i_1_n_6 ,\cntr_reg[0]_i_1_n_7 }),
        .S({\cntr[0]_i_3_n_0 ,\cntr[0]_i_4_n_0 ,\cntr[0]_i_5_n_0 ,\cntr[0]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[8]_i_1_n_5 ),
        .Q(cntr_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[8]_i_1_n_4 ),
        .Q(cntr_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[12]_i_1_n_7 ),
        .Q(cntr_reg[12]));
  CARRY4 \cntr_reg[12]_i_1 
       (.CI(\cntr_reg[8]_i_1_n_0 ),
        .CO({\cntr_reg[12]_i_1_n_0 ,\cntr_reg[12]_i_1_n_1 ,\cntr_reg[12]_i_1_n_2 ,\cntr_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cntr_reg[12]_i_1_n_4 ,\cntr_reg[12]_i_1_n_5 ,\cntr_reg[12]_i_1_n_6 ,\cntr_reg[12]_i_1_n_7 }),
        .S({\cntr[12]_i_2_n_0 ,\cntr[12]_i_3_n_0 ,\cntr[12]_i_4_n_0 ,\cntr[12]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[12]_i_1_n_6 ),
        .Q(cntr_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[12]_i_1_n_5 ),
        .Q(cntr_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[12]_i_1_n_4 ),
        .Q(cntr_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[16]_i_1_n_7 ),
        .Q(cntr_reg[16]));
  CARRY4 \cntr_reg[16]_i_1 
       (.CI(\cntr_reg[12]_i_1_n_0 ),
        .CO({\cntr_reg[16]_i_1_n_0 ,\cntr_reg[16]_i_1_n_1 ,\cntr_reg[16]_i_1_n_2 ,\cntr_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cntr_reg[16]_i_1_n_4 ,\cntr_reg[16]_i_1_n_5 ,\cntr_reg[16]_i_1_n_6 ,\cntr_reg[16]_i_1_n_7 }),
        .S({\cntr[16]_i_2_n_0 ,\cntr[16]_i_3_n_0 ,\cntr[16]_i_4_n_0 ,\cntr[16]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[16]_i_1_n_6 ),
        .Q(cntr_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[16]_i_1_n_5 ),
        .Q(cntr_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[16]_i_1_n_4 ),
        .Q(cntr_reg[19]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[0]_i_1_n_6 ),
        .Q(cntr_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[20]_i_1_n_7 ),
        .Q(cntr_reg[20]));
  CARRY4 \cntr_reg[20]_i_1 
       (.CI(\cntr_reg[16]_i_1_n_0 ),
        .CO({\cntr_reg[20]_i_1_n_0 ,\cntr_reg[20]_i_1_n_1 ,\cntr_reg[20]_i_1_n_2 ,\cntr_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cntr_reg[20]_i_1_n_4 ,\cntr_reg[20]_i_1_n_5 ,\cntr_reg[20]_i_1_n_6 ,\cntr_reg[20]_i_1_n_7 }),
        .S({\cntr[20]_i_2_n_0 ,\cntr[20]_i_3_n_0 ,\cntr[20]_i_4_n_0 ,\cntr[20]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[20]_i_1_n_6 ),
        .Q(cntr_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[20]_i_1_n_5 ),
        .Q(cntr_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[20]_i_1_n_4 ),
        .Q(cntr_reg[23]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[24]_i_1_n_7 ),
        .Q(cntr_reg[24]));
  CARRY4 \cntr_reg[24]_i_1 
       (.CI(\cntr_reg[20]_i_1_n_0 ),
        .CO({\NLW_cntr_reg[24]_i_1_CO_UNCONNECTED [3:2],\cntr_reg[24]_i_1_n_2 ,\cntr_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_cntr_reg[24]_i_1_O_UNCONNECTED [3],\cntr_reg[24]_i_1_n_5 ,\cntr_reg[24]_i_1_n_6 ,\cntr_reg[24]_i_1_n_7 }),
        .S({1'b0,\cntr[24]_i_2_n_0 ,\cntr[24]_i_3_n_0 ,\cntr[24]_i_4_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[24]_i_1_n_6 ),
        .Q(cntr_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[24]_i_1_n_5 ),
        .Q(cntr_reg[26]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[0]_i_1_n_5 ),
        .Q(cntr_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[0]_i_1_n_4 ),
        .Q(cntr_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[4]_i_1_n_7 ),
        .Q(cntr_reg[4]));
  CARRY4 \cntr_reg[4]_i_1 
       (.CI(\cntr_reg[0]_i_1_n_0 ),
        .CO({\cntr_reg[4]_i_1_n_0 ,\cntr_reg[4]_i_1_n_1 ,\cntr_reg[4]_i_1_n_2 ,\cntr_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cntr_reg[4]_i_1_n_4 ,\cntr_reg[4]_i_1_n_5 ,\cntr_reg[4]_i_1_n_6 ,\cntr_reg[4]_i_1_n_7 }),
        .S({\cntr[4]_i_2_n_0 ,\cntr[4]_i_3_n_0 ,\cntr[4]_i_4_n_0 ,\cntr[4]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[4]_i_1_n_6 ),
        .Q(cntr_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[4]_i_1_n_5 ),
        .Q(cntr_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[4]_i_1_n_4 ),
        .Q(cntr_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[8]_i_1_n_7 ),
        .Q(cntr_reg[8]));
  CARRY4 \cntr_reg[8]_i_1 
       (.CI(\cntr_reg[4]_i_1_n_0 ),
        .CO({\cntr_reg[8]_i_1_n_0 ,\cntr_reg[8]_i_1_n_1 ,\cntr_reg[8]_i_1_n_2 ,\cntr_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cntr_reg[8]_i_1_n_4 ,\cntr_reg[8]_i_1_n_5 ,\cntr_reg[8]_i_1_n_6 ,\cntr_reg[8]_i_1_n_7 }),
        .S({\cntr[8]_i_2_n_0 ,\cntr[8]_i_3_n_0 ,\cntr[8]_i_4_n_0 ,\cntr[8]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \cntr_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset_IBUF),
        .D(\cntr_reg[8]_i_1_n_6 ),
        .Q(cntr_reg[9]));
  IBUF \count_to_IBUF[0]_inst 
       (.I(count_to[0]),
        .O(count_to_IBUF[0]));
  IBUF \count_to_IBUF[10]_inst 
       (.I(count_to[10]),
        .O(count_to_IBUF[10]));
  IBUF \count_to_IBUF[11]_inst 
       (.I(count_to[11]),
        .O(count_to_IBUF[11]));
  IBUF \count_to_IBUF[12]_inst 
       (.I(count_to[12]),
        .O(count_to_IBUF[12]));
  IBUF \count_to_IBUF[13]_inst 
       (.I(count_to[13]),
        .O(count_to_IBUF[13]));
  IBUF \count_to_IBUF[14]_inst 
       (.I(count_to[14]),
        .O(count_to_IBUF[14]));
  IBUF \count_to_IBUF[15]_inst 
       (.I(count_to[15]),
        .O(count_to_IBUF[15]));
  IBUF \count_to_IBUF[16]_inst 
       (.I(count_to[16]),
        .O(count_to_IBUF[16]));
  IBUF \count_to_IBUF[17]_inst 
       (.I(count_to[17]),
        .O(count_to_IBUF[17]));
  IBUF \count_to_IBUF[18]_inst 
       (.I(count_to[18]),
        .O(count_to_IBUF[18]));
  IBUF \count_to_IBUF[19]_inst 
       (.I(count_to[19]),
        .O(count_to_IBUF[19]));
  IBUF \count_to_IBUF[1]_inst 
       (.I(count_to[1]),
        .O(count_to_IBUF[1]));
  IBUF \count_to_IBUF[20]_inst 
       (.I(count_to[20]),
        .O(count_to_IBUF[20]));
  IBUF \count_to_IBUF[21]_inst 
       (.I(count_to[21]),
        .O(count_to_IBUF[21]));
  IBUF \count_to_IBUF[22]_inst 
       (.I(count_to[22]),
        .O(count_to_IBUF[22]));
  IBUF \count_to_IBUF[23]_inst 
       (.I(count_to[23]),
        .O(count_to_IBUF[23]));
  IBUF \count_to_IBUF[24]_inst 
       (.I(count_to[24]),
        .O(count_to_IBUF[24]));
  IBUF \count_to_IBUF[25]_inst 
       (.I(count_to[25]),
        .O(count_to_IBUF[25]));
  IBUF \count_to_IBUF[26]_inst 
       (.I(count_to[26]),
        .O(count_to_IBUF[26]));
  IBUF \count_to_IBUF[2]_inst 
       (.I(count_to[2]),
        .O(count_to_IBUF[2]));
  IBUF \count_to_IBUF[3]_inst 
       (.I(count_to[3]),
        .O(count_to_IBUF[3]));
  IBUF \count_to_IBUF[4]_inst 
       (.I(count_to[4]),
        .O(count_to_IBUF[4]));
  IBUF \count_to_IBUF[5]_inst 
       (.I(count_to[5]),
        .O(count_to_IBUF[5]));
  IBUF \count_to_IBUF[6]_inst 
       (.I(count_to[6]),
        .O(count_to_IBUF[6]));
  IBUF \count_to_IBUF[7]_inst 
       (.I(count_to[7]),
        .O(count_to_IBUF[7]));
  IBUF \count_to_IBUF[8]_inst 
       (.I(count_to[8]),
        .O(count_to_IBUF[8]));
  IBUF \count_to_IBUF[9]_inst 
       (.I(count_to[9]),
        .O(count_to_IBUF[9]));
  OBUF pulse_10ns_OBUF_inst
       (.I(pulse_10ns_OBUF),
        .O(pulse_10ns));
  CARRY4 pulse_10ns_OBUF_inst_i_1
       (.CI(pulse_10ns_OBUF_inst_i_2_n_0),
        .CO({NLW_pulse_10ns_OBUF_inst_i_1_CO_UNCONNECTED[3:1],pulse_10ns_OBUF}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_pulse_10ns_OBUF_inst_i_1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,pulse_10ns_OBUF_inst_i_3_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_10
       (.I0(cntr_reg[6]),
        .I1(count_to_IBUF[6]),
        .I2(count_to_IBUF[8]),
        .I3(cntr_reg[8]),
        .I4(count_to_IBUF[7]),
        .I5(cntr_reg[7]),
        .O(pulse_10ns_OBUF_inst_i_10_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_11
       (.I0(cntr_reg[3]),
        .I1(count_to_IBUF[3]),
        .I2(count_to_IBUF[5]),
        .I3(cntr_reg[5]),
        .I4(count_to_IBUF[4]),
        .I5(cntr_reg[4]),
        .O(pulse_10ns_OBUF_inst_i_11_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_12
       (.I0(cntr_reg[0]),
        .I1(count_to_IBUF[0]),
        .I2(count_to_IBUF[2]),
        .I3(cntr_reg[2]),
        .I4(count_to_IBUF[1]),
        .I5(cntr_reg[1]),
        .O(pulse_10ns_OBUF_inst_i_12_n_0));
  CARRY4 pulse_10ns_OBUF_inst_i_2
       (.CI(pulse_10ns_OBUF_inst_i_4_n_0),
        .CO({pulse_10ns_OBUF_inst_i_2_n_0,pulse_10ns_OBUF_inst_i_2_n_1,pulse_10ns_OBUF_inst_i_2_n_2,pulse_10ns_OBUF_inst_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_pulse_10ns_OBUF_inst_i_2_O_UNCONNECTED[3:0]),
        .S({pulse_10ns_OBUF_inst_i_5_n_0,pulse_10ns_OBUF_inst_i_6_n_0,pulse_10ns_OBUF_inst_i_7_n_0,pulse_10ns_OBUF_inst_i_8_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_3
       (.I0(cntr_reg[24]),
        .I1(count_to_IBUF[24]),
        .I2(count_to_IBUF[26]),
        .I3(cntr_reg[26]),
        .I4(count_to_IBUF[25]),
        .I5(cntr_reg[25]),
        .O(pulse_10ns_OBUF_inst_i_3_n_0));
  CARRY4 pulse_10ns_OBUF_inst_i_4
       (.CI(1'b0),
        .CO({pulse_10ns_OBUF_inst_i_4_n_0,pulse_10ns_OBUF_inst_i_4_n_1,pulse_10ns_OBUF_inst_i_4_n_2,pulse_10ns_OBUF_inst_i_4_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_pulse_10ns_OBUF_inst_i_4_O_UNCONNECTED[3:0]),
        .S({pulse_10ns_OBUF_inst_i_9_n_0,pulse_10ns_OBUF_inst_i_10_n_0,pulse_10ns_OBUF_inst_i_11_n_0,pulse_10ns_OBUF_inst_i_12_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_5
       (.I0(cntr_reg[21]),
        .I1(count_to_IBUF[21]),
        .I2(count_to_IBUF[23]),
        .I3(cntr_reg[23]),
        .I4(count_to_IBUF[22]),
        .I5(cntr_reg[22]),
        .O(pulse_10ns_OBUF_inst_i_5_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_6
       (.I0(cntr_reg[18]),
        .I1(count_to_IBUF[18]),
        .I2(count_to_IBUF[20]),
        .I3(cntr_reg[20]),
        .I4(count_to_IBUF[19]),
        .I5(cntr_reg[19]),
        .O(pulse_10ns_OBUF_inst_i_6_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_7
       (.I0(cntr_reg[15]),
        .I1(count_to_IBUF[15]),
        .I2(count_to_IBUF[17]),
        .I3(cntr_reg[17]),
        .I4(count_to_IBUF[16]),
        .I5(cntr_reg[16]),
        .O(pulse_10ns_OBUF_inst_i_7_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_8
       (.I0(cntr_reg[12]),
        .I1(count_to_IBUF[12]),
        .I2(count_to_IBUF[14]),
        .I3(cntr_reg[14]),
        .I4(count_to_IBUF[13]),
        .I5(cntr_reg[13]),
        .O(pulse_10ns_OBUF_inst_i_8_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    pulse_10ns_OBUF_inst_i_9
       (.I0(cntr_reg[9]),
        .I1(count_to_IBUF[9]),
        .I2(count_to_IBUF[11]),
        .I3(cntr_reg[11]),
        .I4(count_to_IBUF[10]),
        .I5(cntr_reg[10]),
        .O(pulse_10ns_OBUF_inst_i_9_n_0));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
