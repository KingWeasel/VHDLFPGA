----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--testbench for pulse_gen
---------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pulse_gen_tb is
--  Port ( );
end pulse_gen_tb;

architecture testbench of pulse_gen_tb is
    signal clk : std_logic;
    signal reset : std_logic;
    signal count_to_test_val : unsigned(26 downto 0);--this way I can make it slightly more general
    signal pulse : std_logic;
begin

    pulse_generator : entity pulse_gen port map
    (
        clk => clk,
        reset => reset,
        count_to => count_to_test_val,
        pulse_10ns => pulse
    );
    
    reset <= '0', '1' after 20 ns, '0' after 100 ns;
    
--    count_to_test_val <= "000000000011000011010100000";--for 1kHz clock
      count_to_test_val <= "101111101011110000100000000";--for 1Hz clock
    
    process
    begin
        clk <= '1';
        wait for 5 ns;
        clk <= '0';
        wait for 5 ns;
    end process;

end testbench;
