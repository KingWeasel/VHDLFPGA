----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--testbench for pulse_gen
---------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity anode_scroller_tb is
--  Port ( );
end anode_scroller_tb;

architecture testbench of anode_scroller_tb is
    signal clk : std_logic;
    signal reset : std_logic;
    signal enable_pulse : std_logic;
    signal anode_set : std_logic_vector (7 downto 0);
begin

    anode_scroller_inst : entity anode_scroller port map
    (
        clk => clk,
        reset => reset,
        scroll_enable => enable_pulse,
        anode_switches => anode_set
    );
    
    reset <= '0', '1' after 20 ns, '0' after 100 ns;
    
    process
    begin
        clk <= '1';
        wait for 5 ns;
        clk <= '0';
        wait for 5 ns;
    end process;
    
    process
    begin
        enable_pulse <= '1';
        wait for 10 ns;
        enable_pulse <= '0';
        wait for 999990 ns;
    end process;

end testbench;
