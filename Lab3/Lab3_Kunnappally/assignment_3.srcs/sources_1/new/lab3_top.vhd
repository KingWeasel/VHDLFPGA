----------------------------------------------------------------------------------
--
-- Author: Jacob Kunnappally
--
-- Description:
--this module implements the circuit required for lab3_top,
--which is a scrolling display of the values of the first
--four switches in hex on a row of seven-segment displays
---------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab3_top is
    Port ( CLK100MHZ : in STD_LOGIC;--Clock
           BTNC : in STD_LOGIC;--async reset
           SW : in STD_LOGIC_VECTOR (3 downto 0);-- 3 downto 0 are display inputs
           LED : out STD_LOGIC_VECTOR (3 downto 0);
           SEG7_CATH : out STD_LOGIC_VECTOR (7 downto 0);
           AN : out STD_LOGIC_VECTOR (7 downto 0)
          );
end lab3_top;

architecture Behavioral of lab3_top is
    signal disp_reg : std_logic_vector ( 31 downto 0);
    signal shift_enable : std_logic;
begin
    --instantiate the display logic
    display_controller : entity seg7_controller port map
    (
        clk => CLK100MHZ,
        reset => BTNC,
        disp_chars => disp_reg,
        anode_ctl => AN,
        seg7_out => SEG7_CATH
    );
    
    -- generates a 1Hz pulse train for activating the
    -- shift_reg process below
    pulse_1Hz : entity pulse_gen port map
    (
        clk => CLK100MHZ,
        reset => BTNC,
        count_to => "101111101011110000100000000", --100,000,000 , or about 1s
        pulse_10ns => shift_enable
    );
    
    -- process for shifting data between
    -- 8 4-bit registers at 1Hz (really one 32-bit register)
    shift_register : process(CLK100MHZ, BTNC)
    begin
        if(BTNC = '1') then
            disp_reg <= (others => '0');
        elsif(rising_edge(CLK100MHZ)) then
            if(shift_enable = '1') then
                disp_reg (3 downto 0) <= SW;
                disp_reg (7 downto 4) <= disp_reg (3 downto 0);
                disp_reg (11 downto 8) <= disp_reg (7 downto 4);
                disp_reg (15 downto 12) <= disp_reg (11 downto 8);
                disp_reg (19 downto 16) <= disp_reg (15 downto 12);
                disp_reg (23 downto 20) <= disp_reg (19 downto 16);
                disp_reg (27 downto 24) <= disp_reg (23 downto 20);
                disp_reg (31 downto 28) <= disp_reg (27 downto 24);                    
            end if;
        end if;
    end process shift_register;
    
    -- doesn't hurt
    LED <= SW;
        
end Behavioral;
